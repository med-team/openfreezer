-- MySQL dump 10.11
--
-- Host: localhost    Database: marina
-- ------------------------------------------------------
-- Server version	5.0.77

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;
/*!40103 SET @OLD_TIME_ZONE=@@TIME_ZONE */;
/*!40103 SET TIME_ZONE='+00:00' */;
/*!40014 SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;

--
-- Table structure for table `AssocProp_tbl`
--

DROP TABLE IF EXISTS `AssocProp_tbl`;
SET @saved_cs_client     = @@character_set_client;
SET character_set_client = utf8;
CREATE TABLE `AssocProp_tbl` (
  `propListID` int(11) NOT NULL auto_increment,
  `assID` int(11) NOT NULL default '0',
  `APropertyID` int(11) NOT NULL default '0',
  `propertyValue` varchar(27) NOT NULL default '',
  `labID` int(11) NOT NULL default '1',
  `status` enum('ACTIVE','DEP') NOT NULL default 'ACTIVE',
  PRIMARY KEY  (`propListID`),
  KEY `propListID` (`propListID`),
  KEY `assID` (`assID`),
  KEY `APropertyID` (`APropertyID`),
  KEY `propValue_index` (`propertyValue`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1;
SET character_set_client = @saved_cs_client;

--
-- Dumping data for table `AssocProp_tbl`
--

LOCK TABLES `AssocProp_tbl` WRITE;
/*!40000 ALTER TABLE `AssocProp_tbl` DISABLE KEYS */;
/*!40000 ALTER TABLE `AssocProp_tbl` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `AssocType_tbl`
--

DROP TABLE IF EXISTS `AssocType_tbl`;
SET @saved_cs_client     = @@character_set_client;
SET character_set_client = utf8;
CREATE TABLE `AssocType_tbl` (
  `ATypeID` int(11) NOT NULL auto_increment,
  `association` varchar(27) default NULL,
  `description` tinytext,
  `status` enum('ACTIVE','DEP') NOT NULL default 'ACTIVE',
  PRIMARY KEY  (`ATypeID`),
  KEY `ATypeID` (`ATypeID`)
) ENGINE=MyISAM AUTO_INCREMENT=7 DEFAULT CHARSET=latin1;
SET character_set_client = @saved_cs_client;

--
-- Dumping data for table `AssocType_tbl`
--

LOCK TABLES `AssocType_tbl` WRITE;
/*!40000 ALTER TABLE `AssocType_tbl` DISABLE KEYS */;
INSERT INTO `AssocType_tbl` VALUES (1,'INSERT','','ACTIVE'),(2,'LOXP','','ACTIVE'),(3,'BASIC','','ACTIVE'),(4,'Insert Oligos','','ACTIVE'),(5,'CellLine Stable','','ACTIVE'),(6,'Insert Parent Vector','','ACTIVE');
/*!40000 ALTER TABLE `AssocType_tbl` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `Assoc_Prop_Type_tbl`
--

DROP TABLE IF EXISTS `Assoc_Prop_Type_tbl`;
SET @saved_cs_client     = @@character_set_client;
SET character_set_client = utf8;
CREATE TABLE `Assoc_Prop_Type_tbl` (
  `APropertyID` int(11) NOT NULL auto_increment,
  `reagentTypeID` int(11) NOT NULL default '0',
  `APropName` varchar(117) NOT NULL default '',
  `hierarchy` enum('PARENT','SIBLING','CHILD') NOT NULL default 'PARENT',
  `alias` tinytext,
  `status` enum('ACTIVE','DEP') NOT NULL default 'ACTIVE',
  `description` varchar(250) default NULL,
  `assocTypeID` int(11) NOT NULL,
  PRIMARY KEY  (`APropertyID`),
  KEY `assocTypeID` (`assocTypeID`)
) ENGINE=MyISAM AUTO_INCREMENT=10 DEFAULT CHARSET=latin1;
SET character_set_client = @saved_cs_client;

--
-- Dumping data for table `Assoc_Prop_Type_tbl`
--

LOCK TABLES `Assoc_Prop_Type_tbl` WRITE;
/*!40000 ALTER TABLE `Assoc_Prop_Type_tbl` DISABLE KEYS */;
INSERT INTO `Assoc_Prop_Type_tbl` VALUES (1,1,'insert id','PARENT','insert_id','ACTIVE','Insert ID',2),(2,1,'vector parent id','PARENT','parent_vector','ACTIVE','Parent Vector ID',1),(4,2,'insert parent vector id','PARENT','insert_parent_vector','ACTIVE','Insert Parent Vector ID',1),(5,2,'sense oligo','PARENT','sense_oligo','ACTIVE','Sense Oligo',3),(6,2,'antisense oligo','PARENT','antisense_oligo','ACTIVE','Antisense Oligo',3),(7,1,'parent insert vector','PARENT','parent_insert_vector','ACTIVE','Insert Parent Vector ID',1),(8,4,'cell line parent vector id','PARENT','cell_line_parent_vector','ACTIVE','Parent Vector ID',1),(9,4,'parent cell line id','PARENT','parent_cell_line','ACTIVE','Parent Cell Line ID',4);
/*!40000 ALTER TABLE `Assoc_Prop_Type_tbl` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `Association_tbl`
--

DROP TABLE IF EXISTS `Association_tbl`;
SET @saved_cs_client     = @@character_set_client;
SET character_set_client = utf8;
CREATE TABLE `Association_tbl` (
  `assID` int(11) NOT NULL auto_increment,
  `reagentID` int(11) NOT NULL default '0',
  `ATypeID` int(11) NOT NULL default '0',
  `labID` int(11) NOT NULL default '1',
  `status` enum('ACTIVE','DEP') NOT NULL default 'ACTIVE',
  PRIMARY KEY  (`assID`),
  KEY `assID` (`assID`),
  KEY `reagentID` (`reagentID`),
  KEY `ATypeID` (`ATypeID`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1;
SET character_set_client = @saved_cs_client;

--
-- Dumping data for table `Association_tbl`
--

LOCK TABLES `Association_tbl` WRITE;
/*!40000 ALTER TABLE `Association_tbl` DISABLE KEYS */;
/*!40000 ALTER TABLE `Association_tbl` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `BugReport_tbl`
--

DROP TABLE IF EXISTS `BugReport_tbl`;
SET @saved_cs_client     = @@character_set_client;
SET character_set_client = utf8;
CREATE TABLE `BugReport_tbl` (
  `bug_type` enum('Bug','Feature') NOT NULL default 'Bug',
  `bug_descr` text,
  `status` enum('ACTIVE','DEP') default 'ACTIVE',
  `is_closed` enum('YES','NO') default 'NO',
  `module` text,
  `bugID` int(11) NOT NULL auto_increment,
  `requested_by` int(11) default NULL,
  PRIMARY KEY  (`bugID`),
  KEY `requested_by` (`requested_by`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1;
SET character_set_client = @saved_cs_client;

--
-- Dumping data for table `BugReport_tbl`
--

LOCK TABLES `BugReport_tbl` WRITE;
/*!40000 ALTER TABLE `BugReport_tbl` DISABLE KEYS */;
/*!40000 ALTER TABLE `BugReport_tbl` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `ChemicalLocationNames_tbl`
--

DROP TABLE IF EXISTS `ChemicalLocationNames_tbl`;
SET @saved_cs_client     = @@character_set_client;
SET character_set_client = utf8;
CREATE TABLE `ChemicalLocationNames_tbl` (
  `chemLocNameID` int(11) NOT NULL auto_increment,
  `chemicalLocationName` varchar(250) NOT NULL,
  `chemicalLocationDescr` varchar(250) NOT NULL,
  `status` enum('ACTIVE','DEP') default 'ACTIVE',
  PRIMARY KEY  (`chemLocNameID`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1;
SET character_set_client = @saved_cs_client;

--
-- Dumping data for table `ChemicalLocationNames_tbl`
--

LOCK TABLES `ChemicalLocationNames_tbl` WRITE;
/*!40000 ALTER TABLE `ChemicalLocationNames_tbl` DISABLE KEYS */;
/*!40000 ALTER TABLE `ChemicalLocationNames_tbl` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `ChemicalLocationTypes_tbl`
--

DROP TABLE IF EXISTS `ChemicalLocationTypes_tbl`;
SET @saved_cs_client     = @@character_set_client;
SET character_set_client = utf8;
CREATE TABLE `ChemicalLocationTypes_tbl` (
  `chemLocTypeID` int(11) NOT NULL auto_increment,
  `chemLocTypeName` varchar(250) NOT NULL default '',
  `status` enum('ACTIVE','DEP') default 'ACTIVE',
  PRIMARY KEY  (`chemLocTypeID`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1;
SET character_set_client = @saved_cs_client;

--
-- Dumping data for table `ChemicalLocationTypes_tbl`
--

LOCK TABLES `ChemicalLocationTypes_tbl` WRITE;
/*!40000 ALTER TABLE `ChemicalLocationTypes_tbl` DISABLE KEYS */;
/*!40000 ALTER TABLE `ChemicalLocationTypes_tbl` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `ChemicalLocations_tbl`
--

DROP TABLE IF EXISTS `ChemicalLocations_tbl`;
SET @saved_cs_client     = @@character_set_client;
SET character_set_client = utf8;
CREATE TABLE `ChemicalLocations_tbl` (
  `chemLocID` int(11) NOT NULL auto_increment,
  `chemLocName` int(11) NOT NULL,
  `chemLocType` int(11) NOT NULL,
  `chemLocComments` int(11) default NULL,
  `status` enum('ACTIVE','DEP') default 'ACTIVE',
  PRIMARY KEY  (`chemLocID`),
  KEY `chemLocName` (`chemLocName`),
  KEY `chemLocType` (`chemLocType`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1;
SET character_set_client = @saved_cs_client;

--
-- Dumping data for table `ChemicalLocations_tbl`
--

LOCK TABLES `ChemicalLocations_tbl` WRITE;
/*!40000 ALTER TABLE `ChemicalLocations_tbl` DISABLE KEYS */;
/*!40000 ALTER TABLE `ChemicalLocations_tbl` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `ChemicalNames_tbl`
--

DROP TABLE IF EXISTS `ChemicalNames_tbl`;
SET @saved_cs_client     = @@character_set_client;
SET character_set_client = utf8;
CREATE TABLE `ChemicalNames_tbl` (
  `chemicalID` int(11) NOT NULL auto_increment,
  `chemicalName` varchar(250) NOT NULL,
  `CAS_No` varchar(250) NOT NULL,
  `status` enum('ACTIVE','DEP') default 'ACTIVE',
  PRIMARY KEY  (`chemicalID`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1;
SET character_set_client = @saved_cs_client;

--
-- Dumping data for table `ChemicalNames_tbl`
--

LOCK TABLES `ChemicalNames_tbl` WRITE;
/*!40000 ALTER TABLE `ChemicalNames_tbl` DISABLE KEYS */;
/*!40000 ALTER TABLE `ChemicalNames_tbl` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `Chemicals_tbl`
--

DROP TABLE IF EXISTS `Chemicals_tbl`;
SET @saved_cs_client     = @@character_set_client;
SET character_set_client = utf8;
CREATE TABLE `Chemicals_tbl` (
  `chemicalID` int(11) NOT NULL default '0',
  `chemicalLocation` int(11) NOT NULL default '0',
  `Comments` int(11) default NULL,
  `Quantity` varchar(50) default NULL,
  `Supplier` int(11) default NULL,
  `Safety` set('A - Compressed Gas','B1 - Flammable Gas','B2 - Flammable Liquid','B3 - Combustible Liquid','B4 - Flammable Solid','B5 - Flammable Aerosol','B6 - Reactive Flammable','C - Oxidizing','D1A - Immediate Very Toxic','D1B - Immediate Toxic','D2A - Other Very Toxic','D2B - Other Toxic','E - Corrosive','F - Dangerously Reactive','Non-Controlled') default NULL,
  `status` enum('ACTIVE','DEP') default 'ACTIVE',
  `chemListID` int(11) NOT NULL auto_increment,
  `MSDS` text,
  PRIMARY KEY  (`chemListID`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1;
SET character_set_client = @saved_cs_client;

--
-- Dumping data for table `Chemicals_tbl`
--

LOCK TABLES `Chemicals_tbl` WRITE;
/*!40000 ALTER TABLE `Chemicals_tbl` DISABLE KEYS */;
/*!40000 ALTER TABLE `Chemicals_tbl` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `CommentLink_tbl`
--

DROP TABLE IF EXISTS `CommentLink_tbl`;
SET @saved_cs_client     = @@character_set_client;
SET character_set_client = utf8;
CREATE TABLE `CommentLink_tbl` (
  `commentLinkID` int(11) NOT NULL auto_increment,
  `link` varchar(27) NOT NULL default '',
  `status` enum('ACTIVE','DEP') NOT NULL default 'ACTIVE',
  PRIMARY KEY  (`commentLinkID`),
  KEY `commentLinkID` (`commentLinkID`)
) ENGINE=MyISAM AUTO_INCREMENT=6 DEFAULT CHARSET=latin1;
SET character_set_client = @saved_cs_client;

--
-- Dumping data for table `CommentLink_tbl`
--

LOCK TABLES `CommentLink_tbl` WRITE;
/*!40000 ALTER TABLE `CommentLink_tbl` DISABLE KEYS */;
INSERT INTO `CommentLink_tbl` VALUES (1,'Reagent','ACTIVE'),(2,'Container','ACTIVE'),(3,'Packet','ACTIVE'),(4,'Experiment','ACTIVE'),(5,'Chemical','ACTIVE');
/*!40000 ALTER TABLE `CommentLink_tbl` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `ContainerGroup_tbl`
--

DROP TABLE IF EXISTS `ContainerGroup_tbl`;
SET @saved_cs_client     = @@character_set_client;
SET character_set_client = utf8;
CREATE TABLE `ContainerGroup_tbl` (
  `contGroupID` int(11) NOT NULL auto_increment,
  `contGroupName` varchar(27) NOT NULL default '',
  `status` enum('ACTIVE','DEP') NOT NULL default 'ACTIVE',
  `isolate_active` enum('YES','NO') default 'NO',
  `contGroupCode` char(2) default NULL,
  PRIMARY KEY  (`contGroupID`),
  KEY `contGroupID` (`contGroupID`)
) ENGINE=MyISAM AUTO_INCREMENT=2 DEFAULT CHARSET=latin1;
SET character_set_client = @saved_cs_client;

--
-- Dumping data for table `ContainerGroup_tbl`
--

LOCK TABLES `ContainerGroup_tbl` WRITE;
/*!40000 ALTER TABLE `ContainerGroup_tbl` DISABLE KEYS */;
INSERT INTO `ContainerGroup_tbl` VALUES (1,'Glycerol Stocks','ACTIVE','YES','GS');
/*!40000 ALTER TABLE `ContainerGroup_tbl` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `ContainerReagentTypes_tbl`
--

DROP TABLE IF EXISTS `ContainerReagentTypes_tbl`;
SET @saved_cs_client     = @@character_set_client;
SET character_set_client = utf8;
CREATE TABLE `ContainerReagentTypes_tbl` (
  `contReagentTypeID` int(11) NOT NULL auto_increment,
  `contTypeID` int(11) NOT NULL,
  `reagentTypeID` int(11) NOT NULL,
  `status` enum('ACTIVE','DEP') NOT NULL default 'ACTIVE',
  PRIMARY KEY  (`contReagentTypeID`),
  KEY `contTypeID` (`contTypeID`),
  KEY `reagentTypeID` (`reagentTypeID`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1;
SET character_set_client = @saved_cs_client;

--
-- Dumping data for table `ContainerReagentTypes_tbl`
--

LOCK TABLES `ContainerReagentTypes_tbl` WRITE;
/*!40000 ALTER TABLE `ContainerReagentTypes_tbl` DISABLE KEYS */;
/*!40000 ALTER TABLE `ContainerReagentTypes_tbl` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `ContainerTypeAttributes_tbl`
--

DROP TABLE IF EXISTS `ContainerTypeAttributes_tbl`;
SET @saved_cs_client     = @@character_set_client;
SET character_set_client = utf8;
CREATE TABLE `ContainerTypeAttributes_tbl` (
  `contTypePropID` int(11) NOT NULL auto_increment,
  `containerTypeID` int(11) NOT NULL,
  `containerTypeAttributeID` int(11) NOT NULL,
  `status` enum('ACTIVE','DEP') NOT NULL default 'ACTIVE',
  PRIMARY KEY  (`contTypePropID`),
  KEY `containerTypeID` (`containerTypeID`),
  KEY `containerTypeAttributeID` (`containerTypeAttributeID`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1;
SET character_set_client = @saved_cs_client;

--
-- Dumping data for table `ContainerTypeAttributes_tbl`
--

LOCK TABLES `ContainerTypeAttributes_tbl` WRITE;
/*!40000 ALTER TABLE `ContainerTypeAttributes_tbl` DISABLE KEYS */;
/*!40000 ALTER TABLE `ContainerTypeAttributes_tbl` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `ContainerTypeID_tbl`
--

DROP TABLE IF EXISTS `ContainerTypeID_tbl`;
SET @saved_cs_client     = @@character_set_client;
SET character_set_client = utf8;
CREATE TABLE `ContainerTypeID_tbl` (
  `contTypeID` int(11) NOT NULL auto_increment,
  `containerName` varchar(27) NOT NULL default '',
  `maxCol` int(11) NOT NULL default '0',
  `maxRow` int(11) NOT NULL default '0',
  `status` enum('ACTIVE','DEP') NOT NULL default 'ACTIVE',
  PRIMARY KEY  (`contTypeID`),
  KEY `contTypeID` (`contTypeID`)
) ENGINE=MyISAM AUTO_INCREMENT=2 DEFAULT CHARSET=latin1;
SET character_set_client = @saved_cs_client;

--
-- Dumping data for table `ContainerTypeID_tbl`
--

LOCK TABLES `ContainerTypeID_tbl` WRITE;
/*!40000 ALTER TABLE `ContainerTypeID_tbl` DISABLE KEYS */;
INSERT INTO `ContainerTypeID_tbl` VALUES (1,'96-well plate',12,8,'ACTIVE');
/*!40000 ALTER TABLE `ContainerTypeID_tbl` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `Container_tbl`
--

DROP TABLE IF EXISTS `Container_tbl`;
SET @saved_cs_client     = @@character_set_client;
SET character_set_client = utf8;
CREATE TABLE `Container_tbl` (
  `containerID` int(11) NOT NULL auto_increment,
  `contGroupID` int(11) NOT NULL default '0',
  `contTypeID` int(11) NOT NULL default '0',
  `contGroupCount` int(11) NOT NULL default '0',
  `name` tinytext,
  `container_desc` tinytext,
  `labID` int(11) NOT NULL default '1',
  `status` enum('ACTIVE','DEP') NOT NULL default 'ACTIVE',
  `barcode` varchar(250) default NULL,
  `isolate_active` enum('YES','NO') NOT NULL default 'YES',
  `shelf` int(11) default NULL,
  `rack` int(11) default NULL,
  `location` int(11) NOT NULL,
  `row_number` int(11) default NULL,
  `col_number` int(11) default NULL,
  `locationName` varchar(250) default NULL,
  `address` varchar(250) default NULL,
  PRIMARY KEY  (`containerID`),
  KEY `containerID` (`containerID`),
  KEY `contGroupID` (`contGroupID`),
  KEY `contTypeID` (`contTypeID`),
  KEY `contGroupCount` (`contGroupCount`),
  KEY `labID` (`labID`),
  KEY `location` (`location`)
) ENGINE=MyISAM AUTO_INCREMENT=7 DEFAULT CHARSET=latin1;
SET character_set_client = @saved_cs_client;

--
-- Dumping data for table `Container_tbl`
--

LOCK TABLES `Container_tbl` WRITE;
/*!40000 ALTER TABLE `Container_tbl` DISABLE KEYS */;
INSERT INTO `Container_tbl` VALUES (1,1,1,1,'First container in OpenFreezer','Feel free to change/remove this',1,'DEP','96GS1','YES',0,0,3,0,0,'Change this',''),(2,2,1,1,'Oligo Box 1','description added now',2,'ACTIVE','CL96OL1','YES',1,2,2,4,3,'Freezer 1',''),(3,2,2,2,'Oligo Box 3','Third oligo box in Colwill lab',2,'DEP','CL100OL1','YES',4,1,2,3,2,'Colwill Freezer3','room 984 '),(4,3,2,1,'Antibody Box 1','Antibodys A-C',2,'ACTIVE','CL100AB1','YES',3,4,1,5,5,'Colwill Fridge 1','room 970'),(5,1,1,1,'First Glycerol Stock Container','',1,'ACTIVE','AD96GS1','YES',0,0,1,0,0,'Fridge 1',''),(6,3,1,2,'Oligo Box 1','desic',2,'ACTIVE','CL96OL1','YES',2,3,2,0,0,'Colwill Freezer3','room 984');
/*!40000 ALTER TABLE `Container_tbl` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `Experiment_tbl`
--

DROP TABLE IF EXISTS `Experiment_tbl`;
SET @saved_cs_client     = @@character_set_client;
SET character_set_client = utf8;
CREATE TABLE `Experiment_tbl` (
  `expID` int(11) NOT NULL auto_increment,
  `reagentID` int(11) default NULL,
  `oldID` varchar(17) default NULL,
  `status` enum('ACTIVE','DEP') NOT NULL default 'ACTIVE',
  PRIMARY KEY  (`expID`),
  KEY `expID` (`expID`),
  KEY `reagentID` (`reagentID`),
  KEY `oldID` (`oldID`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1;
SET character_set_client = @saved_cs_client;

--
-- Dumping data for table `Experiment_tbl`
--

LOCK TABLES `Experiment_tbl` WRITE;
/*!40000 ALTER TABLE `Experiment_tbl` DISABLE KEYS */;
/*!40000 ALTER TABLE `Experiment_tbl` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `GeneralComments_tbl`
--

DROP TABLE IF EXISTS `GeneralComments_tbl`;
SET @saved_cs_client     = @@character_set_client;
SET character_set_client = utf8;
CREATE TABLE `GeneralComments_tbl` (
  `commentID` int(11) NOT NULL auto_increment,
  `commentLinkID` int(250) default NULL,
  `comment` text,
  `labID` int(11) NOT NULL default '1',
  `status` enum('ACTIVE','DEP') NOT NULL default 'ACTIVE',
  PRIMARY KEY  (`commentID`),
  KEY `commentID` (`commentID`),
  KEY `commentLinkID` (`commentLinkID`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1;
SET character_set_client = @saved_cs_client;

--
-- Dumping data for table `GeneralComments_tbl`
--

LOCK TABLES `GeneralComments_tbl` WRITE;
/*!40000 ALTER TABLE `GeneralComments_tbl` DISABLE KEYS */;
/*!40000 ALTER TABLE `GeneralComments_tbl` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `Isolate_tbl`
--

DROP TABLE IF EXISTS `Isolate_tbl`;
SET @saved_cs_client     = @@character_set_client;
SET character_set_client = utf8;
CREATE TABLE `Isolate_tbl` (
  `isolate_pk` int(11) NOT NULL auto_increment,
  `expID` int(11) NOT NULL default '0',
  `isolateNumber` int(11) NOT NULL default '0',
  `isolateName` varchar(117) default 'NONE',
  `description` tinytext,
  `isolate_active` enum('YES','NO') NOT NULL default 'NO',
  `beingUsed` enum('YES','NO') NOT NULL default 'NO',
  `status` enum('ACTIVE','DEP') NOT NULL default 'ACTIVE',
  PRIMARY KEY  (`isolate_pk`),
  KEY `isolate_pk` (`isolate_pk`),
  KEY `expID` (`expID`),
  KEY `isolateNumber` (`isolateNumber`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1;
SET character_set_client = @saved_cs_client;

--
-- Dumping data for table `Isolate_tbl`
--

LOCK TABLES `Isolate_tbl` WRITE;
/*!40000 ALTER TABLE `Isolate_tbl` DISABLE KEYS */;
/*!40000 ALTER TABLE `Isolate_tbl` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `LabInfo_tbl`
--

DROP TABLE IF EXISTS `LabInfo_tbl`;
SET @saved_cs_client     = @@character_set_client;
SET character_set_client = utf8;
CREATE TABLE `LabInfo_tbl` (
  `labID` int(11) NOT NULL auto_increment,
  `lab_name` varchar(27) default NULL,
  `description` tinytext,
  `status` enum('ACTIVE','DEP') NOT NULL default 'ACTIVE',
  `default_access_level` int(11) NOT NULL default '4',
  `location` text,
  `lab_head` varchar(150) default NULL,
  `labCode` char(2) default NULL,
  PRIMARY KEY  (`labID`)
) ENGINE=MyISAM AUTO_INCREMENT=2 DEFAULT CHARSET=latin1;
SET character_set_client = @saved_cs_client;

--
-- Dumping data for table `LabInfo_tbl`
--

LOCK TABLES `LabInfo_tbl` WRITE;
/*!40000 ALTER TABLE `LabInfo_tbl` DISABLE KEYS */;
INSERT INTO `LabInfo_tbl` VALUES (1,'Admin','','ACTIVE',1,'','Admin','AD');
/*!40000 ALTER TABLE `LabInfo_tbl` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `LocationTypes_tbl`
--

DROP TABLE IF EXISTS `LocationTypes_tbl`;
SET @saved_cs_client     = @@character_set_client;
SET character_set_client = utf8;
CREATE TABLE `LocationTypes_tbl` (
  `locationTypeID` int(11) NOT NULL auto_increment,
  `locationTypeName` varchar(250) default NULL,
  `status` enum('ACTIVE','DEP') default 'ACTIVE',
  PRIMARY KEY  (`locationTypeID`)
) ENGINE=MyISAM AUTO_INCREMENT=4 DEFAULT CHARSET=latin1;
SET character_set_client = @saved_cs_client;

--
-- Dumping data for table `LocationTypes_tbl`
--

LOCK TABLES `LocationTypes_tbl` WRITE;
/*!40000 ALTER TABLE `LocationTypes_tbl` DISABLE KEYS */;
INSERT INTO `LocationTypes_tbl` VALUES (1,'Fridge','ACTIVE'),(2,'Freezer','ACTIVE'),(3,'To change, update LocationTypes_tbl in the database','ACTIVE');
/*!40000 ALTER TABLE `LocationTypes_tbl` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `LoginRecord_tbl`
--

DROP TABLE IF EXISTS `LoginRecord_tbl`;
SET @saved_cs_client     = @@character_set_client;
SET character_set_client = utf8;
CREATE TABLE `LoginRecord_tbl` (
  `crec_id` int(11) NOT NULL auto_increment,
  `userID` int(11) NOT NULL default '0',
  `timestamp` datetime default NULL,
  `user_ip` varchar(27) NOT NULL default '',
  `user_host` varchar(27) NOT NULL default '',
  `cookieID` varchar(27) default NULL,
  `sessionID` varchar(27) default NULL,
  `persistent` enum('T','F') NOT NULL default 'F',
  `status` enum('ACTIVE','DEP') NOT NULL default 'ACTIVE',
  PRIMARY KEY  (`crec_id`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1;
SET character_set_client = @saved_cs_client;

--
-- Dumping data for table `LoginRecord_tbl`
--

LOCK TABLES `LoginRecord_tbl` WRITE;
/*!40000 ALTER TABLE `LoginRecord_tbl` DISABLE KEYS */;
/*!40000 ALTER TABLE `LoginRecord_tbl` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `Method_tbl`
--

DROP TABLE IF EXISTS `Method_tbl`;
SET @saved_cs_client     = @@character_set_client;
SET character_set_client = utf8;
CREATE TABLE `Method_tbl` (
  `methodID` int(11) NOT NULL auto_increment,
  `oldID` int(11) default NULL,
  `method` text NOT NULL,
  `status` enum('ACTIVE','DEP') NOT NULL default 'ACTIVE',
  PRIMARY KEY  (`methodID`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1;
SET character_set_client = @saved_cs_client;

--
-- Dumping data for table `Method_tbl`
--

LOCK TABLES `Method_tbl` WRITE;
/*!40000 ALTER TABLE `Method_tbl` DISABLE KEYS */;
/*!40000 ALTER TABLE `Method_tbl` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `PacketOwners_tbl`
--

DROP TABLE IF EXISTS `PacketOwners_tbl`;
SET @saved_cs_client     = @@character_set_client;
SET character_set_client = utf8;
CREATE TABLE `PacketOwners_tbl` (
  `packetID` int(11) NOT NULL auto_increment,
  `firstName` varchar(27) NOT NULL default '',
  `lastName` varchar(27) NOT NULL default '',
  `packetName` varchar(27) NOT NULL default '',
  `description` int(11) default NULL,
  `labID` int(11) NOT NULL default '1',
  `status` enum('ACTIVE','DEP') NOT NULL default 'ACTIVE',
  `dateSubmitted` timestamp NOT NULL default CURRENT_TIMESTAMP on update CURRENT_TIMESTAMP,
  PRIMARY KEY  (`packetID`),
  KEY `packetID` (`packetID`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1;
SET character_set_client = @saved_cs_client;

--
-- Dumping data for table `PacketOwners_tbl`
--

LOCK TABLES `PacketOwners_tbl` WRITE;
/*!40000 ALTER TABLE `PacketOwners_tbl` DISABLE KEYS */;
/*!40000 ALTER TABLE `PacketOwners_tbl` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `Packets_tbl`
--

DROP TABLE IF EXISTS `Packets_tbl`;
SET @saved_cs_client     = @@character_set_client;
SET character_set_client = utf8;
CREATE TABLE `Packets_tbl` (
  `packetID` int(11) NOT NULL auto_increment,
  `ownerID` int(11) NOT NULL default '3',
  `packetName` varchar(250) NOT NULL default '',
  `status` enum('ACTIVE','DEP') NOT NULL default 'ACTIVE',
  `packetDescription` int(11) default NULL,
  `is_private` enum('TRUE','FALSE') default 'FALSE',
  PRIMARY KEY  (`packetID`),
  KEY `ownerID` (`ownerID`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1;
SET character_set_client = @saved_cs_client;

--
-- Dumping data for table `Packets_tbl`
--

LOCK TABLES `Packets_tbl` WRITE;
/*!40000 ALTER TABLE `Packets_tbl` DISABLE KEYS */;
/*!40000 ALTER TABLE `Packets_tbl` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `PrepElemTypes_tbl`
--

DROP TABLE IF EXISTS `PrepElemTypes_tbl`;
SET @saved_cs_client     = @@character_set_client;
SET character_set_client = utf8;
CREATE TABLE `PrepElemTypes_tbl` (
  `elementTypeID` int(11) NOT NULL auto_increment,
  `propertyName` varchar(100) NOT NULL default '',
  `PrepElementDesc` tinytext,
  `status` enum('ACTIVE','DEP') NOT NULL default 'ACTIVE',
  PRIMARY KEY  (`elementTypeID`),
  KEY `elementTypeID` (`elementTypeID`),
  KEY `propertyName` (`propertyName`)
) ENGINE=MyISAM AUTO_INCREMENT=51 DEFAULT CHARSET=latin1;
SET character_set_client = @saved_cs_client;

--
-- Dumping data for table `PrepElemTypes_tbl`
--

LOCK TABLES `PrepElemTypes_tbl` WRITE;
/*!40000 ALTER TABLE `PrepElemTypes_tbl` DISABLE KEYS */;
INSERT INTO `PrepElemTypes_tbl` VALUES (1,'OriginalID','The Original ID Contained In Well','ACTIVE'),(2,'Method ID','Method ID','ACTIVE'),(3,'Bacteria Strain','Bacteria Strain','ACTIVE'),(4,'Concentration','Concentration','ACTIVE'),(5,'Reagent Source','Reagent Source','ACTIVE'),(6,'Cell Line Name','Cell Line Name','ACTIVE'),(7,'Isolate Name','Isolate Name','ACTIVE'),(8,'Plates/Vial','Plates/Vial','ACTIVE'),(9,'Date','Date','ACTIVE'),(10,'Person','Person','ACTIVE'),(11,'Passage','Passage','ACTIVE'),(12,'Alternate ID','Isolate Name','ACTIVE'),(13,'Concentration','Isolate Name','DEP'),(14,'5\' digest/3\' Digest','Isolate Name','ACTIVE'),(44,'Bleed Number','Bleed Number','ACTIVE'),(45,'Date Stored','Date Stored','ACTIVE'),(46,'Expiry Date','Expiry Date','ACTIVE'),(47,'Lot Number','Lot Number','ACTIVE'),(48,'Total Volume','Total Volume','ACTIVE'),(49,'Host ID (name)','Host ID (name)','ACTIVE'),(50,'Number of Freeze/Thaws','Number of Freeze/Thaws','ACTIVE');
/*!40000 ALTER TABLE `PrepElemTypes_tbl` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `PrepElementProp_tbl`
--

DROP TABLE IF EXISTS `PrepElementProp_tbl`;
SET @saved_cs_client     = @@character_set_client;
SET character_set_client = utf8;
CREATE TABLE `PrepElementProp_tbl` (
  `prepPropID` int(11) NOT NULL auto_increment,
  `prepID` int(11) NOT NULL default '0',
  `elementTypeID` int(11) NOT NULL default '0',
  `value` varchar(27) NOT NULL default '',
  `labID` int(11) NOT NULL default '1',
  `status` enum('ACTIVE','DEP') NOT NULL default 'ACTIVE',
  PRIMARY KEY  (`prepPropID`),
  KEY `prepID` (`prepID`),
  KEY `elementTypeID` (`elementTypeID`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1;
SET character_set_client = @saved_cs_client;

--
-- Dumping data for table `PrepElementProp_tbl`
--

LOCK TABLES `PrepElementProp_tbl` WRITE;
/*!40000 ALTER TABLE `PrepElementProp_tbl` DISABLE KEYS */;
/*!40000 ALTER TABLE `PrepElementProp_tbl` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `Prep_Req_tbl`
--

DROP TABLE IF EXISTS `Prep_Req_tbl`;
SET @saved_cs_client     = @@character_set_client;
SET character_set_client = utf8;
CREATE TABLE `Prep_Req_tbl` (
  `prepReqID` int(11) NOT NULL auto_increment,
  `prepElementTypeID` int(11) NOT NULL default '0',
  `containerID` int(11) NOT NULL default '0',
  `requirement` enum('REQ','OPT') NOT NULL default 'OPT',
  `status` enum('ACTIVE','DEP') NOT NULL default 'ACTIVE',
  PRIMARY KEY  (`prepReqID`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1;
SET character_set_client = @saved_cs_client;

--
-- Dumping data for table `Prep_Req_tbl`
--

LOCK TABLES `Prep_Req_tbl` WRITE;
/*!40000 ALTER TABLE `Prep_Req_tbl` DISABLE KEYS */;
/*!40000 ALTER TABLE `Prep_Req_tbl` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `Prep_tbl`
--

DROP TABLE IF EXISTS `Prep_tbl`;
SET @saved_cs_client     = @@character_set_client;
SET character_set_client = utf8;
CREATE TABLE `Prep_tbl` (
  `prepID` int(11) NOT NULL auto_increment,
  `isolate_pk` int(11) NOT NULL default '0',
  `refAvailID` varchar(77) default NULL,
  `wellID` int(11) default NULL,
  `flag` enum('YES','NO') NOT NULL default 'NO',
  `comments` tinytext,
  `labID` int(11) NOT NULL default '1',
  `status` enum('ACTIVE','DEP') NOT NULL default 'ACTIVE',
  PRIMARY KEY  (`prepID`),
  KEY `prepID` (`prepID`),
  KEY `isolate_pk` (`isolate_pk`),
  KEY `refAvailID` (`refAvailID`),
  KEY `wellID` (`wellID`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1;
SET character_set_client = @saved_cs_client;

--
-- Dumping data for table `Prep_tbl`
--

LOCK TABLES `Prep_tbl` WRITE;
/*!40000 ALTER TABLE `Prep_tbl` DISABLE KEYS */;
/*!40000 ALTER TABLE `Prep_tbl` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `ProjectMembers_tbl`
--

DROP TABLE IF EXISTS `ProjectMembers_tbl`;
SET @saved_cs_client     = @@character_set_client;
SET character_set_client = utf8;
CREATE TABLE `ProjectMembers_tbl` (
  `pmID` int(11) NOT NULL auto_increment,
  `packetID` int(11) NOT NULL default '0',
  `memberID` int(11) NOT NULL default '0',
  `status` enum('ACTIVE','DEP') NOT NULL default 'ACTIVE',
  `role` enum('Reader','Writer') NOT NULL default 'Reader',
  PRIMARY KEY  (`pmID`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1;
SET character_set_client = @saved_cs_client;

--
-- Dumping data for table `ProjectMembers_tbl`
--

LOCK TABLES `ProjectMembers_tbl` WRITE;
/*!40000 ALTER TABLE `ProjectMembers_tbl` DISABLE KEYS */;
/*!40000 ALTER TABLE `ProjectMembers_tbl` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `PropertyReq_tbl`
--

DROP TABLE IF EXISTS `PropertyReq_tbl`;
SET @saved_cs_client     = @@character_set_client;
SET character_set_client = utf8;
CREATE TABLE `PropertyReq_tbl` (
  `propertyID` int(11) NOT NULL default '0',
  `reagentTypeID` int(11) NOT NULL default '0',
  `requirement` enum('REQ','OPT') NOT NULL default 'OPT',
  `status` enum('ACTIVE','DEP') NOT NULL default 'ACTIVE',
  PRIMARY KEY  (`propertyID`,`reagentTypeID`),
  KEY `reagentTypeID` (`reagentTypeID`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1;
SET character_set_client = @saved_cs_client;

--
-- Dumping data for table `PropertyReq_tbl`
--

LOCK TABLES `PropertyReq_tbl` WRITE;
/*!40000 ALTER TABLE `PropertyReq_tbl` DISABLE KEYS */;
/*!40000 ALTER TABLE `PropertyReq_tbl` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `ReagentPropList_tbl`
--

DROP TABLE IF EXISTS `ReagentPropList_tbl`;
SET @saved_cs_client     = @@character_set_client;
SET character_set_client = utf8;
CREATE TABLE `ReagentPropList_tbl` (
  `propListID` int(11) NOT NULL auto_increment,
  `reagentID` int(11) NOT NULL default '0',
  `propertyID` int(11) NOT NULL default '0',
  `propertyValue` text,
  `labID` int(11) NOT NULL default '1',
  `status` enum('ACTIVE','DEP') NOT NULL default 'ACTIVE',
  `startPos` int(11) default '0',
  `endPos` int(11) default '0',
  `direction` enum('forward','reverse') default 'forward',
  `descriptor` text,
  PRIMARY KEY  (`propListID`),
  KEY `reagentID` (`reagentID`),
  KEY `propertyID` (`propertyID`),
  KEY `status` (`status`),
  KEY `propListID` (`propListID`),
  FULLTEXT KEY `propertyValue` (`propertyValue`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1;
SET character_set_client = @saved_cs_client;

--
-- Dumping data for table `ReagentPropList_tbl`
--

LOCK TABLES `ReagentPropList_tbl` WRITE;
/*!40000 ALTER TABLE `ReagentPropList_tbl` DISABLE KEYS */;
/*!40000 ALTER TABLE `ReagentPropList_tbl` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `ReagentPropTypeCategories_tbl`
--

DROP TABLE IF EXISTS `ReagentPropTypeCategories_tbl`;
SET @saved_cs_client     = @@character_set_client;
SET character_set_client = utf8;
CREATE TABLE `ReagentPropTypeCategories_tbl` (
  `propertyCategoryID` int(11) NOT NULL auto_increment,
  `propertyCategoryName` varchar(250) default NULL,
  `status` enum('ACTIVE','DEP') default 'ACTIVE',
  `propertyCategoryAlias` varchar(250) default NULL,
  `ordering` int(11) NOT NULL default '2147483647',
  PRIMARY KEY  (`propertyCategoryID`),
  KEY `propertyCategoryName` (`propertyCategoryName`),
  KEY `status` (`status`),
  KEY `propertyCategoryAlias` (`propertyCategoryAlias`),
  KEY `ordering` (`ordering`)
) ENGINE=MyISAM AUTO_INCREMENT=12 DEFAULT CHARSET=latin1;
SET character_set_client = @saved_cs_client;

--
-- Dumping data for table `ReagentPropTypeCategories_tbl`
--

LOCK TABLES `ReagentPropTypeCategories_tbl` WRITE;
/*!40000 ALTER TABLE `ReagentPropTypeCategories_tbl` DISABLE KEYS */;
INSERT INTO `ReagentPropTypeCategories_tbl` VALUES (1,'General Properties','ACTIVE','general_properties',1),(2,'DNA Sequence','ACTIVE','sequence_properties',2),(4,'External Identifiers','ACTIVE','externalIDs',4),(5,'Classifiers','ACTIVE','classifiers',5),(3,'DNA Sequence Features','ACTIVE','dna_sequence_features',3),(8,'RNA Sequence','ACTIVE','rna_sequence_properties',8),(6,'Protein Sequence','ACTIVE','protein_sequence_properties',6),(7,'Protein Sequence Features','ACTIVE','protein_sequence_features',7),(9,'RNA Sequence Features','ACTIVE','rna_sequence_features',9),(10,'Growth Properties','ACTIVE','Growth_Properties',10),(11,'Protein Information','ACTIVE','Protein_Information',11);
/*!40000 ALTER TABLE `ReagentPropTypeCategories_tbl` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `ReagentPropType_tbl`
--

DROP TABLE IF EXISTS `ReagentPropType_tbl`;
SET @saved_cs_client     = @@character_set_client;
SET character_set_client = utf8;
CREATE TABLE `ReagentPropType_tbl` (
  `propertyID` int(11) NOT NULL auto_increment,
  `propertyName` varchar(250) NOT NULL default '',
  `propertyAlias` varchar(250) default NULL,
  `propertyDesc` tinytext,
  `status` enum('ACTIVE','DEP') NOT NULL default 'ACTIVE',
  `propertyColor` varchar(250) default NULL,
  `ordering` int(11) default '2147483647',
  PRIMARY KEY  (`propertyID`),
  KEY `propertyName` (`propertyName`),
  KEY `propertyType` (`propertyAlias`),
  KEY `status` (`status`),
  KEY `propertyID` (`propertyID`),
  KEY `propertyAlias` (`propertyAlias`)
) ENGINE=MyISAM AUTO_INCREMENT=73 DEFAULT CHARSET=latin1;
SET character_set_client = @saved_cs_client;

--
-- Dumping data for table `ReagentPropType_tbl`
--

LOCK TABLES `ReagentPropType_tbl` WRITE;
/*!40000 ALTER TABLE `ReagentPropType_tbl` DISABLE KEYS */;
INSERT INTO `ReagentPropType_tbl` VALUES (1,'5\' linker','5_prime_linker','5\' Linker','ACTIVE','#FF00FF',4),(2,'3\' linker','3_prime_linker','3\' Linker','ACTIVE','#FF00FF',5),(3,'status','status','Status','ACTIVE',NULL,2),(4,'verification','verification','Verification','ACTIVE',NULL,5),(5,'name','name','Name','ACTIVE',NULL,1),(6,'packet id','packet_id','Project ID','ACTIVE',NULL,3),(7,'tag','tag','Tag','ACTIVE','#008000',6),(8,'tag position','tag_position','Tag Position','ACTIVE',NULL,6),(9,'accession number','accession_number','Accession Number','ACTIVE',NULL,4),(12,'description','description','Description','ACTIVE',NULL,8),(13,'comments','comments','Comments','ACTIVE',NULL,7),(14,'5\' cloning site','5_prime_cloning_site','5\' Cloning Site','ACTIVE','#682860',2),(15,'3\' cloning site','3_prime_cloning_site','3\' Cloning Site','ACTIVE','#682860',3),(17,'reagent source','reagent_source','Reagent Source','ACTIVE',NULL,11),(18,'restrictions on use','restrictions_on_use','Restrictions on Use','ACTIVE',NULL,12),(19,'expression system','expression_system','Expression System','ACTIVE',NULL,8),(20,'promoter','promoter','Promoter','ACTIVE','#0A0668',7),(21,'open/closed','open_closed','Open/Closed','ACTIVE',NULL,5),(22,'protocol','protocol','Protocol','ACTIVE',NULL,5),(23,'sequence','sequence','Sequence','ACTIVE',NULL,2),(25,'length','length','Length','ACTIVE',NULL,2),(26,'oligo type','oligo_type','Oligo Type','ACTIVE',NULL,4),(28,'melting temperature','melting_temperature','Melting Temperature','ACTIVE',NULL,2),(29,'molecular weight','molecular_weight','Molecular Weight','ACTIVE',NULL,2),(31,'species','species','Species','ACTIVE',NULL,5),(32,'tissue type','tissue_type','Tissue Type','ACTIVE',NULL,5),(33,'developmental stage','developmental_stage','Developmental Stage','ACTIVE',NULL,5),(34,'morphology','morphology','Morphology','ACTIVE',NULL,5),(35,'alternate id','alternate_id','Alternate ID','ACTIVE',NULL,4),(36,'resistance marker','resistance_marker','Resistance Marker','ACTIVE',NULL,5),(38,'verification comments','verification_comments','Verification Comments','ACTIVE',NULL,10),(39,'entrez gene id','entrez_gene_id','Entrez Gene ID','ACTIVE',NULL,4),(40,'ensembl peptide id','entrez_peptide_id','Ensembl Peptide ID','ACTIVE',NULL,4),(46,'protein translation','protein_translation','Protein Translation','ACTIVE',NULL,2),(47,'vector type','vector_type','Vector type','ACTIVE',NULL,4),(48,'type of insert','type_of_insert','Type of insert','ACTIVE',NULL,5),(49,'cell line type','cell_line_type','Cell Line Type','ACTIVE',NULL,4),(50,'cloning method','cloning_method','Cloning Method','ACTIVE',NULL,5),(54,'ensembl gene id','ensembl_gene_id','Ensembl Gene ID','ACTIVE',NULL,4),(56,'official gene symbol','official_gene_symbol','Official Gene Symbol','ACTIVE',NULL,4),(57,'polya tail','polya_tail','PolyA Tail','ACTIVE','#A52A2A',11),(58,'origin of replication','origin_of_replication','Origin of Replication','ACTIVE','#6E999E',10),(59,'cdna insert','cdna_insert','cDNA','ACTIVE','#0000FF',1),(60,'selectable marker','selectable_marker','Selectable Marker','ACTIVE','#AEB404',8),(61,'miscellaneous','miscellaneous','Miscellaneous','ACTIVE','#FF7F50',9),(62,'restriction site','restriction_site','Restriction Site','ACTIVE','#08E8DE',14),(63,'transcription terminator','transcription_terminator','Transcription Terminator','ACTIVE','#01DF74',15),(64,'cleavage site','cleavage_site','Cleavage Site','ACTIVE','#FFC0CB',13),(65,'intron','intron','Intron','ACTIVE','#9932CC',12),(66,'protein sequence','protein_sequence','Protein Sequence','ACTIVE',NULL,2),(67,'rna sequence','rna_sequence','RNA Sequence','ACTIVE',NULL,2147483647),(68,'subculturing protocol','subculturing_protocol','Subculturing Protocol','ACTIVE',NULL,2147483647),(69,'culture media','culture_media','Culture Media','ACTIVE',NULL,2147483647),(70,'ensembl transcript id','ensembl_transcript_id','Ensembl Transcript ID','ACTIVE',NULL,2147483647),(71,'cloning strategy','cloning_strategy','Cloning Strategy','ACTIVE',NULL,2147483647),(72,'gc content','gc_content','GC content','ACTIVE',NULL,2147483647);
/*!40000 ALTER TABLE `ReagentPropType_tbl` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `ReagentPropertyCategories_tbl`
--

DROP TABLE IF EXISTS `ReagentPropertyCategories_tbl`;
SET @saved_cs_client     = @@character_set_client;
SET character_set_client = utf8;
CREATE TABLE `ReagentPropertyCategories_tbl` (
  `propCatID` int(11) NOT NULL auto_increment,
  `propID` int(11) NOT NULL,
  `categoryID` int(11) NOT NULL,
  `status` enum('ACTIVE','DEP') NOT NULL default 'ACTIVE',
  PRIMARY KEY  (`propCatID`),
  KEY `propID` (`propID`),
  KEY `categoryID` (`categoryID`),
  KEY `propCatID` (`propCatID`),
  KEY `status` (`status`)
) ENGINE=MyISAM AUTO_INCREMENT=2296 DEFAULT CHARSET=latin1;
SET character_set_client = @saved_cs_client;

--
-- Dumping data for table `ReagentPropertyCategories_tbl`
--

LOCK TABLES `ReagentPropertyCategories_tbl` WRITE;
/*!40000 ALTER TABLE `ReagentPropertyCategories_tbl` DISABLE KEYS */;
INSERT INTO `ReagentPropertyCategories_tbl` VALUES (2186,1,3,'ACTIVE'),(2187,2,3,'ACTIVE'),(2188,3,1,'ACTIVE'),(2189,4,1,'ACTIVE'),(2190,5,1,'ACTIVE'),(2191,6,1,'ACTIVE'),(2192,7,3,'ACTIVE'),(2193,8,3,'ACTIVE'),(2194,9,4,'ACTIVE'),(2195,12,1,'ACTIVE'),(2196,13,1,'ACTIVE'),(2197,14,3,'ACTIVE'),(2198,15,3,'ACTIVE'),(2199,17,1,'ACTIVE'),(2200,18,1,'ACTIVE'),(2201,19,3,'ACTIVE'),(2202,20,3,'ACTIVE'),(2203,21,5,'ACTIVE'),(2204,22,5,'ACTIVE'),(2205,23,2,'ACTIVE'),(2206,25,2,'ACTIVE'),(2207,26,1,'ACTIVE'),(2208,28,2,'ACTIVE'),(2209,29,2,'ACTIVE'),(2210,31,5,'ACTIVE'),(2211,32,5,'ACTIVE'),(2212,33,5,'ACTIVE'),(2213,34,5,'ACTIVE'),(2214,35,4,'ACTIVE'),(2215,38,1,'ACTIVE'),(2216,39,4,'ACTIVE'),(2217,40,4,'ACTIVE'),(2218,46,2,'ACTIVE'),(2219,47,1,'ACTIVE'),(2220,48,5,'ACTIVE'),(2221,49,1,'ACTIVE'),(2222,50,5,'ACTIVE'),(2223,54,4,'ACTIVE'),(2224,56,4,'ACTIVE'),(2225,57,3,'ACTIVE'),(2226,58,3,'ACTIVE'),(2227,59,3,'ACTIVE'),(2228,60,3,'ACTIVE'),(2230,61,3,'ACTIVE'),(2231,62,3,'ACTIVE'),(2232,63,3,'ACTIVE'),(2233,64,3,'ACTIVE'),(2234,65,3,'ACTIVE'),(2235,66,6,'ACTIVE'),(2236,67,8,'ACTIVE'),(2237,28,6,'DEP'),(2238,28,8,'DEP'),(2239,29,6,'ACTIVE'),(2240,29,8,'DEP'),(2243,7,8,'ACTIVE'),(2267,28,6,'DEP'),(2271,61,7,'DEP'),(2272,64,7,'DEP'),(2275,8,9,'DEP'),(2276,7,9,'DEP'),(2277,61,9,'DEP'),(2281,60,10,'ACTIVE'),(2282,68,10,'ACTIVE'),(2283,69,10,'ACTIVE'),(2286,8,7,'ACTIVE'),(2287,7,7,'ACTIVE'),(2288,61,7,'ACTIVE'),(2294,8,9,'ACTIVE'),(2295,7,9,'ACTIVE');
/*!40000 ALTER TABLE `ReagentPropertyCategories_tbl` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `ReagentTypeAttribute_Set_tbl`
--

DROP TABLE IF EXISTS `ReagentTypeAttribute_Set_tbl`;
SET @saved_cs_client     = @@character_set_client;
SET character_set_client = utf8;
CREATE TABLE `ReagentTypeAttribute_Set_tbl` (
  `ssetID` int(11) NOT NULL default '0',
  `reagentTypeAttributeID` int(11) NOT NULL,
  `status` enum('ACTIVE','DEP') default 'ACTIVE',
  `reagentTypeAttributeSetID` int(11) NOT NULL auto_increment,
  PRIMARY KEY  (`reagentTypeAttributeSetID`),
  KEY `reagentTypeAttributeID` (`reagentTypeAttributeID`)
) ENGINE=MyISAM AUTO_INCREMENT=3550 DEFAULT CHARSET=latin1;
SET character_set_client = @saved_cs_client;

--
-- Dumping data for table `ReagentTypeAttribute_Set_tbl`
--

LOCK TABLES `ReagentTypeAttribute_Set_tbl` WRITE;
/*!40000 ALTER TABLE `ReagentTypeAttribute_Set_tbl` DISABLE KEYS */;
INSERT INTO `ReagentTypeAttribute_Set_tbl` VALUES (1,4707,'ACTIVE',2523),(7,4707,'ACTIVE',2524),(4,4707,'ACTIVE',2525),(2,4707,'ACTIVE',2526),(5,4707,'ACTIVE',2527),(6,4707,'ACTIVE',2528),(3,4707,'ACTIVE',2529),(141,4775,'ACTIVE',2530),(142,4775,'ACTIVE',2531),(12,4776,'ACTIVE',2532),(722,4776,'ACTIVE',2533),(10,4776,'ACTIVE',2534),(8,4776,'ACTIVE',2535),(723,4776,'ACTIVE',2536),(724,4776,'ACTIVE',2537),(685,4781,'ACTIVE',2538),(686,4781,'ACTIVE',2539),(364,4808,'ACTIVE',2542),(367,4808,'ACTIVE',2543),(366,4808,'ACTIVE',2544),(365,4808,'ACTIVE',2545),(363,4808,'ACTIVE',2546),(117,4786,'ACTIVE',2547),(118,4786,'ACTIVE',2548),(119,4786,'ACTIVE',2549),(120,4786,'ACTIVE',2550),(121,4786,'ACTIVE',2551),(122,4786,'ACTIVE',2552),(123,4786,'ACTIVE',2553),(124,4786,'ACTIVE',2554),(580,4786,'ACTIVE',2555),(1,4701,'ACTIVE',2556),(2,4701,'ACTIVE',2557),(3,4701,'ACTIVE',2558),(4,4701,'ACTIVE',2559),(5,4701,'ACTIVE',2560),(6,4701,'ACTIVE',2561),(7,4701,'ACTIVE',2562),(86,4712,'ACTIVE',2563),(87,4712,'ACTIVE',2564),(88,4712,'ACTIVE',2565),(89,4712,'ACTIVE',2566),(90,4712,'ACTIVE',2567),(91,4712,'ACTIVE',2568),(92,4712,'ACTIVE',2569),(93,4712,'ACTIVE',2570),(94,4712,'ACTIVE',2571),(95,4712,'ACTIVE',2572),(96,4712,'ACTIVE',2573),(97,4712,'ACTIVE',2574),(98,4712,'ACTIVE',2575),(8,4713,'ACTIVE',2576),(9,4713,'ACTIVE',2577),(10,4713,'ACTIVE',2578),(11,4713,'ACTIVE',2579),(12,4713,'ACTIVE',2580),(13,4713,'ACTIVE',2581),(14,4713,'ACTIVE',2582),(684,4713,'ACTIVE',2583),(685,4718,'ACTIVE',2584),(686,4718,'ACTIVE',2585),(322,4735,'ACTIVE',2586),(316,4735,'ACTIVE',2587),(318,4735,'ACTIVE',2588),(438,4735,'ACTIVE',2589),(419,4735,'ACTIVE',2590),(418,4735,'ACTIVE',2591),(319,4735,'ACTIVE',2592),(396,4735,'ACTIVE',2593),(439,4735,'ACTIVE',2594),(320,4735,'ACTIVE',2595),(321,4735,'ACTIVE',2596),(323,4735,'ACTIVE',2597),(308,4730,'ACTIVE',2598),(309,4730,'ACTIVE',2599),(310,4730,'ACTIVE',2600),(311,4730,'ACTIVE',2601),(397,4730,'ACTIVE',2602),(312,4730,'ACTIVE',2603),(313,4730,'ACTIVE',2604),(315,4730,'ACTIVE',2605),(1110,4730,'ACTIVE',2606),(1111,4730,'ACTIVE',2607),(1112,4730,'ACTIVE',2608),(1131,4730,'ACTIVE',2609),(1136,4730,'ACTIVE',2610),(294,4731,'ACTIVE',2611),(301,4731,'ACTIVE',2612),(304,4731,'ACTIVE',2613),(295,4731,'ACTIVE',2614),(298,4731,'ACTIVE',2615),(711,4731,'ACTIVE',2616),(299,4731,'ACTIVE',2617),(296,4731,'ACTIVE',2618),(305,4731,'ACTIVE',2619),(489,4731,'ACTIVE',2620),(306,4731,'ACTIVE',2621),(297,4731,'ACTIVE',2622),(303,4731,'ACTIVE',2623),(300,4731,'ACTIVE',2624),(267,4731,'ACTIVE',2625),(307,4731,'ACTIVE',2626),(302,4731,'ACTIVE',2627),(1038,4731,'ACTIVE',2628),(517,4732,'ACTIVE',2629),(292,4732,'ACTIVE',2630),(291,4732,'ACTIVE',2631),(290,4732,'ACTIVE',2632),(293,4732,'ACTIVE',2633),(24,4729,'ACTIVE',2634),(25,4729,'ACTIVE',2635),(26,4729,'ACTIVE',2636),(27,4729,'ACTIVE',2637),(28,4729,'ACTIVE',2638),(29,4729,'ACTIVE',2639),(151,4729,'ACTIVE',2640),(154,4729,'ACTIVE',2641),(270,4729,'ACTIVE',2642),(48,4726,'ACTIVE',2643),(49,4726,'ACTIVE',2644),(51,4726,'ACTIVE',2645),(52,4726,'ACTIVE',2646),(53,4726,'ACTIVE',2647),(54,4726,'ACTIVE',2648),(55,4726,'ACTIVE',2649),(56,4726,'ACTIVE',2650),(57,4726,'ACTIVE',2651),(61,4726,'ACTIVE',2652),(62,4726,'ACTIVE',2653),(63,4726,'ACTIVE',2654),(64,4726,'ACTIVE',2655),(67,4726,'ACTIVE',2656),(69,4726,'ACTIVE',2657),(70,4726,'ACTIVE',2658),(71,4726,'ACTIVE',2659),(72,4726,'ACTIVE',2660),(77,4726,'ACTIVE',2661),(78,4726,'ACTIVE',2662),(156,4726,'ACTIVE',2663),(259,4726,'ACTIVE',2664),(260,4726,'ACTIVE',2665),(261,4726,'ACTIVE',2666),(262,4726,'ACTIVE',2667),(264,4726,'ACTIVE',2668),(265,4726,'ACTIVE',2669),(266,4726,'ACTIVE',2670),(280,4726,'ACTIVE',2671),(407,4726,'ACTIVE',2672),(409,4726,'ACTIVE',2673),(414,4726,'ACTIVE',2674),(485,4726,'ACTIVE',2675),(514,4726,'ACTIVE',2676),(515,4726,'ACTIVE',2677),(516,4726,'ACTIVE',2678),(630,4726,'ACTIVE',2679),(791,4726,'ACTIVE',2680),(792,4726,'ACTIVE',2681),(793,4726,'ACTIVE',2682),(797,4726,'ACTIVE',2683),(798,4726,'ACTIVE',2684),(799,4726,'ACTIVE',2685),(324,4734,'ACTIVE',2686),(325,4734,'ACTIVE',2687),(368,4734,'ACTIVE',2688),(369,4734,'ACTIVE',2689),(370,4734,'ACTIVE',2690),(443,4734,'ACTIVE',2691),(665,4734,'ACTIVE',2692),(327,4734,'ACTIVE',2693),(328,4734,'ACTIVE',2694),(436,4734,'ACTIVE',2695),(331,4734,'ACTIVE',2696),(425,4734,'ACTIVE',2697),(371,4734,'ACTIVE',2698),(332,4734,'ACTIVE',2699),(372,4734,'ACTIVE',2700),(373,4734,'ACTIVE',2701),(374,4734,'ACTIVE',2702),(333,4734,'ACTIVE',2703),(375,4734,'ACTIVE',2704),(376,4734,'ACTIVE',2705),(377,4734,'ACTIVE',2706),(378,4734,'ACTIVE',2707),(379,4734,'ACTIVE',2708),(380,4734,'ACTIVE',2709),(334,4734,'ACTIVE',2710),(336,4734,'ACTIVE',2711),(542,4734,'ACTIVE',2712),(337,4734,'ACTIVE',2713),(340,4734,'ACTIVE',2714),(381,4734,'ACTIVE',2715),(629,4734,'ACTIVE',2716),(341,4734,'ACTIVE',2717),(342,4734,'ACTIVE',2718),(422,4734,'ACTIVE',2719),(423,4734,'ACTIVE',2720),(424,4734,'ACTIVE',2721),(343,4734,'ACTIVE',2722),(344,4734,'ACTIVE',2723),(345,4734,'ACTIVE',2724),(872,4734,'ACTIVE',2725),(873,4734,'ACTIVE',2726),(437,4734,'ACTIVE',2727),(444,4734,'ACTIVE',2728),(346,4734,'ACTIVE',2729),(382,4734,'ACTIVE',2730),(348,4734,'ACTIVE',2731),(420,4734,'ACTIVE',2732),(421,4734,'ACTIVE',2733),(349,4734,'ACTIVE',2734),(350,4734,'ACTIVE',2735),(385,4734,'ACTIVE',2736),(874,4734,'ACTIVE',2737),(875,4734,'ACTIVE',2738),(441,4734,'ACTIVE',2739),(352,4734,'ACTIVE',2740),(442,4734,'ACTIVE',2741),(353,4734,'ACTIVE',2742),(386,4734,'ACTIVE',2743),(387,4734,'ACTIVE',2744),(354,4734,'ACTIVE',2745),(388,4734,'ACTIVE',2746),(445,4734,'ACTIVE',2747),(440,4734,'ACTIVE',2748),(389,4734,'ACTIVE',2749),(390,4734,'ACTIVE',2750),(391,4734,'ACTIVE',2751),(392,4734,'ACTIVE',2752),(359,4734,'ACTIVE',2753),(393,4734,'ACTIVE',2754),(360,4734,'ACTIVE',2755),(394,4734,'ACTIVE',2756),(395,4734,'ACTIVE',2757),(1007,4734,'ACTIVE',2758),(1008,4734,'ACTIVE',2759),(1036,4734,'ACTIVE',2760),(1037,4734,'ACTIVE',2761),(1039,4734,'ACTIVE',2762),(1040,4734,'ACTIVE',2763),(1044,4734,'ACTIVE',2764),(1045,4734,'ACTIVE',2765),(1046,4734,'ACTIVE',2766),(1048,4734,'ACTIVE',2767),(1049,4734,'ACTIVE',2768),(1050,4734,'ACTIVE',2769),(1051,4734,'ACTIVE',2770),(1052,4734,'ACTIVE',2771),(1055,4734,'ACTIVE',2772),(1056,4734,'ACTIVE',2773),(1057,4734,'ACTIVE',2774),(1058,4734,'ACTIVE',2775),(1059,4734,'ACTIVE',2776),(1060,4734,'ACTIVE',2777),(1061,4734,'ACTIVE',2778),(1062,4734,'ACTIVE',2779),(1063,4734,'ACTIVE',2780),(1064,4734,'ACTIVE',2781),(1065,4734,'ACTIVE',2782),(1066,4734,'ACTIVE',2783),(1067,4734,'ACTIVE',2784),(1068,4734,'ACTIVE',2785),(1069,4734,'ACTIVE',2786),(1070,4734,'ACTIVE',2787),(1071,4734,'ACTIVE',2788),(1072,4734,'ACTIVE',2789),(1073,4734,'ACTIVE',2790),(1074,4734,'ACTIVE',2791),(1075,4734,'ACTIVE',2792),(1076,4734,'ACTIVE',2793),(1077,4734,'ACTIVE',2794),(1078,4734,'ACTIVE',2795),(1097,4734,'ACTIVE',2796),(1098,4734,'ACTIVE',2797),(1099,4734,'ACTIVE',2798),(1100,4734,'ACTIVE',2799),(1101,4734,'ACTIVE',2800),(1102,4734,'ACTIVE',2801),(1103,4734,'ACTIVE',2802),(1104,4734,'ACTIVE',2803),(1105,4734,'ACTIVE',2804),(1107,4734,'ACTIVE',2805),(1108,4734,'ACTIVE',2806),(1109,4734,'ACTIVE',2807),(1113,4734,'ACTIVE',2808),(1115,4734,'ACTIVE',2809),(1117,4734,'ACTIVE',2810),(1118,4734,'ACTIVE',2811),(1119,4734,'ACTIVE',2812),(1120,4734,'ACTIVE',2813),(1121,4734,'ACTIVE',2814),(1122,4734,'ACTIVE',2815),(1123,4734,'ACTIVE',2816),(1124,4734,'ACTIVE',2817),(1125,4734,'ACTIVE',2818),(1126,4734,'ACTIVE',2819),(1127,4734,'ACTIVE',2820),(1128,4734,'ACTIVE',2821),(1129,4734,'ACTIVE',2822),(1132,4734,'ACTIVE',2823),(1133,4734,'ACTIVE',2824),(1134,4734,'ACTIVE',2825),(1137,4734,'ACTIVE',2826),(1138,4734,'ACTIVE',2827),(1139,4734,'ACTIVE',2828),(1140,4734,'ACTIVE',2829),(1141,4734,'ACTIVE',2830),(1142,4734,'ACTIVE',2831),(1143,4734,'ACTIVE',2832),(1144,4734,'ACTIVE',2833),(1145,4734,'ACTIVE',2834),(1146,4734,'ACTIVE',2835),(1147,4734,'ACTIVE',2836),(1148,4734,'ACTIVE',2837),(31,4728,'ACTIVE',2838),(32,4728,'ACTIVE',2839),(33,4728,'ACTIVE',2840),(35,4728,'ACTIVE',2841),(36,4728,'ACTIVE',2842),(38,4728,'ACTIVE',2843),(39,4728,'ACTIVE',2844),(46,4728,'ACTIVE',2845),(160,4728,'ACTIVE',2846),(274,4728,'ACTIVE',2847),(279,4728,'ACTIVE',2848),(398,4728,'ACTIVE',2849),(399,4728,'ACTIVE',2850),(400,4728,'ACTIVE',2851),(401,4728,'ACTIVE',2852),(402,4728,'ACTIVE',2853),(403,4728,'ACTIVE',2854),(404,4728,'ACTIVE',2855),(405,4728,'ACTIVE',2856),(415,4728,'ACTIVE',2857),(1042,4728,'ACTIVE',2858),(1043,4728,'ACTIVE',2859),(1041,4728,'ACTIVE',2860),(1114,4728,'ACTIVE',2861),(1116,4728,'ACTIVE',2862),(1130,4728,'ACTIVE',2863),(1135,4728,'ACTIVE',2864),(1150,4728,'ACTIVE',2865),(364,4733,'ACTIVE',2866),(367,4733,'ACTIVE',2867),(366,4733,'ACTIVE',2868),(365,4733,'ACTIVE',2869),(363,4733,'ACTIVE',2870),(361,4737,'ACTIVE',2871),(410,4737,'ACTIVE',2872),(411,4737,'ACTIVE',2873),(412,4737,'ACTIVE',2874),(362,4737,'ACTIVE',2875),(81,4727,'ACTIVE',2876),(82,4727,'ACTIVE',2877),(83,4727,'ACTIVE',2878),(85,4727,'ACTIVE',2879),(584,4727,'ACTIVE',2880),(1,4706,'ACTIVE',2940),(7,4706,'ACTIVE',2941),(4,4706,'ACTIVE',2942),(2,4706,'ACTIVE',2943),(5,4706,'ACTIVE',2944),(6,4706,'ACTIVE',2945),(3,4706,'ACTIVE',2946),(11,4738,'ACTIVE',2947),(12,4738,'ACTIVE',2948),(722,4738,'ACTIVE',2949),(9,4738,'ACTIVE',2950),(13,4738,'ACTIVE',2951),(10,4738,'ACTIVE',2952),(14,4738,'ACTIVE',2953),(8,4738,'ACTIVE',2954),(685,4743,'ACTIVE',2955),(686,4743,'ACTIVE',2956),(322,4762,'ACTIVE',2957),(316,4762,'ACTIVE',2958),(318,4762,'ACTIVE',2959),(438,4762,'ACTIVE',2960),(419,4762,'ACTIVE',2961),(418,4762,'ACTIVE',2962),(319,4762,'ACTIVE',2963),(396,4762,'ACTIVE',2964),(439,4762,'ACTIVE',2965),(320,4762,'ACTIVE',2966),(321,4762,'ACTIVE',2967),(323,4762,'ACTIVE',2968),(308,4757,'ACTIVE',2969),(309,4757,'ACTIVE',2970),(310,4757,'ACTIVE',2971),(311,4757,'ACTIVE',2972),(397,4757,'ACTIVE',2973),(312,4757,'ACTIVE',2974),(313,4757,'ACTIVE',2975),(315,4757,'ACTIVE',2976),(294,4758,'ACTIVE',2977),(1038,4758,'ACTIVE',2978),(301,4758,'ACTIVE',2979),(304,4758,'ACTIVE',2980),(295,4758,'ACTIVE',2981),(298,4758,'ACTIVE',2982),(711,4758,'ACTIVE',2983),(299,4758,'ACTIVE',2984),(296,4758,'ACTIVE',2985),(305,4758,'ACTIVE',2986),(489,4758,'ACTIVE',2987),(306,4758,'ACTIVE',2988),(297,4758,'ACTIVE',2989),(303,4758,'ACTIVE',2990),(300,4758,'ACTIVE',2991),(267,4758,'ACTIVE',2992),(307,4758,'ACTIVE',2993),(302,4758,'ACTIVE',2994),(517,4759,'ACTIVE',2995),(292,4759,'ACTIVE',2996),(291,4759,'ACTIVE',2997),(290,4759,'ACTIVE',2998),(293,4759,'ACTIVE',2999),(24,4756,'ACTIVE',3000),(270,4756,'ACTIVE',3001),(27,4756,'ACTIVE',3002),(26,4756,'ACTIVE',3003),(25,4756,'ACTIVE',3004),(29,4756,'ACTIVE',3005),(154,4756,'ACTIVE',3006),(28,4756,'ACTIVE',3007),(151,4756,'ACTIVE',3008),(71,4753,'ACTIVE',3009),(407,4753,'ACTIVE',3010),(72,4753,'ACTIVE',3011),(62,4753,'ACTIVE',3012),(63,4753,'ACTIVE',3013),(61,4753,'ACTIVE',3014),(51,4753,'ACTIVE',3015),(57,4753,'ACTIVE',3016),(56,4753,'ACTIVE',3017),(55,4753,'ACTIVE',3018),(53,4753,'ACTIVE',3019),(54,4753,'ACTIVE',3020),(630,4753,'ACTIVE',3021),(67,4753,'ACTIVE',3022),(265,4753,'ACTIVE',3023),(77,4753,'ACTIVE',3024),(409,4753,'ACTIVE',3025),(261,4753,'ACTIVE',3026),(260,4753,'ACTIVE',3027),(264,4753,'ACTIVE',3028),(262,4753,'ACTIVE',3029),(729,4753,'ACTIVE',3030),(514,4753,'ACTIVE',3031),(280,4753,'ACTIVE',3032),(266,4753,'ACTIVE',3033),(69,4753,'ACTIVE',3034),(78,4753,'ACTIVE',3035),(70,4753,'ACTIVE',3036),(48,4753,'ACTIVE',3037),(52,4753,'ACTIVE',3038),(414,4753,'ACTIVE',3039),(485,4753,'ACTIVE',3040),(156,4753,'ACTIVE',3041),(515,4753,'ACTIVE',3042),(516,4753,'ACTIVE',3043),(49,4753,'ACTIVE',3044),(64,4753,'ACTIVE',3045),(259,4753,'ACTIVE',3046),(324,4764,'ACTIVE',3047),(325,4764,'ACTIVE',3048),(368,4764,'ACTIVE',3049),(369,4764,'ACTIVE',3050),(370,4764,'ACTIVE',3051),(443,4764,'ACTIVE',3052),(665,4764,'ACTIVE',3053),(327,4764,'ACTIVE',3054),(328,4764,'ACTIVE',3055),(436,4764,'ACTIVE',3056),(331,4764,'ACTIVE',3057),(425,4764,'ACTIVE',3058),(371,4764,'ACTIVE',3059),(332,4764,'ACTIVE',3060),(372,4764,'ACTIVE',3061),(373,4764,'ACTIVE',3062),(374,4764,'ACTIVE',3063),(1036,4764,'ACTIVE',3064),(333,4764,'ACTIVE',3065),(375,4764,'ACTIVE',3066),(376,4764,'ACTIVE',3067),(377,4764,'ACTIVE',3068),(378,4764,'ACTIVE',3069),(379,4764,'ACTIVE',3070),(380,4764,'ACTIVE',3071),(334,4764,'ACTIVE',3072),(336,4764,'ACTIVE',3073),(542,4764,'ACTIVE',3074),(337,4764,'ACTIVE',3075),(1039,4764,'ACTIVE',3076),(340,4764,'ACTIVE',3077),(381,4764,'ACTIVE',3078),(1007,4764,'ACTIVE',3079),(1008,4764,'ACTIVE',3080),(629,4764,'ACTIVE',3081),(341,4764,'ACTIVE',3082),(342,4764,'ACTIVE',3083),(1040,4764,'ACTIVE',3084),(422,4764,'ACTIVE',3085),(423,4764,'ACTIVE',3086),(424,4764,'ACTIVE',3087),(343,4764,'ACTIVE',3088),(344,4764,'ACTIVE',3089),(345,4764,'ACTIVE',3090),(872,4764,'ACTIVE',3091),(873,4764,'ACTIVE',3092),(437,4764,'ACTIVE',3093),(444,4764,'ACTIVE',3094),(346,4764,'ACTIVE',3095),(382,4764,'ACTIVE',3096),(348,4764,'ACTIVE',3097),(420,4764,'ACTIVE',3098),(421,4764,'ACTIVE',3099),(349,4764,'ACTIVE',3100),(350,4764,'ACTIVE',3101),(385,4764,'ACTIVE',3102),(874,4764,'ACTIVE',3103),(875,4764,'ACTIVE',3104),(441,4764,'ACTIVE',3105),(352,4764,'ACTIVE',3106),(442,4764,'ACTIVE',3107),(353,4764,'ACTIVE',3108),(386,4764,'ACTIVE',3109),(387,4764,'ACTIVE',3110),(354,4764,'ACTIVE',3111),(388,4764,'ACTIVE',3112),(445,4764,'ACTIVE',3113),(1037,4764,'ACTIVE',3114),(440,4764,'ACTIVE',3115),(389,4764,'ACTIVE',3116),(390,4764,'ACTIVE',3117),(391,4764,'ACTIVE',3118),(392,4764,'ACTIVE',3119),(359,4764,'ACTIVE',3120),(393,4764,'ACTIVE',3121),(360,4764,'ACTIVE',3122),(394,4764,'ACTIVE',3123),(395,4764,'ACTIVE',3124),(279,4755,'ACTIVE',3125),(46,4755,'ACTIVE',3126),(398,4755,'ACTIVE',3127),(31,4755,'ACTIVE',3128),(274,4755,'ACTIVE',3129),(399,4755,'ACTIVE',3130),(415,4755,'ACTIVE',3131),(400,4755,'ACTIVE',3132),(160,4755,'ACTIVE',3133),(401,4755,'ACTIVE',3134),(1042,4755,'ACTIVE',3135),(33,4755,'ACTIVE',3136),(1043,4755,'ACTIVE',3137),(405,4755,'ACTIVE',3138),(35,4755,'ACTIVE',3139),(36,4755,'ACTIVE',3140),(402,4755,'ACTIVE',3141),(403,4755,'ACTIVE',3142),(32,4755,'ACTIVE',3143),(38,4755,'ACTIVE',3144),(1041,4755,'ACTIVE',3145),(39,4755,'ACTIVE',3146),(404,4755,'ACTIVE',3147),(364,4763,'ACTIVE',3148),(367,4763,'ACTIVE',3149),(366,4763,'ACTIVE',3150),(365,4763,'ACTIVE',3151),(363,4763,'ACTIVE',3152),(361,4761,'ACTIVE',3153),(410,4761,'ACTIVE',3154),(411,4761,'ACTIVE',3155),(412,4761,'ACTIVE',3156),(362,4761,'ACTIVE',3157),(81,4754,'ACTIVE',3158),(584,4754,'ACTIVE',3159),(85,4754,'ACTIVE',3160),(82,4754,'ACTIVE',3161),(83,4754,'ACTIVE',3162),(145,4768,'ACTIVE',3163),(146,4768,'ACTIVE',3164),(147,4768,'ACTIVE',3165),(148,4768,'ACTIVE',3166),(149,4768,'ACTIVE',3167),(152,4768,'ACTIVE',3168),(153,4768,'ACTIVE',3169),(157,4768,'ACTIVE',3170),(245,4768,'ACTIVE',3171),(283,4768,'ACTIVE',3172),(286,4768,'ACTIVE',3173),(704,4768,'ACTIVE',3174),(110,4774,'ACTIVE',3175),(111,4774,'ACTIVE',3176),(112,4774,'ACTIVE',3177),(113,4774,'ACTIVE',3178),(114,4774,'ACTIVE',3179),(115,4774,'ACTIVE',3180),(116,4774,'ACTIVE',3181),(144,4774,'ACTIVE',3182),(150,4774,'ACTIVE',3183),(288,4774,'ACTIVE',3184),(105,4772,'ACTIVE',3185),(106,4772,'ACTIVE',3186),(107,4772,'ACTIVE',3187),(108,4772,'ACTIVE',3188),(109,4772,'ACTIVE',3189),(128,4773,'ACTIVE',3190),(129,4773,'ACTIVE',3191),(130,4773,'ACTIVE',3192),(161,4773,'ACTIVE',3193),(162,4773,'ACTIVE',3194),(163,4773,'ACTIVE',3195),(164,4773,'ACTIVE',3196),(165,4773,'ACTIVE',3197),(244,4773,'ACTIVE',3198),(426,4773,'ACTIVE',3199),(99,4771,'ACTIVE',3200),(100,4771,'ACTIVE',3201),(101,4771,'ACTIVE',3202),(102,4771,'ACTIVE',3203),(103,4771,'ACTIVE',3204),(104,4771,'ACTIVE',3205),(727,4736,'ACTIVE',3206),(1227,4734,'ACTIVE',3247),(1228,4734,'ACTIVE',3248),(1229,4734,'ACTIVE',3249),(1230,4734,'ACTIVE',3250),(948,4736,'ACTIVE',3251),(1149,4736,'ACTIVE',3252),(1231,4734,'ACTIVE',3253),(1232,4734,'ACTIVE',3254),(1233,4734,'ACTIVE',3255),(1234,4734,'ACTIVE',3256),(1240,4734,'ACTIVE',3262),(1241,4734,'ACTIVE',3263),(1242,4734,'ACTIVE',3264),(1243,4734,'ACTIVE',3265),(1244,4734,'ACTIVE',3266),(1245,4734,'ACTIVE',3267),(1246,4734,'ACTIVE',3268),(1247,4734,'ACTIVE',3269),(1248,4736,'ACTIVE',3270),(1249,4734,'ACTIVE',3271),(1250,4734,'ACTIVE',3272),(1251,4734,'ACTIVE',3273),(1252,4734,'ACTIVE',3274),(1253,4734,'ACTIVE',3275),(1254,4734,'ACTIVE',3276),(1053,4736,'ACTIVE',3277),(1255,4736,'ACTIVE',3278),(1256,4736,'ACTIVE',3279),(1257,4736,'ACTIVE',3280),(1054,4736,'ACTIVE',3281),(1258,4734,'ACTIVE',3282),(1259,4734,'ACTIVE',3283),(1260,4734,'ACTIVE',3284),(1261,4734,'ACTIVE',3285),(1262,4734,'ACTIVE',3286),(1263,4734,'ACTIVE',3287),(1264,4734,'ACTIVE',3288),(1265,4734,'ACTIVE',3289),(1266,4734,'ACTIVE',3290),(1267,4734,'ACTIVE',3291),(728,4736,'ACTIVE',3292),(739,4736,'ACTIVE',3293),(1268,4734,'ACTIVE',3294),(1269,4734,'ACTIVE',3295),(1270,4734,'ACTIVE',3296),(1271,4734,'ACTIVE',3297),(1272,4734,'ACTIVE',3298),(1273,4713,'ACTIVE',3299),(1274,4734,'ACTIVE',3300),(1275,4734,'ACTIVE',3301),(1276,4734,'ACTIVE',3302),(1277,4734,'ACTIVE',3303),(1278,4734,'ACTIVE',3304),(1279,4734,'ACTIVE',3305),(1280,4734,'ACTIVE',3306),(1281,4736,'ACTIVE',3307),(1282,4736,'ACTIVE',3308),(1283,4734,'ACTIVE',3309),(1284,4734,'ACTIVE',3310),(949,4736,'ACTIVE',3311),(1285,4734,'ACTIVE',3312),(1286,4734,'ACTIVE',3313),(1287,4734,'ACTIVE',3314),(1288,4734,'ACTIVE',3315),(1289,4734,'ACTIVE',3316),(1290,4734,'ACTIVE',3317),(1291,4734,'ACTIVE',3318),(1292,4734,'ACTIVE',3319),(1293,4734,'ACTIVE',3320),(1294,4734,'ACTIVE',3321),(1295,4734,'ACTIVE',3322),(1296,4734,'ACTIVE',3323),(1297,4734,'ACTIVE',3324),(1298,4734,'ACTIVE',3325),(1299,4734,'ACTIVE',3326),(1300,4734,'ACTIVE',3327),(1301,4731,'ACTIVE',3328),(1302,4734,'ACTIVE',3329),(1303,4734,'ACTIVE',3330),(1304,4734,'ACTIVE',3331),(1305,4734,'ACTIVE',3332),(1306,4734,'ACTIVE',3333),(1307,4734,'ACTIVE',3334),(1308,4734,'ACTIVE',3335),(1309,4728,'ACTIVE',3336),(1047,4736,'ACTIVE',3337),(1310,4736,'ACTIVE',3338),(1311,4734,'ACTIVE',3339),(1312,4734,'ACTIVE',3340),(1313,4734,'ACTIVE',3341),(1314,4734,'ACTIVE',3342),(1315,4734,'ACTIVE',3343),(1316,4734,'ACTIVE',3344),(1317,4734,'ACTIVE',3345),(1318,4736,'ACTIVE',3346),(1319,4730,'ACTIVE',3347),(1320,4734,'ACTIVE',3348),(1321,4734,'ACTIVE',3349),(1322,4734,'ACTIVE',3350),(1323,4736,'ACTIVE',3351),(1324,4736,'ACTIVE',3352),(1325,4729,'ACTIVE',3353),(1326,4734,'ACTIVE',3354),(1327,4734,'ACTIVE',3355),(1328,4734,'ACTIVE',3356),(1329,4734,'ACTIVE',3357),(1330,4734,'ACTIVE',3358),(1331,4734,'ACTIVE',3359),(1332,4734,'ACTIVE',3360),(1333,4734,'ACTIVE',3361),(1334,4734,'ACTIVE',3362),(1335,4734,'ACTIVE',3363),(1336,4734,'ACTIVE',3364),(1337,4730,'ACTIVE',3365),(1338,4734,'ACTIVE',3366),(1339,4734,'ACTIVE',3367),(1340,4734,'ACTIVE',3368),(1341,4734,'ACTIVE',3369),(1342,4734,'ACTIVE',3370),(1343,4734,'ACTIVE',3371),(1344,4734,'ACTIVE',3372),(1345,4734,'ACTIVE',3373),(1346,4734,'ACTIVE',3374),(1347,4734,'ACTIVE',3375),(1348,4734,'ACTIVE',3376),(1349,4734,'ACTIVE',3377),(1350,4734,'ACTIVE',3378),(1351,4734,'ACTIVE',3379),(1352,4734,'ACTIVE',3380),(1353,4734,'ACTIVE',3381),(1354,4734,'ACTIVE',3382),(1355,4734,'ACTIVE',3383),(1356,4734,'ACTIVE',3384),(1357,4734,'ACTIVE',3385),(1358,4734,'ACTIVE',3386),(1359,4713,'ACTIVE',3387),(1360,4713,'ACTIVE',3388),(1,4708,'ACTIVE',3455),(7,4708,'ACTIVE',3456),(4,4708,'ACTIVE',3457),(2,4708,'ACTIVE',3458),(5,4708,'ACTIVE',3459),(6,4708,'ACTIVE',3460),(3,4708,'ACTIVE',3461),(125,4787,'ACTIVE',3462),(126,4787,'ACTIVE',3463),(127,4787,'ACTIVE',3464),(11,4788,'ACTIVE',3465),(12,4788,'ACTIVE',3466),(8,4788,'ACTIVE',3467),(685,4793,'ACTIVE',3468),(686,4793,'ACTIVE',3469),(157,4794,'ACTIVE',3470),(245,4794,'ACTIVE',3471),(283,4794,'ACTIVE',3472),(128,4796,'ACTIVE',3473),(129,4796,'ACTIVE',3474),(130,4796,'ACTIVE',3475),(161,4796,'ACTIVE',3476),(162,4796,'ACTIVE',3477),(164,4796,'ACTIVE',3478),(244,4796,'ACTIVE',3479),(426,4796,'ACTIVE',3480),(725,4796,'ACTIVE',3481),(134,4797,'ACTIVE',3482),(135,4797,'ACTIVE',3483),(158,4797,'ACTIVE',3484),(246,4797,'ACTIVE',3485),(282,4797,'ACTIVE',3486),(285,4797,'ACTIVE',3487),(726,4797,'ACTIVE',3488),(706,4798,'ACTIVE',3489),(136,4798,'ACTIVE',3490),(137,4798,'ACTIVE',3491),(131,4799,'ACTIVE',3492),(132,4799,'ACTIVE',3493),(133,4799,'ACTIVE',3494),(695,4795,'ACTIVE',3495),(692,4795,'ACTIVE',3496),(693,4795,'ACTIVE',3497),(699,4795,'ACTIVE',3498),(700,4795,'ACTIVE',3499),(691,4795,'ACTIVE',3500),(687,4795,'ACTIVE',3501),(701,4795,'ACTIVE',3502),(696,4795,'ACTIVE',3503),(1389,4776,'ACTIVE',3514),(1390,4807,'ACTIVE',3515),(1400,4788,'ACTIVE',3539),(1401,4794,'ACTIVE',3540),(1402,4897,'DEP',3541),(1403,4897,'DEP',3542),(1402,4897,'DEP',3543),(1403,4897,'DEP',3544),(1402,4897,'ACTIVE',3545),(1403,4897,'ACTIVE',3546),(1404,4788,'ACTIVE',3547),(1405,4897,'ACTIVE',3548),(1406,4736,'ACTIVE',3549);
/*!40000 ALTER TABLE `ReagentTypeAttribute_Set_tbl` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `ReagentTypeAttributes_tbl`
--

DROP TABLE IF EXISTS `ReagentTypeAttributes_tbl`;
SET @saved_cs_client     = @@character_set_client;
SET character_set_client = utf8;
CREATE TABLE `ReagentTypeAttributes_tbl` (
  `reagentTypePropertyID` int(11) NOT NULL auto_increment,
  `reagentTypeID` int(11) NOT NULL,
  `propertyTypeID` int(11) NOT NULL,
  `status` enum('ACTIVE','DEP') NOT NULL default 'ACTIVE',
  `ordering` int(11) NOT NULL default '1',
  `is_multiple` enum('YES','NO') default 'NO',
  `is_hyperlink` enum('YES','NO') NOT NULL default 'NO',
  `is_customizeable` enum('YES','NO') NOT NULL default 'NO',
  PRIMARY KEY  (`reagentTypePropertyID`),
  KEY `reagentTypeID` (`reagentTypeID`),
  KEY `status` (`status`),
  KEY `propertyTypeID` (`propertyTypeID`),
  KEY `reagentTypePropertyID` (`reagentTypePropertyID`)
) ENGINE=MyISAM AUTO_INCREMENT=4941 DEFAULT CHARSET=latin1;
SET character_set_client = @saved_cs_client;

--
-- Dumping data for table `ReagentTypeAttributes_tbl`
--

LOCK TABLES `ReagentTypeAttributes_tbl` WRITE;
/*!40000 ALTER TABLE `ReagentTypeAttributes_tbl` DISABLE KEYS */;
INSERT INTO `ReagentTypeAttributes_tbl` VALUES (4700,1,2190,'ACTIVE',1,'NO','NO','NO'),(4701,1,2188,'ACTIVE',2,'NO','NO','NO'),(4702,1,2191,'ACTIVE',3,'NO','NO','NO'),(4703,2,2190,'ACTIVE',1,'NO','NO','NO'),(4704,3,2190,'ACTIVE',1,'NO','NO','NO'),(4705,4,2190,'ACTIVE',1,'NO','NO','NO'),(4706,2,2188,'ACTIVE',2,'NO','NO','NO'),(4707,3,2188,'ACTIVE',2,'NO','NO','NO'),(4708,4,2188,'ACTIVE',2,'NO','NO','NO'),(4709,2,2191,'ACTIVE',3,'NO','NO','NO'),(4710,3,2191,'ACTIVE',3,'NO','NO','NO'),(4711,4,2191,'ACTIVE',3,'NO','NO','NO'),(4712,1,2219,'ACTIVE',4,'NO','NO','NO'),(4713,1,2189,'ACTIVE',7,'YES','NO','YES'),(4714,1,2196,'ACTIVE',6,'NO','NO','NO'),(4715,1,2195,'ACTIVE',5,'NO','NO','NO'),(4716,1,2215,'ACTIVE',8,'NO','NO','NO'),(4717,1,2199,'ACTIVE',10,'NO','NO','NO'),(4718,1,2200,'ACTIVE',9,'NO','NO','NO'),(4719,1,2205,'ACTIVE',1,'NO','NO','NO'),(4720,1,2206,'DEP',1,'NO','NO','NO'),(4721,1,2197,'ACTIVE',2,'NO','NO','NO'),(4722,1,2198,'ACTIVE',3,'NO','NO','NO'),(4723,1,2186,'ACTIVE',4,'NO','NO','NO'),(4724,1,2187,'ACTIVE',5,'NO','NO','NO'),(4725,1,2227,'ACTIVE',1,'NO','NO','NO'),(4726,1,2192,'ACTIVE',14,'NO','NO','YES'),(4727,1,2193,'ACTIVE',1,'NO','NO','NO'),(4728,1,2202,'ACTIVE',11,'NO','NO','YES'),(4729,1,2201,'ACTIVE',1,'NO','NO','YES'),(4730,1,2225,'ACTIVE',10,'NO','NO','YES'),(4731,1,2228,'ACTIVE',13,'NO','NO','YES'),(4732,1,2234,'ACTIVE',7,'NO','NO','YES'),(4733,1,2233,'ACTIVE',6,'NO','NO','YES'),(4734,1,2230,'ACTIVE',8,'NO','NO','YES'),(4735,1,2226,'ACTIVE',9,'NO','NO','YES'),(4736,1,2231,'ACTIVE',12,'NO','NO','NO'),(4737,1,2232,'ACTIVE',15,'NO','NO','YES'),(4738,2,2189,'ACTIVE',6,'YES','NO','YES'),(4739,2,2196,'ACTIVE',5,'NO','NO','NO'),(4740,2,2195,'ACTIVE',4,'NO','NO','NO'),(4741,2,2215,'ACTIVE',7,'NO','NO','NO'),(4742,2,2199,'ACTIVE',9,'NO','NO','NO'),(4743,2,2200,'ACTIVE',8,'NO','NO','NO'),(4744,2,2205,'ACTIVE',1,'NO','NO','NO'),(4745,2,2206,'DEP',1,'NO','NO','NO'),(4746,2,2218,'ACTIVE',1,'NO','NO','NO'),(4747,2,2239,'ACTIVE',1,'NO','NO','NO'),(4748,2,2197,'ACTIVE',2,'NO','NO','NO'),(4749,2,2198,'ACTIVE',3,'NO','NO','NO'),(4750,2,2186,'ACTIVE',4,'NO','NO','NO'),(4751,2,2187,'ACTIVE',5,'NO','NO','NO'),(4752,2,2227,'ACTIVE',1,'NO','NO','NO'),(4753,2,2192,'ACTIVE',14,'NO','NO','NO'),(4754,2,2193,'ACTIVE',1,'NO','NO','NO'),(4755,2,2202,'ACTIVE',11,'NO','NO','YES'),(4756,2,2201,'ACTIVE',1,'NO','NO','YES'),(4757,2,2225,'ACTIVE',10,'NO','NO','YES'),(4758,2,2228,'ACTIVE',13,'NO','NO','YES'),(4759,2,2234,'ACTIVE',7,'NO','NO','YES'),(4760,2,2231,'ACTIVE',12,'NO','NO','NO'),(4761,2,2232,'ACTIVE',15,'NO','NO','YES'),(4762,2,2226,'ACTIVE',9,'NO','NO','YES'),(4763,2,2233,'ACTIVE',6,'NO','NO','YES'),(4764,2,2230,'ACTIVE',8,'NO','NO','YES'),(4765,2,2223,'ACTIVE',4,'NO','NO','NO'),(4766,2,2216,'ACTIVE',2,'NO','NO','NO'),(4767,2,2224,'ACTIVE',1,'NO','NO','NO'),(4768,2,2214,'ACTIVE',7,'YES','NO','YES'),(4769,2,2194,'ACTIVE',3,'NO','NO','NO'),(4770,2,2217,'ACTIVE',6,'NO','NO','NO'),(4771,2,2220,'ACTIVE',1,'NO','NO','NO'),(4772,2,2203,'ACTIVE',2,'NO','NO','NO'),(4773,2,2210,'ACTIVE',4,'NO','NO','YES'),(4774,2,2222,'ACTIVE',3,'YES','NO','YES'),(4775,3,2207,'ACTIVE',4,'NO','NO','NO'),(4776,3,2189,'ACTIVE',7,'YES','NO','YES'),(4777,3,2196,'ACTIVE',6,'NO','NO','NO'),(4778,3,2195,'ACTIVE',5,'NO','NO','NO'),(4779,3,2215,'ACTIVE',8,'NO','NO','NO'),(4780,3,2199,'ACTIVE',10,'NO','NO','NO'),(4781,3,2200,'ACTIVE',9,'NO','NO','NO'),(4782,3,2205,'ACTIVE',1,'NO','NO','NO'),(4783,3,2208,'ACTIVE',1,'NO','NO','NO'),(4784,3,2209,'ACTIVE',1,'NO','NO','NO'),(4785,3,2194,'ACTIVE',3,'NO','NO','NO'),(4786,3,2204,'ACTIVE',1,'YES','NO','YES'),(4787,4,2221,'ACTIVE',4,'NO','NO','NO'),(4788,4,2189,'ACTIVE',7,'YES','NO','YES'),(4789,4,2196,'ACTIVE',6,'NO','NO','NO'),(4790,4,2195,'ACTIVE',5,'NO','NO','NO'),(4791,4,2215,'ACTIVE',8,'NO','NO','NO'),(4792,4,2199,'ACTIVE',10,'NO','NO','NO'),(4793,4,2200,'ACTIVE',9,'NO','NO','NO'),(4794,4,2214,'ACTIVE',1,'YES','NO','YES'),(4795,4,2229,'ACTIVE',1,'YES','NO','YES'),(4796,4,2210,'ACTIVE',1,'NO','NO','YES'),(4797,4,2211,'ACTIVE',2,'NO','NO','YES'),(4798,4,2213,'ACTIVE',3,'NO','NO','YES'),(4799,4,2212,'ACTIVE',4,'NO','NO','YES'),(4800,4,2241,'ACTIVE',3,'NO','NO','NO'),(4801,4,2242,'ACTIVE',2,'NO','NO','NO'),(4802,2,2244,'ACTIVE',5,'NO','NO','NO'),(4803,3,2197,'ACTIVE',1,'NO','NO','NO'),(4804,3,2198,'ACTIVE',2,'NO','NO','NO'),(4805,3,2186,'ACTIVE',3,'NO','NO','NO'),(4806,3,2187,'ACTIVE',4,'NO','NO','NO'),(4807,3,2230,'ACTIVE',6,'NO','NO','YES'),(4808,3,2233,'ACTIVE',5,'NO','NO','YES'),(4809,3,2224,'ACTIVE',1,'NO','NO','NO'),(4810,3,2216,'ACTIVE',2,'NO','NO','NO'),(4811,3,2223,'ACTIVE',4,'NO','NO','NO'),(4812,3,2244,'ACTIVE',5,'NO','NO','NO'),(4813,1,2245,'ACTIVE',11,'NO','NO','NO'),(4814,3,2246,'ACTIVE',4,'NO','NO','NO'),(4897,4,2281,'ACTIVE',1,'YES','NO','YES'),(4898,4,2282,'ACTIVE',3,'NO','NO','NO'),(4899,4,2283,'ACTIVE',2,'NO','NO','NO'),(4940,3,2296,'ACTIVE',1,'NO','NO','NO');
/*!40000 ALTER TABLE `ReagentTypeAttributes_tbl` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `ReagentType_tbl`
--

DROP TABLE IF EXISTS `ReagentType_tbl`;
SET @saved_cs_client     = @@character_set_client;
SET character_set_client = utf8;
CREATE TABLE `ReagentType_tbl` (
  `reagentTypeID` int(11) NOT NULL auto_increment,
  `reagentTypeName` varchar(27) NOT NULL default '',
  `reagent_prefix` varchar(27) NOT NULL default '',
  `status` enum('ACTIVE','DEP') NOT NULL default 'ACTIVE',
  PRIMARY KEY  (`reagentTypeID`),
  KEY `reagentTypeName` (`reagentTypeName`),
  KEY `status` (`status`),
  KEY `reagentTypeID` (`reagentTypeID`)
) ENGINE=MyISAM AUTO_INCREMENT=5 DEFAULT CHARSET=latin1;
SET character_set_client = @saved_cs_client;

--
-- Dumping data for table `ReagentType_tbl`
--

LOCK TABLES `ReagentType_tbl` WRITE;
/*!40000 ALTER TABLE `ReagentType_tbl` DISABLE KEYS */;
INSERT INTO `ReagentType_tbl` VALUES (1,'Vector','V','ACTIVE'),(2,'Insert','I','ACTIVE'),(3,'Oligo','O','ACTIVE'),(4,'CellLine','C','ACTIVE');
/*!40000 ALTER TABLE `ReagentType_tbl` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `Reagent_SubType_tbl`
--

DROP TABLE IF EXISTS `Reagent_SubType_tbl`;
SET @saved_cs_client     = @@character_set_client;
SET character_set_client = utf8;
CREATE TABLE `Reagent_SubType_tbl` (
  `reagent_SubTypeID` int(11) NOT NULL auto_increment,
  `reagent_typeID` int(11) NOT NULL default '0',
  `name` varchar(47) NOT NULL default '',
  `alias` varchar(27) default NULL,
  `affix` varchar(17) default NULL,
  `labID` int(11) NOT NULL default '1',
  `status` enum('ACTIVE','DEP') NOT NULL default 'ACTIVE',
  PRIMARY KEY  (`reagent_SubTypeID`)
) ENGINE=MyISAM AUTO_INCREMENT=8 DEFAULT CHARSET=latin1;
SET character_set_client = @saved_cs_client;

--
-- Dumping data for table `Reagent_SubType_tbl`
--

LOCK TABLES `Reagent_SubType_tbl` WRITE;
/*!40000 ALTER TABLE `Reagent_SubType_tbl` DISABLE KEYS */;
INSERT INTO `Reagent_SubType_tbl` VALUES (1,1,'Non-Recombination Vector','nonrecomb',NULL,1,'ACTIVE'),(2,1,'Creator Expression Vector via Recombination','recomb',NULL,1,'ACTIVE'),(3,1,'Novel Vector','novel',NULL,1,'ACTIVE'),(4,1,'Gateway Entry Vector','gateway_entry',NULL,1,'ACTIVE'),(5,1,'Gateway Expression Vector','gateway_expression',NULL,1,'ACTIVE'),(6,4,'Parent Cell Line','parent_cell_line',NULL,1,'ACTIVE'),(7,4,'Stable Cell Line','stable_cell_line',NULL,1,'ACTIVE');
/*!40000 ALTER TABLE `Reagent_SubType_tbl` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `Reagents_tbl`
--

DROP TABLE IF EXISTS `Reagents_tbl`;
SET @saved_cs_client     = @@character_set_client;
SET character_set_client = utf8;
CREATE TABLE `Reagents_tbl` (
  `reagentID` int(11) NOT NULL auto_increment,
  `reagentTypeID` int(11) NOT NULL default '0',
  `reagent_SubTypeID` int(11) NOT NULL default '1',
  `3rd_typing_id` int(11) NOT NULL default '0',
  `groupID` int(11) NOT NULL default '0',
  `creatorID` smallint(6) NOT NULL default '1',
  `labID` int(11) NOT NULL default '1',
  `status` enum('ACTIVE','DEP') NOT NULL default 'ACTIVE',
  PRIMARY KEY  (`reagentID`),
  KEY `reagentID` (`reagentID`),
  KEY `reagentTypeID` (`reagentTypeID`),
  KEY `groupID` (`groupID`),
  KEY `status` (`status`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1;
SET character_set_client = @saved_cs_client;

--
-- Dumping data for table `Reagents_tbl`
--

LOCK TABLES `Reagents_tbl` WRITE;
/*!40000 ALTER TABLE `Reagents_tbl` DISABLE KEYS */;
/*!40000 ALTER TABLE `Reagents_tbl` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `SecuredPages_tbl`
--

DROP TABLE IF EXISTS `SecuredPages_tbl`;
SET @saved_cs_client     = @@character_set_client;
SET character_set_client = utf8;
CREATE TABLE `SecuredPages_tbl` (
  `pageID` int(11) NOT NULL auto_increment,
  `section` varchar(150) NOT NULL default '',
  `status` enum('ACTIVE','DEP') NOT NULL default 'ACTIVE',
  `baseURL` varchar(250) NOT NULL default '',
  PRIMARY KEY  (`pageID`)
) ENGINE=MyISAM AUTO_INCREMENT=47 DEFAULT CHARSET=latin1;
SET character_set_client = @saved_cs_client;

--
-- Dumping data for table `SecuredPages_tbl`
--

LOCK TABLES `SecuredPages_tbl` WRITE;
/*!40000 ALTER TABLE `SecuredPages_tbl` DISABLE KEYS */;
INSERT INTO `SecuredPages_tbl` VALUES (1,'Home','ACTIVE','index.php'),(2,'Reagent Tracker','ACTIVE','Reagent.php'),(3,'Location Tracker','ACTIVE','Location.php'),(5,'Project Management','ACTIVE','Project.php'),(6,'User Management','ACTIVE','User.php'),(7,'Documentation','ACTIVE','Docs.php'),(8,'Terms and Conditions','ACTIVE','copyright.php'),(9,'Contact Us','ACTIVE','contacts.php'),(10,'Reagent Tracker','ACTIVE','search.php'),(12,'Lab Management','ACTIVE','User.php'),(13,'Search projects','ACTIVE','Project.php?View=2'),(14,'Add containers','ACTIVE','Location.php?View=6&Sub=3'),(15,'Search containers','ACTIVE','Location.php?View=2'),(16,'Add container sizes','ACTIVE','Location.php?View=6&Sub=1'),(17,'Add reagents','ACTIVE','Reagent.php?View=2'),(18,'Search reagents','ACTIVE','search.php?View=1'),(21,'Add projects','ACTIVE','Project.php?View=1'),(22,'Delete Projects','ACTIVE','Project.php?View=3'),(23,'Add users','ACTIVE','User.php?View=1'),(24,'Search users','ACTIVE','User.php?View=2'),(25,'Add laboratories','ACTIVE','User.php?View=3'),(26,'Search laboratories','ACTIVE','User.php?View=4'),(27,'Change your password','ACTIVE','User.php?View=6'),(30,'Personal page','ACTIVE','User.php?View=7'),(34,'Help and Support','ACTIVE','bugreport.php'),(35,'View your orders','ACTIVE','User.php?View=8'),(36,'Chemical Tracker','ACTIVE','Chemical.php'),(37,'Search Chemicals','ACTIVE','Chemical.php?View=1'),(38,'Add Chemicals','ACTIVE','Chemical.php?View=2'),(41,'Search reagent types','ACTIVE','Reagent.php?View=5'),(42,'Add container types','ACTIVE','Location.php?View=6&Sub=2'),(43,'Search container types','ACTIVE','Location.php?View=6&Sub=4'),(44,'Search container sizes','ACTIVE','Location.php?View=6&Sub=5'),(45,'Add reagent types','ACTIVE','Reagent.php?View=3'),(46,'Statistics','ACTIVE','Reagent.php?View=4');
/*!40000 ALTER TABLE `SecuredPages_tbl` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `SequenceType_tbl`
--

DROP TABLE IF EXISTS `SequenceType_tbl`;
SET @saved_cs_client     = @@character_set_client;
SET character_set_client = utf8;
CREATE TABLE `SequenceType_tbl` (
  `seqTypeID` int(11) NOT NULL auto_increment,
  `seqTypeName` varchar(17) NOT NULL default '',
  `status` enum('ACTIVE','DEP') NOT NULL default 'ACTIVE',
  PRIMARY KEY  (`seqTypeID`)
) ENGINE=MyISAM AUTO_INCREMENT=5 DEFAULT CHARSET=latin1;
SET character_set_client = @saved_cs_client;

--
-- Dumping data for table `SequenceType_tbl`
--

LOCK TABLES `SequenceType_tbl` WRITE;
/*!40000 ALTER TABLE `SequenceType_tbl` DISABLE KEYS */;
INSERT INTO `SequenceType_tbl` VALUES (1,'DNA','ACTIVE'),(2,'Protein','ACTIVE'),(4,'RNA','ACTIVE');
/*!40000 ALTER TABLE `SequenceType_tbl` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `Sequences_tbl`
--

DROP TABLE IF EXISTS `Sequences_tbl`;
SET @saved_cs_client     = @@character_set_client;
SET character_set_client = utf8;
CREATE TABLE `Sequences_tbl` (
  `seqID` int(11) NOT NULL auto_increment,
  `seqTypeID` int(11) NOT NULL default '0',
  `sequence` text NOT NULL,
  `frame` smallint(6) NOT NULL default '0',
  `length` int(11) default NULL,
  `labID` int(11) NOT NULL default '1',
  `status` enum('ACTIVE','DEP') NOT NULL default 'ACTIVE',
  `start` int(11) default NULL,
  `end` int(11) default NULL,
  `mw` double(65,2) default NULL,
  PRIMARY KEY  (`seqID`),
  KEY `seqID` (`seqID`),
  FULLTEXT KEY `sequence` (`sequence`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1;
SET character_set_client = @saved_cs_client;

--
-- Dumping data for table `Sequences_tbl`
--

LOCK TABLES `Sequences_tbl` WRITE;
/*!40000 ALTER TABLE `Sequences_tbl` DISABLE KEYS */;
/*!40000 ALTER TABLE `Sequences_tbl` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `System_Set_Groups_tbl`
--

DROP TABLE IF EXISTS `System_Set_Groups_tbl`;
SET @saved_cs_client     = @@character_set_client;
SET character_set_client = utf8;
CREATE TABLE `System_Set_Groups_tbl` (
  `ssetGroupID` int(11) NOT NULL auto_increment,
  `groupDesc` tinytext,
  `propertyIDLink` int(11) default NULL,
  `status` enum('ACTIVE','DEP') NOT NULL default 'ACTIVE',
  PRIMARY KEY  (`ssetGroupID`)
) ENGINE=MyISAM AUTO_INCREMENT=75 DEFAULT CHARSET=latin1;
SET character_set_client = @saved_cs_client;

--
-- Dumping data for table `System_Set_Groups_tbl`
--

LOCK TABLES `System_Set_Groups_tbl` WRITE;
/*!40000 ALTER TABLE `System_Set_Groups_tbl` DISABLE KEYS */;
INSERT INTO `System_Set_Groups_tbl` VALUES (1,'General Properties Status',2188,'ACTIVE'),(2,'General Properties Verification',2189,'ACTIVE'),(4,'DNA Sequence Features Expression System',2201,'ACTIVE'),(5,'DNA Sequence Features Promoter',2202,'ACTIVE'),(6,'DNA Sequence Features Tag',2192,'ACTIVE'),(7,'DNA Sequence Features Tag Position',2193,'ACTIVE'),(8,'General Properties Vector type',2219,'ACTIVE'),(9,'Classifiers Type of Insert',2220,'ACTIVE'),(10,'General Properties Cell Line Type',2221,'ACTIVE'),(11,'Classifiers Open/Closed',2203,'ACTIVE'),(12,'Classifiers Protocol',2204,'ACTIVE'),(13,'Classifiers Cloning Method',2222,'ACTIVE'),(14,'General Properties Oligo Type',2207,'ACTIVE'),(15,'Classifiers Species',2210,'ACTIVE'),(16,'Classifiers Developmental Stage',2212,'ACTIVE'),(17,'Classifiers Tissue Type',2211,'ACTIVE'),(18,'Classifiers Morphology',2213,'ACTIVE'),(20,'External Identifiers Alternate ID',2214,'ACTIVE'),(23,'DNA Sequence Features Intron',2234,'ACTIVE'),(24,'DNA Sequence Features Selectable Marker',2228,'ACTIVE'),(25,'DNA Sequence Features PolyA Tail',2225,'ACTIVE'),(26,'DNA Sequence Features Origin of Replication',2226,'ACTIVE'),(27,'DNA Sequence Features Miscellaneous',2230,'ACTIVE'),(29,'DNA Sequence Features Transcription Terminator',2232,'ACTIVE'),(30,'DNA Sequence Features Cleavage Site',2233,'ACTIVE'),(33,'General Properties Restrictions On Use',2200,'ACTIVE'),(35,'DNA Sequence Features 5\' Cloning Site',2197,'ACTIVE'),(36,'DNA Sequence Features 3\' Cloning Site',2198,'DEP'),(37,'DNA Sequence Features Restriction Site',2231,'ACTIVE'),(38,'General Properties Project ID',2191,'DEP'),(53,'Protein Sequence Features Miscellaneous',2271,'DEP'),(54,'Protein Sequence Features Cleavage Site',2272,'DEP'),(57,'RNA Sequence Features Tag Position',2275,'DEP'),(58,'RNA Sequence Features Tag',2276,'DEP'),(59,'RNA Sequence Features Miscellaneous',2277,'DEP'),(61,'General Properties Project ID',2191,'DEP'),(62,'General Properties Project ID',2191,'DEP'),(64,'Growth Properties selectable marker',2281,'ACTIVE'),(66,'General Properties Project ID',2191,'DEP'),(68,'Protein Sequence Features Tag Position',2286,'ACTIVE'),(69,'Protein Sequence Features Tag',2287,'ACTIVE'),(70,'Protein Sequence Features Miscellaneous',2288,'ACTIVE'),(72,'General Properties Project ID',2191,'ACTIVE'),(73,'RNA Sequence Features Tag Position',2294,'ACTIVE'),(74,'RNA Sequence Features Tag',2295,'ACTIVE');
/*!40000 ALTER TABLE `System_Set_Groups_tbl` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `System_Set_tbl`
--

DROP TABLE IF EXISTS `System_Set_tbl`;
SET @saved_cs_client     = @@character_set_client;
SET character_set_client = utf8;
CREATE TABLE `System_Set_tbl` (
  `ssetID` int(11) NOT NULL auto_increment,
  `ssetGroupID` int(11) NOT NULL default '0',
  `entityName` text NOT NULL,
  `entityDesc` text,
  `ordering` int(11) default NULL,
  `labID` int(11) NOT NULL default '1',
  `restriction` enum('YES','NO') NOT NULL default 'NO',
  `status` enum('ACTIVE','DEP') NOT NULL default 'ACTIVE',
  PRIMARY KEY  (`ssetID`),
  KEY `labID` (`labID`),
  KEY `status` (`status`),
  KEY `ssetGroupID` (`ssetGroupID`)
) ENGINE=MyISAM AUTO_INCREMENT=1418 DEFAULT CHARSET=latin1;
SET character_set_client = @saved_cs_client;

--
-- Dumping data for table `System_Set_tbl`
--

LOCK TABLES `System_Set_tbl` WRITE;
/*!40000 ALTER TABLE `System_Set_tbl` DISABLE KEYS */;
INSERT INTO `System_Set_tbl` VALUES (1,1,'Completed','Completed',1,1,'NO','ACTIVE'),(2,1,'In Progress','In Progress',1,1,'NO','ACTIVE'),(3,1,'Planning','Planning',1,1,'NO','ACTIVE'),(4,1,'Failed','Failed',1,1,'NO','ACTIVE'),(5,1,'Not Available','Not Available',1,1,'NO','ACTIVE'),(6,1,'Not Required','Not Required',1,1,'NO','ACTIVE'),(7,1,'Do Not Use','Do Not Use',1,1,'NO','ACTIVE'),(8,2,'Unverified','Unverified',1,1,'NO','ACTIVE'),(9,2,'RE Verified','RE Verified',1,1,'NO','ACTIVE'),(10,2,'Sequence Verified','Sequence Verified',1,1,'NO','ACTIVE'),(11,2,'Expression Verified','Expression Verified',1,1,'NO','ACTIVE'),(12,2,'Failed Verification','Failed Verification',1,1,'NO','ACTIVE'),(13,2,'Sequence And Expression Verified','Sequence And Expression Verified',1,1,'NO','ACTIVE'),(14,2,'Sequenced, Mutations Identified','Sequenced, Mutations Identified',1,1,'NO','ACTIVE'),(23,4,'None','None',1,1,'NO','ACTIVE'),(24,4,'Bacteria','Bacteria',1,1,'NO','ACTIVE'),(25,4,'Mammalian','Mammalian',1,1,'NO','ACTIVE'),(26,4,'Insect','Insect',1,1,'NO','ACTIVE'),(27,4,'In Vitro TNT','In Vitro TNT',1,1,'NO','ACTIVE'),(28,4,'Mammalian Retrovirus','Mammalian Retrovirus',1,1,'NO','ACTIVE'),(29,4,'Mammalian and In Vitro TNT','Mammalian and In Vitro TNT',1,1,'NO','ACTIVE'),(31,5,'CMV','CMV',1,1,'NO','ACTIVE'),(32,5,'T7','T7',1,1,'NO','ACTIVE'),(33,5,'LTR','LTR',1,1,'NO','ACTIVE'),(35,5,'Polyhedrin','Polyhedrin',1,1,'NO','ACTIVE'),(36,5,'SP6','SP6',1,1,'NO','ACTIVE'),(38,5,'TAC','TAC',1,1,'NO','ACTIVE'),(39,5,'TRC','TRC',1,1,'NO','ACTIVE'),(46,5,'b-actin','b-actin',1,1,'NO','ACTIVE'),(48,6,'Single Flag','Single Flag',1,1,'NO','ACTIVE'),(49,6,'Triple Flag','Triple Flag',1,1,'NO','ACTIVE'),(51,6,'Double Myc','Double Myc',1,1,'NO','ACTIVE'),(52,6,'Single HA','Single HA',1,1,'NO','ACTIVE'),(53,6,'GST','GST',1,1,'NO','ACTIVE'),(54,6,'His','His',1,1,'NO','ACTIVE'),(55,6,'EYFP','EYFP',1,1,'NO','ACTIVE'),(56,6,'EGFP','EGFP',1,1,'NO','ACTIVE'),(57,6,'ECFP','ECFP',1,1,'NO','ACTIVE'),(61,6,'Citrine YFP','Citrine YFP',1,1,'NO','ACTIVE'),(62,6,'Cerulean CFP','Cerulean CFP',1,1,'NO','ACTIVE'),(63,6,'CFP','CFP',1,1,'NO','ACTIVE'),(64,6,'Triple HA','Triple HA',1,1,'NO','ACTIVE'),(67,6,'MBP','MBP',1,1,'NO','ACTIVE'),(69,6,'Renilla Luciferase','Renilla Luciferase',1,1,'NO','ACTIVE'),(70,6,'S','S',1,1,'NO','ACTIVE'),(71,6,'BAP','BAP',1,1,'NO','ACTIVE'),(72,6,'CBP','CBP',1,1,'NO','ACTIVE'),(77,6,'mCitrine YFP','mCitrine YFP',1,1,'NO','ACTIVE'),(78,6,'RFP','RFP',1,1,'NO','ACTIVE'),(80,7,'None','None',1,1,'NO','ACTIVE'),(81,7,'C-Terminus','C-Terminus',1,1,'NO','ACTIVE'),(82,7,'N-Terminus','N-Terminus',1,1,'NO','ACTIVE'),(83,7,'N-Terminus (splice)','N-Terminus (splice)',1,1,'NO','ACTIVE'),(84,7,'N- and C-Termini','N- and C-Termini',1,1,'NO','ACTIVE'),(85,7,'Internal','Internal',1,1,'NO','ACTIVE'),(86,8,'Parent Vector','Parent Vector',1,1,'NO','ACTIVE'),(87,8,'Creator Parent Donor','Creator Parent Donor',1,1,'NO','ACTIVE'),(88,8,'Creator Parent Acceptor','Creator Parent Acceptor',1,1,'NO','ACTIVE'),(89,8,'cDNA Source','cDNA Source',1,1,'NO','ACTIVE'),(90,8,'cDNA Expression','cDNA Expression',1,1,'NO','ACTIVE'),(91,8,'Cloning Intermediate','Cloning Intermediate',1,1,'NO','ACTIVE'),(92,8,'Creator cDNA Donor','Creator cDNA Donor',1,1,'NO','ACTIVE'),(93,8,'Creator cDNA Expression','Creator cDNA Expression',1,1,'NO','ACTIVE'),(94,8,'RNAi','RNAi',1,1,'NO','ACTIVE'),(95,8,'Gateway Parent Destination Vector','Gateway Parent Destination Vector',1,1,'NO','ACTIVE'),(96,8,'Gateway Parent Donor Vector','Gateway Parent Donor Vector',1,1,'NO','ACTIVE'),(97,8,'Gateway Entry Clone','Gateway Entry Clone',1,1,'NO','ACTIVE'),(98,8,'Gateway Expression Clone','Gateway Expression Clone',1,1,'NO','ACTIVE'),(99,9,'None','None',1,1,'NO','ACTIVE'),(100,9,'cDNA with UTRs','cDNA with UTRs',1,1,'NO','ACTIVE'),(101,9,'ORF','ORF',1,1,'NO','ACTIVE'),(102,9,'ORF Subsequence','ORF Subsequence',1,1,'NO','ACTIVE'),(103,9,'DNA Fragment','DNA Fragment',1,1,'NO','ACTIVE'),(104,9,'ORF Domain','ORF Domain',1,1,'NO','ACTIVE'),(105,11,'Open with ATG','Open with ATG',1,1,'','ACTIVE'),(106,11,'Open, no ATG','Open, no ATG',1,1,'','ACTIVE'),(107,11,'Closed with ATG','Closed with ATG',1,1,'','ACTIVE'),(108,11,'Closed, no ATG','Closed, no ATG',1,1,'','ACTIVE'),(109,11,'Non-Coding','Non-Coding',1,1,'','ACTIVE'),(110,13,'PCR RE','PCR Restriction Enzyme',1,1,'NO','ACTIVE'),(111,13,'PCR In Fusion','PCR In Fusion',1,1,'NO','ACTIVE'),(112,13,'RE','Restriction Enzyme',1,1,'NO','ACTIVE'),(113,13,'Oligos','Oligos',1,1,'NO','ACTIVE'),(114,13,'Mutagenesis','Mutagenesis',1,1,'NO','ACTIVE'),(115,13,'PCR BP Reaction','PCR BP Reaction',1,1,'NO','ACTIVE'),(116,13,'PCR Topo','PCR Topo',1,1,'NO','ACTIVE'),(117,12,'PCR','PCR',1,1,'NO','ACTIVE'),(118,12,'PCR RE','PCR Restriction Enzyme',1,1,'NO','ACTIVE'),(119,12,'PCR In Fusion','PCR In Fusion',1,1,'NO','ACTIVE'),(120,12,'Sequencing','Sequencing',1,1,'NO','ACTIVE'),(121,12,'Oligos','Oligos',1,1,'NO','ACTIVE'),(122,12,'Mutagenesis','Mutagenesis',1,1,'NO','ACTIVE'),(123,12,'BP Reaction','BP Reaction',1,1,'NO','ACTIVE'),(124,12,'PCR Topo','PCR Topo',1,1,'NO','ACTIVE'),(125,10,'Parent','Parent',1,1,'NO','ACTIVE'),(126,10,'cDNA Stable','cDNA Stable',1,1,'NO','ACTIVE'),(127,10,'RNAi Stable','RNAi Stable',1,1,'NO','ACTIVE'),(128,15,'Canis Familiaris','Canis Familiaris',1,1,'NO','ACTIVE'),(129,15,'Cercopithecus aethiops','Cercopithecus aethiops',1,1,'NO','ACTIVE'),(130,15,'Homo Sapiens','Homo Sapiens',1,1,'NO','ACTIVE'),(131,16,'embryo','embryo',1,1,'NO','ACTIVE'),(132,16,'child/teen','child/teen',1,1,'NO','ACTIVE'),(133,16,'adult','adult',1,1,'NO','ACTIVE'),(134,17,'Kidney','Kidney',1,1,'NO','ACTIVE'),(135,17,'T lymphocyte','T lymphocyte',1,1,'NO','ACTIVE'),(136,18,'epithelial','epithelial',1,1,'NO','ACTIVE'),(137,18,'lymphoblast','lymphoblast',1,1,'NO','ACTIVE'),(141,14,'sense','sense',1,1,'NO','ACTIVE'),(142,14,'antisense','antisense',1,1,'NO','ACTIVE'),(144,13,'LR Reaction','LR reaction',1,1,'NO','ACTIVE'),(145,20,'IMAGE','IMAGE',1,1,'NO','ACTIVE'),(146,20,'RIKEN','RIKEN',1,1,'NO','ACTIVE'),(147,20,'Kazusa','Kazusa',1,1,'NO','ACTIVE'),(148,20,'LIFESEQ','LIFESEQ',1,1,'NO','ACTIVE'),(149,20,'RZPD','RZPD',1,1,'NO','ACTIVE'),(150,13,'PCR','PCR',1,1,'NO','ACTIVE'),(151,4,'Yeast','Yeast',1,1,'NO','ACTIVE'),(152,20,'HIP','HIP',1,1,'NO','ACTIVE'),(153,20,'ADDGENE','ADDGENE',1,1,'NO','ACTIVE'),(154,4,'Mammalian Lentivirus','Mammalian Lentivirus',1,1,'NO','ACTIVE'),(156,6,'T7','T7',1,1,'NO','ACTIVE'),(157,20,'PMID','PMID',1,1,'NO','ACTIVE'),(158,17,'Bronchial','Bronchial',1,1,'NO','ACTIVE'),(160,5,'LAC','LAC',NULL,1,'NO','ACTIVE'),(161,15,'Rattus norvegicus','Rattus norvegicus',1,1,'NO','ACTIVE'),(162,15,'Mus musculus','Mus musculus',1,1,'NO','ACTIVE'),(163,15,'Human adenovirus 2','Human adenovirus 2',1,1,'NO','ACTIVE'),(164,15,'Danio rerio','Danio rerio',1,1,'NO','ACTIVE'),(165,15,'synthetic construct','synthetic construct',1,1,'NO','ACTIVE'),(244,15,'Arabidopsis thaliana','Arabidopsis thaliana',NULL,1,'NO','ACTIVE'),(245,20,'ATCC','ATCC',NULL,1,'NO','ACTIVE'),(246,17,'colerectal adenocarcinoma','colerectal adenocarcinoma',NULL,1,'NO','ACTIVE'),(259,6,'TrxA','TrxA',NULL,1,'NO','ACTIVE'),(260,6,'monomeric CFP','monomeric CFP',NULL,1,'NO','ACTIVE'),(261,6,'monomeric Cerulean CFP','monomeric Cerulean CFP',NULL,1,'NO','ACTIVE'),(262,6,'monomeric GFP','monomeric GFP',NULL,1,'NO','ACTIVE'),(264,6,'monomeric Citrine YFP','monomeric Citrine YFP',NULL,1,'NO','ACTIVE'),(265,6,'mCherry','mCherry',NULL,1,'NO','ACTIVE'),(266,6,'photoactivatable GFP','photoactivatable GFP',NULL,1,'NO','ACTIVE'),(267,24,'Thymidine Kinase','Thymidine Kinase',NULL,1,'NO','ACTIVE'),(270,4,'Bacteria and In Vitro TNT','Bacteria and In Vitro TNT',NULL,1,'NO','ACTIVE'),(274,5,'CMV enhancer','CMV enhancer',NULL,1,'NO','ACTIVE'),(278,4,'','',NULL,1,'NO','DEP'),(279,5,'Ampicillin','Ampicillin',NULL,1,'NO','ACTIVE'),(280,6,'NusA','NusA',NULL,1,'NO','ACTIVE'),(282,17,'cervical','cervical',NULL,1,'NO','ACTIVE'),(283,20,'Invitrogen','Invitrogen',NULL,1,'NO','ACTIVE'),(285,17,'Breast cancer','Breast cancer',NULL,1,'NO','ACTIVE'),(286,20,'HGNC','HGNC',NULL,1,'NO','ACTIVE'),(288,13,'Gene Synthesis','Gene Synthesis',NULL,1,'NO','ACTIVE'),(290,23,'CMV intron B','CMV intron B',NULL,1,'NO','ACTIVE'),(291,23,'CMV intron A','CMV intron A',NULL,1,'NO','ACTIVE'),(292,23,'beta-globin','beta-globin',NULL,1,'NO','ACTIVE'),(293,23,'SV40','SV40',NULL,1,'NO','ACTIVE'),(294,24,'Ampicillin Resistance','Ampicillin Resistance',NULL,1,'NO','ACTIVE'),(295,24,'Chloramphenicol Resistance','Chloramphenicol Resistance',NULL,1,'NO','ACTIVE'),(296,24,'Kanamycin Resistance','Kanamycin Resistance',NULL,1,'NO','ACTIVE'),(297,24,'Puromycin Resistance','Puromycin Resistance',NULL,1,'NO','ACTIVE'),(298,24,'G418/neomycin Resistance','G418/neomycin Resistance',NULL,1,'NO','ACTIVE'),(299,24,'Hygromycin Resistance','Hygromycin Resistance',NULL,1,'NO','ACTIVE'),(300,24,'Spectinomycin Resistance','Spectinomycin Resistance',NULL,1,'NO','ACTIVE'),(301,24,'Blasticidin Resistance','Blasticidin Resistance',NULL,1,'NO','ACTIVE'),(302,24,'Zeocin Resistance','Zeocin Resistance',NULL,1,'NO','ACTIVE'),(303,24,'SacB','SacB',NULL,1,'NO','ACTIVE'),(304,24,'ccdB','ccdB',NULL,1,'NO','ACTIVE'),(305,24,'Kanamycin/Neomycin resistance','Kanamycin/Neomycin resistance',NULL,1,'NO','ACTIVE'),(306,24,'Neomycin resistance','Neomycin resistance',NULL,1,'NO','ACTIVE'),(307,24,'URA3','URA3',NULL,1,'NO','ACTIVE'),(308,25,'BGH','BGH',NULL,1,'NO','ACTIVE'),(309,25,'hGH','hGH',NULL,1,'NO','ACTIVE'),(310,25,'HSV TK','HSV TK',NULL,1,'NO','ACTIVE'),(311,25,'pGH','pGH',NULL,1,'NO','ACTIVE'),(312,25,'SV40','SV40',NULL,1,'NO','ACTIVE'),(313,25,'SV40 early','SV40 early',NULL,1,'NO','ACTIVE'),(315,25,'unspecified','unspecified',NULL,1,'NO','ACTIVE'),(316,26,'ColE1','ColE1',NULL,1,'NO','ACTIVE'),(318,26,'F1','F1',NULL,1,'NO','ACTIVE'),(319,26,'pBR322','pBR322',NULL,1,'NO','ACTIVE'),(320,26,'pUC','pUC',NULL,1,'NO','ACTIVE'),(321,26,'SV40','SV40',NULL,1,'NO','ACTIVE'),(322,26,'2 micron','2 micron',NULL,1,'NO','ACTIVE'),(323,26,'unspecified','unspecified',NULL,1,'NO','ACTIVE'),(324,27,'3\' PCMV LTR','3\' PCMV LTR',NULL,1,'NO','ACTIVE'),(325,27,'5\' PCMV LTR','5\' PCMV LTR',NULL,1,'NO','ACTIVE'),(327,27,'Chloramphenicol ORF','Chloramphenicol ORF',NULL,1,'NO','ACTIVE'),(328,27,'CITE','CITE',NULL,1,'NO','ACTIVE'),(331,27,'CMV tissue modifier','CMV tissue modifier',NULL,1,'NO','ACTIVE'),(332,27,'extended packaging signal','extended packaging signal',NULL,1,'NO','ACTIVE'),(333,27,'glycine kinker','glycine kinker',NULL,1,'NO','ACTIVE'),(334,27,'inactivated ccdA','inactivated ccdA',NULL,1,'NO','ACTIVE'),(335,27,'intron','intron',NULL,1,'NO','ACTIVE'),(336,27,'IRES','IRES',NULL,1,'NO','ACTIVE'),(337,27,'lacI repressor','Lac repressor',NULL,1,'NO','ACTIVE'),(340,27,'lacZ alpha','lacZ alpha',NULL,1,'NO','ACTIVE'),(341,27,'MCS','MCS',NULL,1,'NO','ACTIVE'),(342,27,'ori RNA2','ori RNA2',NULL,1,'NO','ACTIVE'),(343,27,'pas 3\' stem','pas 3\' stem',NULL,1,'NO','ACTIVE'),(344,27,'pas 5\' stem','pas 5\' stem',NULL,1,'NO','ACTIVE'),(345,27,'pasL','pasL',NULL,1,'NO','ACTIVE'),(346,27,'PKA site','PKA site',NULL,1,'NO','ACTIVE'),(348,27,'PSI GAG','PSI GAG',NULL,1,'NO','ACTIVE'),(349,27,'repressor','repressor',NULL,1,'NO','ACTIVE'),(350,27,'ROP','ROP ORF',NULL,1,'NO','ACTIVE'),(352,27,'splice acceptor','splice acceptor',NULL,1,'NO','ACTIVE'),(353,27,'splice donor','splice donor',NULL,1,'NO','ACTIVE'),(354,27,'SV40 repressor','SV40 repressor',NULL,1,'NO','ACTIVE'),(359,27,'transcription start site','transcription start site',NULL,1,'NO','ACTIVE'),(360,27,'truncated GAG','truncated GAG',NULL,1,'NO','ACTIVE'),(361,29,'rrnB','rrnB',NULL,1,'NO','ACTIVE'),(362,29,'T7','T7',NULL,1,'NO','ACTIVE'),(363,30,'thrombin','thrombin',NULL,1,'NO','ACTIVE'),(364,30,'enterokinase','enterokinase',NULL,1,'NO','ACTIVE'),(365,30,'TEV','TEV',NULL,1,'NO','ACTIVE'),(366,30,'PreScission Protease','PreScission Protease',NULL,1,'NO','ACTIVE'),(367,30,'Factor Xa','Factor Xa',NULL,1,'NO','ACTIVE'),(368,27,'CAG enhancer','CAG enhancer',NULL,1,'NO','ACTIVE'),(369,27,'Calmodulin Binding Domain','Calmodulin Binding Domain',NULL,1,'NO','ACTIVE'),(370,27,'CAP','CAP',NULL,1,'NO','ACTIVE'),(371,27,'cPPT','cPPT',NULL,1,'NO','ACTIVE'),(372,27,'FRT site','FRT site',NULL,1,'NO','ACTIVE'),(373,27,'Fusion Joint','Fusion Joint',NULL,1,'NO','ACTIVE'),(374,27,'GAG HIV','GAG HIV',NULL,1,'NO','ACTIVE'),(375,27,'HIV REV Nes','HIV REV Nes',NULL,1,'NO','ACTIVE'),(376,27,'HIV-1 3\' LTR','HIV-1 3\' LTR',NULL,1,'NO','ACTIVE'),(377,27,'HIV-1 5\' LTR','HIV-1 5\' LTR',NULL,1,'NO','ACTIVE'),(378,27,'HIV-1 PSI packaging','HIV-1 PSI packaging',NULL,1,'NO','ACTIVE'),(379,27,'Hygromycin ORF resistance minus ATG','Hygromycin ORF resistance minus ATG',NULL,1,'NO','ACTIVE'),(380,27,'Igkappa secretion tag','Igkappa secretion tag',NULL,1,'NO','ACTIVE'),(381,27,'LTR','LTR',NULL,1,'NO','ACTIVE'),(382,27,'protein A','protein A',NULL,1,'NO','ACTIVE'),(385,27,'RRE','RRE',NULL,1,'NO','ACTIVE'),(386,27,'SV40 NLS','SV40 NLS',NULL,1,'NO','ACTIVE'),(387,27,'SV40 polyadenylation signal','SV40 polyadenylation signal',NULL,1,'NO','ACTIVE'),(388,27,'SV40ER reg','SV40ER reg',NULL,1,'NO','ACTIVE'),(389,27,'TetR','TetR',NULL,1,'NO','ACTIVE'),(390,27,'Tetracycline operator','Tetracycline operator',NULL,1,'NO','ACTIVE'),(391,27,'Tn7L','Tn7L',NULL,1,'NO','ACTIVE'),(392,27,'Tn7R','Tn7R',NULL,1,'NO','ACTIVE'),(393,27,'TRE','TRE',NULL,1,'NO','ACTIVE'),(394,27,'VSV G','VSV G',NULL,1,'NO','ACTIVE'),(395,27,'WPRE','WPRE',NULL,1,'NO','ACTIVE'),(396,26,'pMB1','pMB1',NULL,1,'NO','ACTIVE'),(397,25,'rabbit beta-globin','rabbit beta-globin',NULL,1,'NO','ACTIVE'),(398,5,'cBA','cBA',NULL,1,'NO','ACTIVE'),(399,5,'EM7','EM7',NULL,1,'NO','ACTIVE'),(400,5,'HSV tk','HSV tk',NULL,1,'NO','ACTIVE'),(401,5,'lacI reg','lacI reg',NULL,1,'NO','ACTIVE'),(402,5,'SV40','SV40',NULL,1,'NO','ACTIVE'),(403,5,'T3','T3',NULL,1,'NO','ACTIVE'),(404,5,'Ura3','Ura3',NULL,1,'NO','ACTIVE'),(405,5,'PKG','PKG',NULL,1,'NO','ACTIVE'),(406,24,'Gentamicin','Gentamicin',NULL,1,'NO','ACTIVE'),(407,6,'c-myc','c-myc',NULL,1,'NO','ACTIVE'),(409,6,'mitochondria tracker','mitochondria tracker',NULL,1,'NO','ACTIVE'),(410,29,'rrnB T1','rrnB T1',NULL,1,'NO','ACTIVE'),(411,29,'rrnB T2','rrnB T2',NULL,1,'NO','ACTIVE'),(412,29,'rrnB term','rrnB term',NULL,1,'NO','ACTIVE'),(414,6,'Streptag II','Streptag II',NULL,1,'NO','ACTIVE'),(415,5,'GAL1','GAL1',NULL,1,'NO','ACTIVE'),(418,26,'oriV','oriV',NULL,1,'NO','ACTIVE'),(419,26,'ori2','ori2',NULL,1,'NO','ACTIVE'),(420,27,'redF','redF',NULL,1,'NO','ACTIVE'),(421,27,'repE','repE',NULL,1,'NO','ACTIVE'),(422,27,'ParA','ParA',NULL,1,'NO','ACTIVE'),(423,27,'ParB','ParB',NULL,1,'NO','ACTIVE'),(424,27,'ParC','ParC',NULL,1,'NO','ACTIVE'),(425,27,'cosN','cosN',NULL,1,'NO','ACTIVE'),(426,15,'Pan troglodytes',' Pan troglodytes',NULL,1,'NO','ACTIVE'),(436,27,'CMV fwd primer','CMV fwd primer',NULL,1,'NO','ACTIVE'),(437,27,'pBABE rev primer','pBABE rev primer',NULL,1,'NO','ACTIVE'),(438,26,'M13 origin','M13 origin',NULL,1,'NO','ACTIVE'),(439,26,'Polyoma origin','Polyoma origin',NULL,1,'NO','ACTIVE'),(440,27,'T7 primer sequence','T7 primer sequence',NULL,1,'NO','ACTIVE'),(441,27,'SP6 primer sequence','SP6 primer sequence',NULL,1,'NO','ACTIVE'),(442,27,'Splice and PolyA','Splice and PolyA',NULL,1,'NO','ACTIVE'),(443,27,'Chimeric intron','Chimeric intron',NULL,1,'NO','ACTIVE'),(444,27,'Phage F1 region','Phage F1 region',NULL,1,'NO','ACTIVE'),(445,27,'Synthetic polyadenylation signal','Synthetic polyadenylation signal',NULL,1,'NO','ACTIVE'),(485,6,'Streptavidin Binding Peptide','Streptavidin Binding Peptide',NULL,1,'NO','ACTIVE'),(489,24,'LacZ ccdB fusion','LacZ ccdB fusion',NULL,1,'NO','ACTIVE'),(513,20,'','',NULL,1,'NO','DEP'),(514,6,'mVENUS','mVENUS',NULL,1,'NO','ACTIVE'),(515,6,'tandem hcRed','tandem hcRed',NULL,1,'NO','ACTIVE'),(516,6,'tandem mCherry','tandem mCherry',NULL,1,'NO','ACTIVE'),(517,23,'Adenovirus L1 major late intron before 2nd lead','Adenovirus L1 major late intron before 2nd leader',NULL,1,'NO','ACTIVE'),(542,27,'IRES-EGFP','IRES-EGFP',NULL,1,'NO','ACTIVE'),(580,12,'esiRNA','esiRNA',1,1,'NO','ACTIVE'),(584,7,'C-terminus (no ATG)','C-terminus (no ATG)',NULL,1,'NO','ACTIVE'),(629,27,'malE DNA','malE DNA',NULL,1,'NO','ACTIVE'),(630,6,'malE','malE',NULL,1,'NO','ACTIVE'),(665,27,'Chitin Binding Domain','Chitin Binding Domain',NULL,1,'NO','ACTIVE'),(682,20,'V','V',NULL,1,'NO','DEP'),(684,2,'5\' and 3\' ends Sequence Verified','5\' and 3\' ends Sequence Verified',NULL,1,'NO','ACTIVE'),(685,33,'No','No',NULL,1,'NO','ACTIVE'),(686,33,'Yes','Yes',NULL,1,'NO','ACTIVE'),(706,18,'Endothelial','Endothelial',NULL,1,'NO','ACTIVE'),(711,24,'Gentamicin Resistance','Gentamicin Resistance',NULL,1,'NO','ACTIVE'),(722,2,'Gel Verified','Gel Verified',NULL,1,'NO','ACTIVE'),(723,2,'Verified as Primer for PCR','Verified as Primer for PCR',NULL,1,'NO','ACTIVE'),(724,2,'Verified as Sequencing Primer','Verified as Sequencing Primer',NULL,1,'NO','ACTIVE'),(725,15,'Saccharomyces cerevisiae','Saccharomyces cerevisiae',NULL,1,'NO','ACTIVE'),(726,17,'Lung','Lung',NULL,1,'NO','ACTIVE'),(727,37,'LoxP','LoxP',NULL,1,'NO','ACTIVE'),(728,37,'attR2','attR2',NULL,1,'NO','ACTIVE'),(729,6,'MS quantitation','MS quantitation',NULL,1,'NO','ACTIVE'),(730,23,'Adenovirus L1 major late intron before 2nd lead','Adenovirus L1 major late intron before 2nd leader',NULL,1,'NO','ACTIVE'),(731,23,'Adenovirus L1 major late intron before 2nd lead','Adenovirus L1 major late intron before 2nd leader',NULL,1,'NO','ACTIVE'),(732,23,'Adenovirus L1 major late intron before 2nd lead','Adenovirus L1 major late intron before 2nd leader',NULL,1,'NO','ACTIVE'),(733,23,'Adenovirus L1 major late intron before 2nd lead','Adenovirus L1 major late intron before 2nd leader',NULL,1,'NO','ACTIVE'),(734,23,'Adenovirus L1 major late intron before 2nd lead','Adenovirus L1 major late intron before 2nd leader',NULL,1,'NO','ACTIVE'),(735,23,'Adenovirus L1 major late intron before 2nd lead','Adenovirus L1 major late intron before 2nd leader',NULL,1,'NO','ACTIVE'),(736,23,'Adenovirus L1 major late intron before 2nd lead','Adenovirus L1 major late intron before 2nd leader',NULL,1,'NO','ACTIVE'),(737,23,'Adenovirus L1 major late intron before 2nd lead','Adenovirus L1 major late intron before 2nd leader',NULL,1,'NO','ACTIVE'),(738,23,'Adenovirus L1 major late intron before 2nd lead','Adenovirus L1 major late intron before 2nd leader',NULL,1,'NO','ACTIVE'),(739,37,'attR1','attR1',NULL,1,'NO','ACTIVE'),(740,23,'Adenovirus L1 major late intron before 2nd lead','Adenovirus L1 major late intron before 2nd leader',NULL,1,'NO','ACTIVE'),(741,23,'Adenovirus L1 major late intron before 2nd lead','Adenovirus L1 major late intron before 2nd leader',NULL,1,'NO','ACTIVE'),(742,23,'Adenovirus L1 major late intron before 2nd lead','Adenovirus L1 major late intron before 2nd leader',NULL,1,'NO','ACTIVE'),(743,23,'Adenovirus L1 major late intron before 2nd lead','Adenovirus L1 major late intron before 2nd leader',NULL,1,'NO','ACTIVE'),(744,23,'Adenovirus L1 major late intron before 2nd lead','Adenovirus L1 major late intron before 2nd leader',NULL,1,'NO','ACTIVE'),(745,23,'Adenovirus L1 major late intron before 2nd lead','Adenovirus L1 major late intron before 2nd leader',NULL,1,'NO','ACTIVE'),(746,23,'Adenovirus L1 major late intron before 2nd lead','Adenovirus L1 major late intron before 2nd leader',NULL,1,'NO','ACTIVE'),(747,23,'Adenovirus L1 major late intron before 2nd lead','Adenovirus L1 major late intron before 2nd leader',NULL,1,'NO','ACTIVE'),(748,23,'Adenovirus L1 major late intron before 2nd lead','Adenovirus L1 major late intron before 2nd leader',NULL,1,'NO','ACTIVE'),(749,23,'Adenovirus L1 major late intron before 2nd lead','Adenovirus L1 major late intron before 2nd leader',NULL,1,'NO','ACTIVE'),(750,23,'Adenovirus L1 major late intron before 2nd lead','Adenovirus L1 major late intron before 2nd leader',NULL,1,'NO','ACTIVE'),(751,23,'Adenovirus L1 major late intron before 2nd lead','Adenovirus L1 major late intron before 2nd leader',NULL,1,'NO','ACTIVE'),(752,23,'Adenovirus L1 major late intron before 2nd lead','Adenovirus L1 major late intron before 2nd leader',NULL,1,'NO','ACTIVE'),(753,23,'Adenovirus L1 major late intron before 2nd lead','Adenovirus L1 major late intron before 2nd leader',NULL,1,'NO','ACTIVE'),(754,23,'Adenovirus L1 major late intron before 2nd lead','Adenovirus L1 major late intron before 2nd leader',NULL,1,'NO','ACTIVE'),(755,23,'Adenovirus L1 major late intron before 2nd lead','Adenovirus L1 major late intron before 2nd leader',NULL,1,'NO','ACTIVE'),(756,23,'Adenovirus L1 major late intron before 2nd lead','Adenovirus L1 major late intron before 2nd leader',NULL,1,'NO','ACTIVE'),(757,23,'Adenovirus L1 major late intron before 2nd lead','Adenovirus L1 major late intron before 2nd leader',NULL,1,'NO','ACTIVE'),(758,23,'Adenovirus L1 major late intron before 2nd lead','Adenovirus L1 major late intron before 2nd leader',NULL,1,'NO','ACTIVE'),(759,23,'Adenovirus L1 major late intron before 2nd lead','Adenovirus L1 major late intron before 2nd leader',NULL,1,'NO','ACTIVE'),(760,23,'Adenovirus L1 major late intron before 2nd lead','Adenovirus L1 major late intron before 2nd leader',NULL,1,'NO','ACTIVE'),(761,23,'Adenovirus L1 major late intron before 2nd lead','Adenovirus L1 major late intron before 2nd leader',NULL,1,'NO','ACTIVE'),(762,23,'Adenovirus L1 major late intron before 2nd lead','Adenovirus L1 major late intron before 2nd leader',NULL,1,'NO','ACTIVE'),(763,23,'Adenovirus L1 major late intron before 2nd lead','Adenovirus L1 major late intron before 2nd leader',NULL,1,'NO','ACTIVE'),(764,23,'Adenovirus L1 major late intron before 2nd lead','Adenovirus L1 major late intron before 2nd leader',NULL,1,'NO','ACTIVE'),(765,23,'Adenovirus L1 major late intron before 2nd lead','Adenovirus L1 major late intron before 2nd leader',NULL,1,'NO','ACTIVE'),(766,23,'Adenovirus L1 major late intron before 2nd lead','Adenovirus L1 major late intron before 2nd leader',NULL,1,'NO','ACTIVE'),(767,23,'Adenovirus L1 major late intron before 2nd lead','Adenovirus L1 major late intron before 2nd leader',NULL,1,'NO','ACTIVE'),(768,23,'Adenovirus L1 major late intron before 2nd lead','Adenovirus L1 major late intron before 2nd leader',NULL,1,'NO','ACTIVE'),(769,23,'Adenovirus L1 major late intron before 2nd lead','Adenovirus L1 major late intron before 2nd leader',NULL,1,'NO','ACTIVE'),(770,23,'Adenovirus L1 major late intron before 2nd lead','Adenovirus L1 major late intron before 2nd leader',NULL,1,'NO','ACTIVE'),(771,23,'Adenovirus L1 major late intron before 2nd lead','Adenovirus L1 major late intron before 2nd leader',NULL,1,'NO','ACTIVE'),(772,23,'Adenovirus L1 major late intron before 2nd lead','Adenovirus L1 major late intron before 2nd leader',NULL,1,'NO','ACTIVE'),(773,23,'Adenovirus L1 major late intron before 2nd lead','Adenovirus L1 major late intron before 2nd leader',NULL,1,'NO','ACTIVE'),(774,23,'Adenovirus L1 major late intron before 2nd lead','Adenovirus L1 major late intron before 2nd leader',NULL,1,'NO','ACTIVE'),(775,23,'Adenovirus L1 major late intron before 2nd lead','Adenovirus L1 major late intron before 2nd leader',NULL,1,'NO','ACTIVE'),(776,23,'Adenovirus L1 major late intron before 2nd lead','Adenovirus L1 major late intron before 2nd leader',NULL,1,'NO','ACTIVE'),(777,23,'Adenovirus L1 major late intron before 2nd lead','Adenovirus L1 major late intron before 2nd leader',NULL,1,'NO','ACTIVE'),(778,23,'Adenovirus L1 major late intron before 2nd lead','Adenovirus L1 major late intron before 2nd leader',NULL,1,'NO','ACTIVE'),(779,23,'Adenovirus L1 major late intron before 2nd lead','Adenovirus L1 major late intron before 2nd leader',NULL,1,'NO','ACTIVE'),(780,23,'Adenovirus L1 major late intron before 2nd lead','Adenovirus L1 major late intron before 2nd leader',NULL,1,'NO','ACTIVE'),(781,23,'Adenovirus L1 major late intron before 2nd lead','Adenovirus L1 major late intron before 2nd leader',NULL,1,'NO','ACTIVE'),(782,23,'Adenovirus L1 major late intron before 2nd lead','Adenovirus L1 major late intron before 2nd leader',NULL,1,'NO','ACTIVE'),(783,23,'Adenovirus L1 major late intron before 2nd lead','Adenovirus L1 major late intron before 2nd leader',NULL,1,'NO','ACTIVE'),(784,23,'Adenovirus L1 major late intron before 2nd lead','Adenovirus L1 major late intron before 2nd leader',NULL,1,'NO','ACTIVE'),(785,23,'Adenovirus L1 major late intron before 2nd lead','Adenovirus L1 major late intron before 2nd leader',NULL,1,'NO','ACTIVE'),(786,23,'Adenovirus L1 major late intron before 2nd lead','Adenovirus L1 major late intron before 2nd leader',NULL,1,'NO','ACTIVE'),(787,23,'Adenovirus L1 major late intron before 2nd lead','Adenovirus L1 major late intron before 2nd leader',NULL,1,'NO','ACTIVE'),(788,6,'MS peptide quantitation peptides 1 and 2','MS peptide quantitation peptides 1 and 2',NULL,1,'NO','ACTIVE'),(789,6,'MS peptide quantitation peptides 1 and 3','MS peptide quantitation peptides 1 and 3',NULL,1,'NO','ACTIVE'),(790,6,'MS peptide quantitation peptides 2 and 3','MS peptide quantitation peptides 2 and 3',NULL,1,'NO','ACTIVE'),(791,6,'MS quantitation peptide 1','MS quantitation peptide 1',NULL,1,'NO','ACTIVE'),(792,6,'MS quantitation peptide 2','MS quantitation peptide 2',NULL,1,'NO','ACTIVE'),(793,6,'MS quantitation peptide 3','MS quantitation peptide 3',NULL,1,'NO','ACTIVE'),(794,6,'MS quantitation peptides 1 and 2','MS quantitation peptides 1 and 2',NULL,1,'NO','ACTIVE'),(795,6,'MS quantitation peptides 1 and 3','MS quantitation peptides 1 and 3',NULL,1,'NO','ACTIVE'),(796,6,'MS quantitation peptides 2 and 3','MS quantitation peptides 2 and 3',NULL,1,'NO','ACTIVE'),(797,6,'MS quantitation peptide 4','MS quantitation peptide 4',NULL,1,'NO','ACTIVE'),(798,6,'MS quantitation peptide 5','MS quantitation peptide 5',NULL,1,'NO','ACTIVE'),(799,6,'MS quantitation peptide 6','MS quantitation peptide 6',NULL,1,'NO','ACTIVE'),(800,23,'Adenovirus L1 major late intron before 2nd lead','Adenovirus L1 major late intron before 2nd leader',NULL,1,'NO','ACTIVE'),(801,23,'Adenovirus L1 major late intron before 2nd lead','Adenovirus L1 major late intron before 2nd leader',NULL,1,'NO','ACTIVE'),(802,23,'Adenovirus L1 major late intron before 2nd lead','Adenovirus L1 major late intron before 2nd leader',NULL,1,'NO','ACTIVE'),(803,23,'Adenovirus L1 major late intron before 2nd lead','Adenovirus L1 major late intron before 2nd leader',NULL,1,'NO','ACTIVE'),(804,23,'Adenovirus L1 major late intron before 2nd lead','Adenovirus L1 major late intron before 2nd leader',NULL,1,'NO','ACTIVE'),(805,23,'Adenovirus L1 major late intron before 2nd lead','Adenovirus L1 major late intron before 2nd leader',NULL,1,'NO','ACTIVE'),(806,23,'Adenovirus L1 major late intron before 2nd lead','Adenovirus L1 major late intron before 2nd leader',NULL,1,'NO','ACTIVE'),(807,23,'Adenovirus L1 major late intron before 2nd lead','Adenovirus L1 major late intron before 2nd leader',NULL,1,'NO','ACTIVE'),(808,23,'Adenovirus L1 major late intron before 2nd lead','Adenovirus L1 major late intron before 2nd leader',NULL,1,'NO','ACTIVE'),(809,23,'Adenovirus L1 major late intron before 2nd lead','Adenovirus L1 major late intron before 2nd leader',NULL,1,'NO','ACTIVE'),(810,23,'Adenovirus L1 major late intron before 2nd lead','Adenovirus L1 major late intron before 2nd leader',NULL,1,'NO','ACTIVE'),(811,23,'Adenovirus L1 major late intron before 2nd lead','Adenovirus L1 major late intron before 2nd leader',NULL,1,'NO','ACTIVE'),(812,23,'Adenovirus L1 major late intron before 2nd lead','Adenovirus L1 major late intron before 2nd leader',NULL,1,'NO','ACTIVE'),(813,23,'Adenovirus L1 major late intron before 2nd lead','Adenovirus L1 major late intron before 2nd leader',NULL,1,'NO','ACTIVE'),(814,23,'Adenovirus L1 major late intron before 2nd lead','Adenovirus L1 major late intron before 2nd leader',NULL,1,'NO','ACTIVE'),(815,23,'Adenovirus L1 major late intron before 2nd lead','Adenovirus L1 major late intron before 2nd leader',NULL,1,'NO','ACTIVE'),(816,23,'Adenovirus L1 major late intron before 2nd lead','Adenovirus L1 major late intron before 2nd leader',NULL,1,'NO','ACTIVE'),(817,23,'Adenovirus L1 major late intron before 2nd lead','Adenovirus L1 major late intron before 2nd leader',NULL,1,'NO','ACTIVE'),(818,23,'Adenovirus L1 major late intron before 2nd lead','Adenovirus L1 major late intron before 2nd leader',NULL,1,'NO','ACTIVE'),(819,23,'Adenovirus L1 major late intron before 2nd lead','Adenovirus L1 major late intron before 2nd leader',NULL,1,'NO','ACTIVE'),(820,23,'Adenovirus L1 major late intron before 2nd lead','Adenovirus L1 major late intron before 2nd leader',NULL,1,'NO','ACTIVE'),(821,23,'Adenovirus L1 major late intron before 2nd lead','Adenovirus L1 major late intron before 2nd leader',NULL,1,'NO','ACTIVE'),(822,23,'Adenovirus L1 major late intron before 2nd lead','Adenovirus L1 major late intron before 2nd leader',NULL,1,'NO','ACTIVE'),(823,23,'Adenovirus L1 major late intron before 2nd lead','Adenovirus L1 major late intron before 2nd leader',NULL,1,'NO','ACTIVE'),(824,23,'Adenovirus L1 major late intron before 2nd lead','Adenovirus L1 major late intron before 2nd leader',NULL,1,'NO','ACTIVE'),(825,23,'Adenovirus L1 major late intron before 2nd lead','Adenovirus L1 major late intron before 2nd leader',NULL,1,'NO','ACTIVE'),(826,23,'Adenovirus L1 major late intron before 2nd lead','Adenovirus L1 major late intron before 2nd leader',NULL,1,'NO','ACTIVE'),(827,23,'Adenovirus L1 major late intron before 2nd lead','Adenovirus L1 major late intron before 2nd leader',NULL,1,'NO','ACTIVE'),(828,23,'Adenovirus L1 major late intron before 2nd lead','Adenovirus L1 major late intron before 2nd leader',NULL,1,'NO','ACTIVE'),(829,23,'Adenovirus L1 major late intron before 2nd lead','Adenovirus L1 major late intron before 2nd leader',NULL,1,'NO','ACTIVE'),(830,23,'Adenovirus L1 major late intron before 2nd lead','Adenovirus L1 major late intron before 2nd leader',NULL,1,'NO','ACTIVE'),(831,23,'Adenovirus L1 major late intron before 2nd lead','Adenovirus L1 major late intron before 2nd leader',NULL,1,'NO','ACTIVE'),(832,23,'Adenovirus L1 major late intron before 2nd lead','Adenovirus L1 major late intron before 2nd leader',NULL,1,'NO','ACTIVE'),(833,23,'Adenovirus L1 major late intron before 2nd lead','Adenovirus L1 major late intron before 2nd leader',NULL,1,'NO','ACTIVE'),(834,23,'Adenovirus L1 major late intron before 2nd lead','Adenovirus L1 major late intron before 2nd leader',NULL,1,'NO','ACTIVE'),(835,23,'Adenovirus L1 major late intron before 2nd lead','Adenovirus L1 major late intron before 2nd leader',NULL,1,'NO','ACTIVE'),(836,23,'Adenovirus L1 major late intron before 2nd lead','Adenovirus L1 major late intron before 2nd leader',NULL,1,'NO','ACTIVE'),(837,23,'Adenovirus L1 major late intron before 2nd lead','Adenovirus L1 major late intron before 2nd leader',NULL,1,'NO','ACTIVE'),(838,23,'Adenovirus L1 major late intron before 2nd lead','Adenovirus L1 major late intron before 2nd leader',NULL,1,'NO','ACTIVE'),(839,23,'Adenovirus L1 major late intron before 2nd lead','Adenovirus L1 major late intron before 2nd leader',NULL,1,'NO','ACTIVE'),(840,23,'Adenovirus L1 major late intron before 2nd lead','Adenovirus L1 major late intron before 2nd leader',NULL,1,'NO','ACTIVE'),(841,23,'Adenovirus L1 major late intron before 2nd lead','Adenovirus L1 major late intron before 2nd leader',NULL,1,'NO','ACTIVE'),(842,23,'Adenovirus L1 major late intron before 2nd lead','Adenovirus L1 major late intron before 2nd leader',NULL,1,'NO','ACTIVE'),(843,23,'Adenovirus L1 major late intron before 2nd lead','Adenovirus L1 major late intron before 2nd leader',NULL,1,'NO','ACTIVE'),(844,23,'Adenovirus L1 major late intron before 2nd lead','Adenovirus L1 major late intron before 2nd leader',NULL,1,'NO','ACTIVE'),(845,23,'Adenovirus L1 major late intron before 2nd lead','Adenovirus L1 major late intron before 2nd leader',NULL,1,'NO','ACTIVE'),(846,23,'Adenovirus L1 major late intron before 2nd lead','Adenovirus L1 major late intron before 2nd leader',NULL,1,'NO','ACTIVE'),(847,23,'Adenovirus L1 major late intron before 2nd lead','Adenovirus L1 major late intron before 2nd leader',NULL,1,'NO','ACTIVE'),(848,23,'Adenovirus L1 major late intron before 2nd lead','Adenovirus L1 major late intron before 2nd leader',NULL,1,'NO','ACTIVE'),(849,23,'Adenovirus L1 major late intron before 2nd lead','Adenovirus L1 major late intron before 2nd leader',NULL,1,'NO','ACTIVE'),(850,23,'Adenovirus L1 major late intron before 2nd lead','Adenovirus L1 major late intron before 2nd leader',NULL,1,'NO','ACTIVE'),(851,23,'Adenovirus L1 major late intron before 2nd lead','Adenovirus L1 major late intron before 2nd leader',NULL,1,'NO','ACTIVE'),(852,23,'Adenovirus L1 major late intron before 2nd lead','Adenovirus L1 major late intron before 2nd leader',NULL,1,'NO','ACTIVE'),(853,23,'Adenovirus L1 major late intron before 2nd lead','Adenovirus L1 major late intron before 2nd leader',NULL,1,'NO','ACTIVE'),(854,23,'Adenovirus L1 major late intron before 2nd lead','Adenovirus L1 major late intron before 2nd leader',NULL,1,'NO','ACTIVE'),(855,23,'Adenovirus L1 major late intron before 2nd lead','Adenovirus L1 major late intron before 2nd leader',NULL,1,'NO','ACTIVE'),(856,23,'Adenovirus L1 major late intron before 2nd lead','Adenovirus L1 major late intron before 2nd leader',NULL,1,'NO','ACTIVE'),(857,23,'Adenovirus L1 major late intron before 2nd lead','Adenovirus L1 major late intron before 2nd leader',NULL,1,'NO','ACTIVE'),(858,23,'Adenovirus L1 major late intron before 2nd lead','Adenovirus L1 major late intron before 2nd leader',NULL,1,'NO','ACTIVE'),(859,23,'Adenovirus L1 major late intron before 2nd lead','Adenovirus L1 major late intron before 2nd leader',NULL,1,'NO','ACTIVE'),(860,23,'Adenovirus L1 major late intron before 2nd lead','Adenovirus L1 major late intron before 2nd leader',NULL,1,'NO','ACTIVE'),(861,23,'Adenovirus L1 major late intron before 2nd lead','Adenovirus L1 major late intron before 2nd leader',NULL,1,'NO','ACTIVE'),(862,23,'Adenovirus L1 major late intron before 2nd lead','Adenovirus L1 major late intron before 2nd leader',NULL,1,'NO','ACTIVE'),(863,23,'Adenovirus L1 major late intron before 2nd lead','Adenovirus L1 major late intron before 2nd leader',NULL,1,'NO','ACTIVE'),(864,23,'Adenovirus L1 major late intron before 2nd lead','Adenovirus L1 major late intron before 2nd leader',NULL,1,'NO','ACTIVE'),(865,23,'Adenovirus L1 major late intron before 2nd lead','Adenovirus L1 major late intron before 2nd leader',NULL,1,'NO','ACTIVE'),(866,23,'Adenovirus L1 major late intron before 2nd lead','Adenovirus L1 major late intron before 2nd leader',NULL,1,'NO','ACTIVE'),(867,23,'Adenovirus L1 major late intron before 2nd lead','Adenovirus L1 major late intron before 2nd leader',NULL,1,'NO','ACTIVE'),(868,23,'Adenovirus L1 major late intron before 2nd lead','Adenovirus L1 major late intron before 2nd leader',NULL,1,'NO','ACTIVE'),(869,23,'Adenovirus L1 major late intron before 2nd lead','Adenovirus L1 major late intron before 2nd leader',NULL,1,'NO','ACTIVE'),(870,23,'Adenovirus L1 major late intron before 2nd lead','Adenovirus L1 major late intron before 2nd leader',NULL,1,'NO','ACTIVE'),(871,23,'Adenovirus L1 major late intron before 2nd lead','Adenovirus L1 major late intron before 2nd leader',NULL,1,'NO','ACTIVE'),(872,27,'PB 3\' LTR','PB 3\' LTR',NULL,1,'NO','ACTIVE'),(873,27,'PB 5\' LTR','PB 5\' LTR',NULL,1,'NO','ACTIVE'),(874,27,'RV 3\' LTR','RV 3\' LTR',NULL,1,'NO','ACTIVE'),(875,27,'RV 5\' LTR','RV 5\' LTR',NULL,1,'NO','ACTIVE'),(876,23,'Adenovirus L1 major late intron before 2nd lead','Adenovirus L1 major late intron before 2nd leader',NULL,1,'NO','ACTIVE'),(877,23,'Adenovirus L1 major late intron before 2nd lead','Adenovirus L1 major late intron before 2nd leader',NULL,1,'NO','ACTIVE'),(878,23,'Adenovirus L1 major late intron before 2nd lead','Adenovirus L1 major late intron before 2nd leader',NULL,1,'NO','ACTIVE'),(879,23,'Adenovirus L1 major late intron before 2nd lead','Adenovirus L1 major late intron before 2nd leader',NULL,1,'NO','ACTIVE'),(880,23,'Adenovirus L1 major late intron before 2nd lead','Adenovirus L1 major late intron before 2nd leader',NULL,1,'NO','ACTIVE'),(881,23,'Adenovirus L1 major late intron before 2nd lead','Adenovirus L1 major late intron before 2nd leader',NULL,1,'NO','ACTIVE'),(882,23,'Adenovirus L1 major late intron before 2nd lead','Adenovirus L1 major late intron before 2nd leader',NULL,1,'NO','ACTIVE'),(883,23,'Adenovirus L1 major late intron before 2nd lead','Adenovirus L1 major late intron before 2nd leader',NULL,1,'NO','ACTIVE'),(884,23,'Adenovirus L1 major late intron before 2nd lead','Adenovirus L1 major late intron before 2nd leader',NULL,1,'NO','ACTIVE'),(885,23,'Adenovirus L1 major late intron before 2nd lead','Adenovirus L1 major late intron before 2nd leader',NULL,1,'NO','ACTIVE'),(886,23,'Adenovirus L1 major late intron before 2nd lead','Adenovirus L1 major late intron before 2nd leader',NULL,1,'NO','ACTIVE'),(887,23,'Adenovirus L1 major late intron before 2nd lead','Adenovirus L1 major late intron before 2nd leader',NULL,1,'NO','ACTIVE'),(888,23,'Adenovirus L1 major late intron before 2nd lead','Adenovirus L1 major late intron before 2nd leader',NULL,1,'NO','ACTIVE'),(889,23,'Adenovirus L1 major late intron before 2nd lead','Adenovirus L1 major late intron before 2nd leader',NULL,1,'NO','ACTIVE'),(890,23,'Adenovirus L1 major late intron before 2nd lead','Adenovirus L1 major late intron before 2nd leader',NULL,1,'NO','ACTIVE'),(891,23,'Adenovirus L1 major late intron before 2nd lead','Adenovirus L1 major late intron before 2nd leader',NULL,1,'NO','ACTIVE'),(892,23,'Adenovirus L1 major late intron before 2nd lead','Adenovirus L1 major late intron before 2nd leader',NULL,1,'NO','ACTIVE'),(893,23,'Adenovirus L1 major late intron before 2nd lead','Adenovirus L1 major late intron before 2nd leader',NULL,1,'NO','ACTIVE'),(894,23,'Adenovirus L1 major late intron before 2nd lead','Adenovirus L1 major late intron before 2nd leader',NULL,1,'NO','ACTIVE'),(895,23,'Adenovirus L1 major late intron before 2nd lead','Adenovirus L1 major late intron before 2nd leader',NULL,1,'NO','ACTIVE'),(896,23,'Adenovirus L1 major late intron before 2nd lead','Adenovirus L1 major late intron before 2nd leader',NULL,1,'NO','ACTIVE'),(897,23,'Adenovirus L1 major late intron before 2nd lead','Adenovirus L1 major late intron before 2nd leader',NULL,1,'NO','ACTIVE'),(898,23,'Adenovirus L1 major late intron before 2nd lead','Adenovirus L1 major late intron before 2nd leader',NULL,1,'NO','ACTIVE'),(899,23,'Adenovirus L1 major late intron before 2nd lead','Adenovirus L1 major late intron before 2nd leader',NULL,1,'NO','ACTIVE'),(900,23,'Adenovirus L1 major late intron before 2nd lead','Adenovirus L1 major late intron before 2nd leader',NULL,1,'NO','ACTIVE'),(901,23,'Adenovirus L1 major late intron before 2nd lead','Adenovirus L1 major late intron before 2nd leader',NULL,1,'NO','ACTIVE'),(902,23,'Adenovirus L1 major late intron before 2nd lead','Adenovirus L1 major late intron before 2nd leader',NULL,1,'NO','ACTIVE'),(903,23,'Adenovirus L1 major late intron before 2nd lead','Adenovirus L1 major late intron before 2nd leader',NULL,1,'NO','ACTIVE'),(904,23,'Adenovirus L1 major late intron before 2nd lead','Adenovirus L1 major late intron before 2nd leader',NULL,1,'NO','ACTIVE'),(905,23,'Adenovirus L1 major late intron before 2nd lead','Adenovirus L1 major late intron before 2nd leader',NULL,1,'NO','ACTIVE'),(906,23,'Adenovirus L1 major late intron before 2nd lead','Adenovirus L1 major late intron before 2nd leader',NULL,1,'NO','ACTIVE'),(907,23,'Adenovirus L1 major late intron before 2nd lead','Adenovirus L1 major late intron before 2nd leader',NULL,1,'NO','ACTIVE'),(908,23,'Adenovirus L1 major late intron before 2nd lead','Adenovirus L1 major late intron before 2nd leader',NULL,1,'NO','ACTIVE'),(909,23,'Adenovirus L1 major late intron before 2nd lead','Adenovirus L1 major late intron before 2nd leader',NULL,1,'NO','ACTIVE'),(910,23,'Adenovirus L1 major late intron before 2nd lead','Adenovirus L1 major late intron before 2nd leader',NULL,1,'NO','ACTIVE'),(911,23,'Adenovirus L1 major late intron before 2nd lead','Adenovirus L1 major late intron before 2nd leader',NULL,1,'NO','ACTIVE'),(912,23,'Adenovirus L1 major late intron before 2nd lead','Adenovirus L1 major late intron before 2nd leader',NULL,1,'NO','ACTIVE'),(913,23,'Adenovirus L1 major late intron before 2nd lead','Adenovirus L1 major late intron before 2nd leader',NULL,1,'NO','ACTIVE'),(914,23,'Adenovirus L1 major late intron before 2nd lead','Adenovirus L1 major late intron before 2nd leader',NULL,1,'NO','ACTIVE'),(915,23,'Adenovirus L1 major late intron before 2nd lead','Adenovirus L1 major late intron before 2nd leader',NULL,1,'NO','ACTIVE'),(916,23,'Adenovirus L1 major late intron before 2nd lead','Adenovirus L1 major late intron before 2nd leader',NULL,1,'NO','ACTIVE'),(917,23,'Adenovirus L1 major late intron before 2nd lead','Adenovirus L1 major late intron before 2nd leader',NULL,1,'NO','ACTIVE'),(918,23,'Adenovirus L1 major late intron before 2nd lead','Adenovirus L1 major late intron before 2nd leader',NULL,1,'NO','ACTIVE'),(919,23,'Adenovirus L1 major late intron before 2nd lead','Adenovirus L1 major late intron before 2nd leader',NULL,1,'NO','ACTIVE'),(920,23,'Adenovirus L1 major late intron before 2nd lead','Adenovirus L1 major late intron before 2nd leader',NULL,1,'NO','ACTIVE'),(921,23,'Adenovirus L1 major late intron before 2nd lead','Adenovirus L1 major late intron before 2nd leader',NULL,1,'NO','ACTIVE'),(922,23,'Adenovirus L1 major late intron before 2nd lead','Adenovirus L1 major late intron before 2nd leader',NULL,1,'NO','ACTIVE'),(923,23,'Adenovirus L1 major late intron before 2nd lead','Adenovirus L1 major late intron before 2nd leader',NULL,1,'NO','ACTIVE'),(924,23,'Adenovirus L1 major late intron before 2nd lead','Adenovirus L1 major late intron before 2nd leader',NULL,1,'NO','ACTIVE'),(925,23,'Adenovirus L1 major late intron before 2nd lead','Adenovirus L1 major late intron before 2nd leader',NULL,1,'NO','ACTIVE'),(926,23,'Adenovirus L1 major late intron before 2nd lead','Adenovirus L1 major late intron before 2nd leader',NULL,1,'NO','ACTIVE'),(927,23,'Adenovirus L1 major late intron before 2nd lead','Adenovirus L1 major late intron before 2nd leader',NULL,1,'NO','ACTIVE'),(928,23,'Adenovirus L1 major late intron before 2nd lead','Adenovirus L1 major late intron before 2nd leader',NULL,1,'NO','ACTIVE'),(929,23,'Adenovirus L1 major late intron before 2nd lead','Adenovirus L1 major late intron before 2nd leader',NULL,1,'NO','ACTIVE'),(930,23,'Adenovirus L1 major late intron before 2nd lead','Adenovirus L1 major late intron before 2nd leader',NULL,1,'NO','ACTIVE'),(931,23,'Adenovirus L1 major late intron before 2nd lead','Adenovirus L1 major late intron before 2nd leader',NULL,1,'NO','ACTIVE'),(932,23,'Adenovirus L1 major late intron before 2nd lead','Adenovirus L1 major late intron before 2nd leader',NULL,1,'NO','ACTIVE'),(933,23,'Adenovirus L1 major late intron before 2nd lead','Adenovirus L1 major late intron before 2nd leader',NULL,1,'NO','ACTIVE'),(934,23,'Adenovirus L1 major late intron before 2nd lead','Adenovirus L1 major late intron before 2nd leader',NULL,1,'NO','ACTIVE'),(935,23,'Adenovirus L1 major late intron before 2nd lead','Adenovirus L1 major late intron before 2nd leader',NULL,1,'NO','ACTIVE'),(936,23,'Adenovirus L1 major late intron before 2nd lead','Adenovirus L1 major late intron before 2nd leader',NULL,1,'NO','ACTIVE'),(937,23,'Adenovirus L1 major late intron before 2nd lead','Adenovirus L1 major late intron before 2nd leader',NULL,1,'NO','ACTIVE'),(938,23,'Adenovirus L1 major late intron before 2nd lead','Adenovirus L1 major late intron before 2nd leader',NULL,1,'NO','ACTIVE'),(939,23,'Adenovirus L1 major late intron before 2nd lead','Adenovirus L1 major late intron before 2nd leader',NULL,1,'NO','ACTIVE'),(940,23,'Adenovirus L1 major late intron before 2nd lead','Adenovirus L1 major late intron before 2nd leader',NULL,1,'NO','ACTIVE'),(941,23,'Adenovirus L1 major late intron before 2nd lead','Adenovirus L1 major late intron before 2nd leader',NULL,1,'NO','ACTIVE'),(942,23,'Adenovirus L1 major late intron before 2nd lead','Adenovirus L1 major late intron before 2nd leader',NULL,1,'NO','ACTIVE'),(943,23,'Adenovirus L1 major late intron before 2nd lead','Adenovirus L1 major late intron before 2nd leader',NULL,1,'NO','ACTIVE'),(944,23,'Adenovirus L1 major late intron before 2nd lead','Adenovirus L1 major late intron before 2nd leader',NULL,1,'NO','ACTIVE'),(945,23,'Adenovirus L1 major late intron before 2nd lead','Adenovirus L1 major late intron before 2nd leader',NULL,1,'NO','ACTIVE'),(946,23,'Adenovirus L1 major late intron before 2nd lead','Adenovirus L1 major late intron before 2nd leader',NULL,1,'NO','ACTIVE'),(947,23,'Adenovirus L1 major late intron before 2nd lead','Adenovirus L1 major late intron before 2nd leader',NULL,1,'NO','ACTIVE'),(948,37,'attB1','attB1',NULL,1,'NO','ACTIVE'),(949,37,'attB2','attB2',NULL,1,'NO','ACTIVE'),(950,23,'Adenovirus L1 major late intron before 2nd lead','Adenovirus L1 major late intron before 2nd leader',NULL,1,'NO','ACTIVE'),(951,23,'Adenovirus L1 major late intron before 2nd lead','Adenovirus L1 major late intron before 2nd leader',NULL,1,'NO','ACTIVE'),(952,23,'Adenovirus L1 major late intron before 2nd lead','Adenovirus L1 major late intron before 2nd leader',NULL,1,'NO','ACTIVE'),(953,23,'Adenovirus L1 major late intron before 2nd lead','Adenovirus L1 major late intron before 2nd leader',NULL,1,'NO','ACTIVE'),(954,23,'Adenovirus L1 major late intron before 2nd lead','Adenovirus L1 major late intron before 2nd leader',NULL,1,'NO','ACTIVE'),(955,23,'Adenovirus L1 major late intron before 2nd lead','Adenovirus L1 major late intron before 2nd leader',NULL,1,'NO','ACTIVE'),(956,23,'Adenovirus L1 major late intron before 2nd lead','Adenovirus L1 major late intron before 2nd leader',NULL,1,'NO','ACTIVE'),(957,23,'Adenovirus L1 major late intron before 2nd lead','Adenovirus L1 major late intron before 2nd leader',NULL,1,'NO','ACTIVE'),(958,23,'Adenovirus L1 major late intron before 2nd lead','Adenovirus L1 major late intron before 2nd leader',NULL,1,'NO','ACTIVE'),(959,23,'Adenovirus L1 major late intron before 2nd lead','Adenovirus L1 major late intron before 2nd leader',NULL,1,'NO','ACTIVE'),(960,23,'Adenovirus L1 major late intron before 2nd lead','Adenovirus L1 major late intron before 2nd leader',NULL,1,'NO','ACTIVE'),(961,23,'Adenovirus L1 major late intron before 2nd lead','Adenovirus L1 major late intron before 2nd leader',NULL,1,'NO','ACTIVE'),(962,23,'Adenovirus L1 major late intron before 2nd lead','Adenovirus L1 major late intron before 2nd leader',NULL,1,'NO','ACTIVE'),(963,23,'Adenovirus L1 major late intron before 2nd lead','Adenovirus L1 major late intron before 2nd leader',NULL,1,'NO','ACTIVE'),(964,23,'Adenovirus L1 major late intron before 2nd lead','Adenovirus L1 major late intron before 2nd leader',NULL,1,'NO','ACTIVE'),(965,23,'Adenovirus L1 major late intron before 2nd lead','Adenovirus L1 major late intron before 2nd leader',NULL,1,'NO','ACTIVE'),(966,23,'Adenovirus L1 major late intron before 2nd lead','Adenovirus L1 major late intron before 2nd leader',NULL,1,'NO','ACTIVE'),(967,23,'Adenovirus L1 major late intron before 2nd lead','Adenovirus L1 major late intron before 2nd leader',NULL,1,'NO','ACTIVE'),(968,23,'Adenovirus L1 major late intron before 2nd lead','Adenovirus L1 major late intron before 2nd leader',NULL,1,'NO','ACTIVE'),(969,23,'Adenovirus L1 major late intron before 2nd lead','Adenovirus L1 major late intron before 2nd leader',NULL,1,'NO','ACTIVE'),(970,23,'Adenovirus L1 major late intron before 2nd lead','Adenovirus L1 major late intron before 2nd leader',NULL,1,'NO','ACTIVE'),(971,23,'Adenovirus L1 major late intron before 2nd lead','Adenovirus L1 major late intron before 2nd leader',NULL,1,'NO','ACTIVE'),(972,23,'Adenovirus L1 major late intron before 2nd lead','Adenovirus L1 major late intron before 2nd leader',NULL,1,'NO','ACTIVE'),(973,23,'Adenovirus L1 major late intron before 2nd lead','Adenovirus L1 major late intron before 2nd leader',NULL,1,'NO','ACTIVE'),(974,23,'Adenovirus L1 major late intron before 2nd lead','Adenovirus L1 major late intron before 2nd leader',NULL,1,'NO','ACTIVE'),(975,23,'Adenovirus L1 major late intron before 2nd lead','Adenovirus L1 major late intron before 2nd leader',NULL,1,'NO','ACTIVE'),(976,23,'Adenovirus L1 major late intron before 2nd lead','Adenovirus L1 major late intron before 2nd leader',NULL,1,'NO','ACTIVE'),(977,23,'Adenovirus L1 major late intron before 2nd lead','Adenovirus L1 major late intron before 2nd leader',NULL,1,'NO','ACTIVE'),(978,23,'Adenovirus L1 major late intron before 2nd lead','Adenovirus L1 major late intron before 2nd leader',NULL,1,'NO','ACTIVE'),(979,23,'Adenovirus L1 major late intron before 2nd lead','Adenovirus L1 major late intron before 2nd leader',NULL,1,'NO','ACTIVE'),(980,23,'Adenovirus L1 major late intron before 2nd lead','Adenovirus L1 major late intron before 2nd leader',NULL,1,'NO','ACTIVE'),(981,23,'Adenovirus L1 major late intron before 2nd lead','Adenovirus L1 major late intron before 2nd leader',NULL,1,'NO','ACTIVE'),(982,23,'Adenovirus L1 major late intron before 2nd lead','Adenovirus L1 major late intron before 2nd leader',NULL,1,'NO','ACTIVE'),(983,23,'Adenovirus L1 major late intron before 2nd lead','Adenovirus L1 major late intron before 2nd leader',NULL,1,'NO','ACTIVE'),(984,23,'Adenovirus L1 major late intron before 2nd lead','Adenovirus L1 major late intron before 2nd leader',NULL,1,'NO','ACTIVE'),(985,23,'Adenovirus L1 major late intron before 2nd lead','Adenovirus L1 major late intron before 2nd leader',NULL,1,'NO','ACTIVE'),(986,23,'Adenovirus L1 major late intron before 2nd lead','Adenovirus L1 major late intron before 2nd leader',NULL,1,'NO','ACTIVE'),(987,23,'Adenovirus L1 major late intron before 2nd lead','Adenovirus L1 major late intron before 2nd leader',NULL,1,'NO','ACTIVE'),(988,23,'Adenovirus L1 major late intron before 2nd lead','Adenovirus L1 major late intron before 2nd leader',NULL,1,'NO','ACTIVE'),(989,23,'Adenovirus L1 major late intron before 2nd lead','Adenovirus L1 major late intron before 2nd leader',NULL,1,'NO','ACTIVE'),(990,23,'Adenovirus L1 major late intron before 2nd lead','Adenovirus L1 major late intron before 2nd leader',NULL,1,'NO','ACTIVE'),(991,23,'Adenovirus L1 major late intron before 2nd lead','Adenovirus L1 major late intron before 2nd leader',NULL,1,'NO','ACTIVE'),(992,23,'Adenovirus L1 major late intron before 2nd lead','Adenovirus L1 major late intron before 2nd leader',NULL,1,'NO','ACTIVE'),(993,23,'Adenovirus L1 major late intron before 2nd lead','Adenovirus L1 major late intron before 2nd leader',NULL,1,'NO','ACTIVE'),(994,23,'Adenovirus L1 major late intron before 2nd lead','Adenovirus L1 major late intron before 2nd leader',NULL,1,'NO','ACTIVE'),(995,23,'Adenovirus L1 major late intron before 2nd lead','Adenovirus L1 major late intron before 2nd leader',NULL,1,'NO','ACTIVE'),(996,23,'Adenovirus L1 major late intron before 2nd lead','Adenovirus L1 major late intron before 2nd leader',NULL,1,'NO','ACTIVE'),(997,23,'Adenovirus L1 major late intron before 2nd lead','Adenovirus L1 major late intron before 2nd leader',NULL,1,'NO','ACTIVE'),(998,23,'Adenovirus L1 major late intron before 2nd lead','Adenovirus L1 major late intron before 2nd leader',NULL,1,'NO','ACTIVE'),(999,23,'Adenovirus L1 major late intron before 2nd lead','Adenovirus L1 major late intron before 2nd leader',NULL,1,'NO','ACTIVE'),(1000,23,'Adenovirus L1 major late intron before 2nd lead','Adenovirus L1 major late intron before 2nd leader',NULL,1,'NO','ACTIVE'),(1001,23,'Adenovirus L1 major late intron before 2nd lead','Adenovirus L1 major late intron before 2nd leader',NULL,1,'NO','ACTIVE'),(1002,23,'Adenovirus L1 major late intron before 2nd lead','Adenovirus L1 major late intron before 2nd leader',NULL,1,'NO','ACTIVE'),(1003,23,'Adenovirus L1 major late intron before 2nd lead','Adenovirus L1 major late intron before 2nd leader',NULL,1,'NO','ACTIVE'),(1004,23,'Adenovirus L1 major late intron before 2nd lead','Adenovirus L1 major late intron before 2nd leader',NULL,1,'NO','ACTIVE'),(1005,23,'Adenovirus L1 major late intron before 2nd lead','Adenovirus L1 major late intron before 2nd leader',NULL,1,'NO','ACTIVE'),(1006,23,'Adenovirus L1 major late intron before 2nd lead','Adenovirus L1 major late intron before 2nd leader',NULL,1,'NO','ACTIVE'),(1007,27,'M13-fwd','M13-fwd',NULL,1,'NO','ACTIVE'),(1008,27,'M13-rev','M13-rev',NULL,1,'NO','ACTIVE'),(1009,23,'Adenovirus L1 major late intron before 2nd lead','Adenovirus L1 major late intron before 2nd leader',NULL,1,'NO','ACTIVE'),(1010,23,'Adenovirus L1 major late intron before 2nd lead','Adenovirus L1 major late intron before 2nd leader',NULL,1,'NO','ACTIVE'),(1011,23,'Adenovirus L1 major late intron before 2nd lead','Adenovirus L1 major late intron before 2nd leader',NULL,1,'NO','ACTIVE'),(1012,23,'Adenovirus L1 major late intron before 2nd lead','Adenovirus L1 major late intron before 2nd leader',NULL,1,'NO','ACTIVE'),(1013,23,'Adenovirus L1 major late intron before 2nd lead','Adenovirus L1 major late intron before 2nd leader',NULL,1,'NO','ACTIVE'),(1014,23,'Adenovirus L1 major late intron before 2nd lead','Adenovirus L1 major late intron before 2nd leader',NULL,1,'NO','ACTIVE'),(1015,23,'Adenovirus L1 major late intron before 2nd lead','Adenovirus L1 major late intron before 2nd leader',NULL,1,'NO','ACTIVE'),(1016,23,'Adenovirus L1 major late intron before 2nd lead','Adenovirus L1 major late intron before 2nd leader',NULL,1,'NO','ACTIVE'),(1017,23,'Adenovirus L1 major late intron before 2nd lead','Adenovirus L1 major late intron before 2nd leader',NULL,1,'NO','ACTIVE'),(1018,23,'Adenovirus L1 major late intron before 2nd lead','Adenovirus L1 major late intron before 2nd leader',NULL,1,'NO','ACTIVE'),(1019,23,'Adenovirus L1 major late intron before 2nd lead','Adenovirus L1 major late intron before 2nd leader',NULL,1,'NO','ACTIVE'),(1020,23,'Adenovirus L1 major late intron before 2nd lead','Adenovirus L1 major late intron before 2nd leader',NULL,1,'NO','ACTIVE'),(1021,23,'Adenovirus L1 major late intron before 2nd lead','Adenovirus L1 major late intron before 2nd leader',NULL,1,'NO','ACTIVE'),(1022,23,'Adenovirus L1 major late intron before 2nd lead','Adenovirus L1 major late intron before 2nd leader',NULL,1,'NO','ACTIVE'),(1023,23,'Adenovirus L1 major late intron before 2nd lead','Adenovirus L1 major late intron before 2nd leader',NULL,1,'NO','ACTIVE'),(1024,23,'Adenovirus L1 major late intron before 2nd lead','Adenovirus L1 major late intron before 2nd leader',NULL,1,'NO','ACTIVE'),(1025,23,'Adenovirus L1 major late intron before 2nd lead','Adenovirus L1 major late intron before 2nd leader',NULL,1,'NO','ACTIVE'),(1026,23,'Adenovirus L1 major late intron before 2nd lead','Adenovirus L1 major late intron before 2nd leader',NULL,1,'NO','ACTIVE'),(1027,23,'Adenovirus L1 major late intron before 2nd lead','Adenovirus L1 major late intron before 2nd leader',NULL,1,'NO','ACTIVE'),(1028,23,'Adenovirus L1 major late intron before 2nd lead','Adenovirus L1 major late intron before 2nd leader',NULL,1,'NO','ACTIVE'),(1029,23,'Adenovirus L1 major late intron before 2nd lead','Adenovirus L1 major late intron before 2nd leader',NULL,1,'NO','ACTIVE'),(1030,23,'Adenovirus L1 major late intron before 2nd lead','Adenovirus L1 major late intron before 2nd leader',NULL,1,'NO','ACTIVE'),(1031,23,'Adenovirus L1 major late intron before 2nd lead','Adenovirus L1 major late intron before 2nd leader',NULL,1,'NO','ACTIVE'),(1032,23,'Adenovirus L1 major late intron before 2nd lead','Adenovirus L1 major late intron before 2nd leader',NULL,1,'NO','ACTIVE'),(1033,23,'Adenovirus L1 major late intron before 2nd lead','Adenovirus L1 major late intron before 2nd leader',NULL,1,'NO','ACTIVE'),(1034,23,'Adenovirus L1 major late intron before 2nd lead','Adenovirus L1 major late intron before 2nd leader',NULL,1,'NO','ACTIVE'),(1035,23,'Adenovirus L1 major late intron before 2nd lead','Adenovirus L1 major late intron before 2nd leader',NULL,1,'NO','ACTIVE'),(1036,27,'GGC-->GC','GGC-->GC',NULL,1,'NO','ACTIVE'),(1037,27,'T-->C','T-->C',NULL,1,'NO','ACTIVE'),(1038,24,'b-geo','b-geo',NULL,1,'NO','ACTIVE'),(1039,27,'LacZ','LacZ',NULL,1,'NO','ACTIVE'),(1040,27,'pA','pA',NULL,1,'NO','ACTIVE'),(1041,5,'tetO2','tetO2',NULL,1,'NO','ACTIVE'),(1042,5,'LacO','LacO',NULL,1,'NO','ACTIVE'),(1043,5,'mCMV','mCMV',NULL,1,'NO','ACTIVE'),(1044,27,'TATA','TATA',NULL,1,'NO','ACTIVE'),(1045,27,'IR/DR(L)\\Lmut44','IR/DR(L)\\Lmut44',NULL,1,'NO','ACTIVE'),(1046,27,'IR/DR(R)\\Rmut13','IR/DR(R)\\Rmut13',NULL,1,'NO','ACTIVE'),(1047,37,'PacI','PacI',NULL,1,'NO','ACTIVE'),(1048,27,'mOct-4 5\'UTR','mOct-4 5\'UTR',NULL,1,'NO','ACTIVE'),(1049,27,'murine Oct-4','murine Oct-4',NULL,1,'NO','ACTIVE'),(1050,27,'Oct4-RTF','Oct4-RTF',NULL,1,'NO','ACTIVE'),(1051,27,'mOct-4 3\'UTR','mOct-4 3\'UTR',NULL,1,'NO','ACTIVE'),(1052,27,'TFIBRTR','TFIBRTR',NULL,1,'NO','ACTIVE'),(1053,37,'attL1','attL1',NULL,1,'NO','ACTIVE'),(1054,37,'attL2','attL2',NULL,1,'NO','ACTIVE'),(1055,27,'PBseqPac','PBseqPac',NULL,1,'NO','ACTIVE'),(1056,27,'plasmid','plasmid',NULL,1,'NO','ACTIVE'),(1057,27,'MoMLV U3','MoMLV U3',NULL,1,'NO','ACTIVE'),(1058,27,'MoMLV R','MoMLV R',NULL,1,'NO','ACTIVE'),(1059,27,'MoMLV U5','MoMLV U5',NULL,1,'NO','ACTIVE'),(1060,27,'PBS Pro-tRNA','PBS Pro-tRNA',NULL,1,'NO','ACTIVE'),(1061,27,'MLV gag','MLV gag',NULL,1,'NO','ACTIVE'),(1062,27,'delta gag','delta gag',NULL,1,'NO','ACTIVE'),(1063,27,'PPT','PPT',NULL,1,'NO','ACTIVE'),(1064,27,'PBseqNhe','PBseqNhe',NULL,1,'NO','ACTIVE'),(1065,27,'mcMyc','mcMyc',NULL,1,'NO','ACTIVE'),(1066,27,'GSG linker','GSG linker',NULL,1,'NO','ACTIVE'),(1067,27,'F2A','F2A',NULL,1,'NO','ACTIVE'),(1068,27,'mKlf4','mKlf4',NULL,1,'NO','ACTIVE'),(1069,27,'T2A','T2A',NULL,1,'NO','ACTIVE'),(1070,27,'mOct4','mOct4',NULL,1,'NO','ACTIVE'),(1071,27,'E2A','E2A',NULL,1,'NO','ACTIVE'),(1072,27,'mSox2','mSox2',NULL,1,'NO','ACTIVE'),(1073,27,'Klf4-RTF','Klf4-RTF',NULL,1,'NO','ACTIVE'),(1074,27,'mKlf4 3\'UTR','mKlf4 3\'UTR',NULL,1,'NO','ACTIVE'),(1075,27,'Sox2','Sox2',NULL,1,'NO','ACTIVE'),(1076,27,'Sox2-RTF','Sox2-RTF',NULL,1,'NO','ACTIVE'),(1077,27,'c-Myc','c-Myc',NULL,1,'NO','ACTIVE'),(1078,27,'c-Myc-RTF','c-Myc-RTF',NULL,1,'NO','ACTIVE'),(1079,23,'Adenovirus L1 major late intron before 2nd lead','Adenovirus L1 major late intron before 2nd leader',NULL,1,'NO','ACTIVE'),(1080,23,'Adenovirus L1 major late intron before 2nd lead','Adenovirus L1 major late intron before 2nd leader',NULL,1,'NO','ACTIVE'),(1081,23,'Adenovirus L1 major late intron before 2nd lead','Adenovirus L1 major late intron before 2nd leader',NULL,1,'NO','ACTIVE'),(1082,23,'Adenovirus L1 major late intron before 2nd lead','Adenovirus L1 major late intron before 2nd leader',NULL,1,'NO','ACTIVE'),(1083,23,'Adenovirus L1 major late intron before 2nd lead','Adenovirus L1 major late intron before 2nd leader',NULL,1,'NO','ACTIVE'),(1084,23,'Adenovirus L1 major late intron before 2nd lead','Adenovirus L1 major late intron before 2nd leader',NULL,1,'NO','ACTIVE'),(1085,23,'Adenovirus L1 major late intron before 2nd lead','Adenovirus L1 major late intron before 2nd leader',NULL,1,'NO','ACTIVE'),(1086,23,'Adenovirus L1 major late intron before 2nd lead','Adenovirus L1 major late intron before 2nd leader',NULL,1,'NO','ACTIVE'),(1087,23,'Adenovirus L1 major late intron before 2nd lead','Adenovirus L1 major late intron before 2nd leader',NULL,1,'NO','ACTIVE'),(1088,23,'Adenovirus L1 major late intron before 2nd lead','Adenovirus L1 major late intron before 2nd leader',NULL,1,'NO','ACTIVE'),(1089,23,'Adenovirus L1 major late intron before 2nd lead','Adenovirus L1 major late intron before 2nd leader',NULL,1,'NO','ACTIVE'),(1090,23,'Adenovirus L1 major late intron before 2nd lead','Adenovirus L1 major late intron before 2nd leader',NULL,1,'NO','ACTIVE'),(1091,23,'Adenovirus L1 major late intron before 2nd lead','Adenovirus L1 major late intron before 2nd leader',NULL,1,'NO','ACTIVE'),(1092,23,'Adenovirus L1 major late intron before 2nd lead','Adenovirus L1 major late intron before 2nd leader',NULL,1,'NO','ACTIVE'),(1093,23,'Adenovirus L1 major late intron before 2nd lead','Adenovirus L1 major late intron before 2nd leader',NULL,1,'NO','ACTIVE'),(1094,23,'Adenovirus L1 major late intron before 2nd lead','Adenovirus L1 major late intron before 2nd leader',NULL,1,'NO','ACTIVE'),(1095,23,'Adenovirus L1 major late intron before 2nd lead','Adenovirus L1 major late intron before 2nd leader',NULL,1,'NO','ACTIVE'),(1096,23,'Adenovirus L1 major late intron before 2nd lead','Adenovirus L1 major late intron before 2nd leader',NULL,1,'NO','ACTIVE'),(1097,27,'mOrange','mOrange',NULL,1,'NO','ACTIVE'),(1098,27,'EGFP','EGFP',NULL,1,'NO','ACTIVE'),(1099,27,'mCherry ORF','mCherry ORF',NULL,1,'NO','ACTIVE'),(1100,27,'EBFP2','EBFP2',NULL,1,'NO','ACTIVE'),(1101,27,'cloNAT','cloNAT',NULL,1,'NO','ACTIVE'),(1102,27,'2A','2A',NULL,1,'NO','ACTIVE'),(1103,27,'FMDV-2A','FMDV-2A',NULL,1,'NO','ACTIVE'),(1104,27,'pMXs','pMXs',NULL,1,'NO','ACTIVE'),(1105,27,'Klf4','Klf4',NULL,1,'NO','ACTIVE'),(1106,37,'TOPO','TOPO',NULL,1,'NO','ACTIVE'),(1107,27,'mKlf4 pCX_OKS','mKlf4 pCX_OKS',NULL,1,'NO','ACTIVE'),(1108,27,'Oct3/4','Oct3/4',NULL,1,'NO','ACTIVE'),(1109,27,'Sox2-S768','Sox2-S768',NULL,1,'NO','ACTIVE'),(1110,25,'SV40 terminator','SV40 terminator',NULL,1,'NO','ACTIVE'),(1111,25,'SV40 late','SV40 late',NULL,1,'NO','ACTIVE'),(1112,25,'rBG terminator','rBG terminator',NULL,1,'NO','ACTIVE'),(1113,27,'EBV_rev_primer','EBV_rev_primer',NULL,1,'NO','ACTIVE'),(1114,5,'CAG','CAG',NULL,1,'NO','ACTIVE'),(1115,27,'tdTomato','tdTomato',NULL,1,'NO','ACTIVE'),(1116,5,'rtTA-2','rtTA-2',NULL,1,'NO','ACTIVE'),(1117,27,'BCKam primer','BCKam primer',NULL,1,'NO','ACTIVE'),(1118,27,'5\'GAP','5\'GAP',NULL,1,'NO','ACTIVE'),(1119,27,'ENSMUSG00000032202','ENSMUSG00000032202',NULL,1,'NO','ACTIVE'),(1120,27,'dsRed insertion 3\'','dsRed insertion 3\'',NULL,1,'NO','ACTIVE'),(1121,27,'R2 primer','R2 primer',NULL,1,'NO','ACTIVE'),(1122,27,'seq primer','seq primer',NULL,1,'NO','ACTIVE'),(1123,27,'R1 primer','R1 primer',NULL,1,'NO','ACTIVE'),(1124,27,'dsRed insertion 5\'','dsRed insertion 5\'',NULL,1,'NO','ACTIVE'),(1125,27,'3\'GAP','3\'GAP',NULL,1,'NO','ACTIVE'),(1126,27,'MDT primer','MDT primer',NULL,1,'NO','ACTIVE'),(1127,27,'DTA start','DTA start',NULL,1,'NO','ACTIVE'),(1128,27,'DTA ','DTA ',NULL,1,'NO','ACTIVE'),(1129,27,'DTA stop','DTA stop',NULL,1,'NO','ACTIVE'),(1130,5,'MC1','MC1',NULL,1,'NO','ACTIVE'),(1131,25,'PGK ','PGK ',NULL,1,'NO','ACTIVE'),(1132,27,'TAPR','TAPR',NULL,1,'NO','ACTIVE'),(1133,27,'FRT','FRT',NULL,1,'NO','ACTIVE'),(1134,27,'TAPF','TAPF',NULL,1,'NO','ACTIVE'),(1135,5,'PGK','PGK',NULL,1,'NO','ACTIVE'),(1136,25,'putative','putative',NULL,1,'NO','ACTIVE'),(1137,27,'F3','F3',NULL,1,'NO','ACTIVE'),(1138,27,'polyoma virus late promoter','polyoma virus late promoter',NULL,1,'NO','ACTIVE'),(1139,27,'G-->A','G-->A',NULL,1,'NO','ACTIVE'),(1140,27,'TK2 ORF','TK2 ORF',NULL,1,'NO','ACTIVE'),(1141,27,'unknown','unknown',NULL,1,'NO','ACTIVE'),(1142,27,'HSVTK2 3\'','HSVTK2 3\'',NULL,1,'NO','ACTIVE'),(1143,27,'PGK F','PGK F',NULL,1,'NO','ACTIVE'),(1144,27,'TK-R primer','TK-R primer',NULL,1,'NO','ACTIVE'),(1145,27,'NLSFLPoSty1F','NLSFLPoSty1F',NULL,1,'NO','ACTIVE'),(1146,27,'insert','insert',NULL,1,'NO','ACTIVE'),(1147,27,'BGH Rev','BGH Rev',NULL,1,'NO','ACTIVE'),(1148,27,'BPA','BPA',NULL,1,'NO','ACTIVE'),(1149,37,'BsrGI','BsrGI',NULL,1,'NO','ACTIVE'),(1150,5,'HSV2 TK','HSV2 TK',NULL,1,'NO','ACTIVE'),(1157,2,'Ectopic Protein Verified','Ectopic Protein Verified',NULL,1,'NO','DEP'),(1158,2,'Endogenous Protein Verified','Endogenous Protein Verified',NULL,1,'NO','DEP'),(1227,27,'iNBBrep','iNBBrep',NULL,1,'NO','ACTIVE'),(1228,27,'En2 SA','En2 SA',NULL,1,'NO','ACTIVE'),(1229,27,'MC1Rev','MC1Rev',NULL,1,'NO','ACTIVE'),(1230,27,'TempF','TempF',NULL,1,'NO','ACTIVE'),(1231,27,'T-T overhang','T-T overhang',NULL,1,'NO','ACTIVE'),(1232,27,'delTK1','delTK1',NULL,1,'NO','ACTIVE'),(1233,27,'TK1 ORF','TK1 ORF',NULL,1,'NO','ACTIVE'),(1234,27,'NEO','NEO',NULL,1,'NO','ACTIVE'),(1240,27,'fusion PCR overlap','fusion PCR overlap',NULL,1,'NO','ACTIVE'),(1241,27,'5\'UTR','5\'UTR',NULL,1,'NO','ACTIVE'),(1242,27,'attP-F primer','attP-F primer',NULL,1,'NO','ACTIVE'),(1243,27,'Neo rev2','Neo rev2',NULL,1,'NO','ACTIVE'),(1244,27,'Neo fwd','Neo fwd',NULL,1,'NO','ACTIVE'),(1245,27,'TempR','TempR',NULL,1,'NO','ACTIVE'),(1246,27,'MDSSR seq primer','MDSSR seq primer',NULL,1,'NO','ACTIVE'),(1247,27,'MC1J seq primer','MC1J seq primer',NULL,1,'NO','ACTIVE'),(1248,37,'AsiSI','AsiSI',NULL,1,'NO','ACTIVE'),(1249,27,'FRT3','FRT3',NULL,1,'NO','ACTIVE'),(1250,27,'iPCR','iPCR',NULL,1,'NO','ACTIVE'),(1251,27,'bgreF','bgreF',NULL,1,'NO','ACTIVE'),(1252,27,'bgcF','bgcF',NULL,1,'NO','ACTIVE'),(1253,27,'bgcR','bgcR',NULL,1,'NO','ACTIVE'),(1254,27,'bgreR','bgreR',NULL,1,'NO','ACTIVE'),(1255,37,'AscI','AscI',NULL,1,'NO','ACTIVE'),(1256,37,'EagI','EagI',NULL,1,'NO','ACTIVE'),(1257,37,'attP1','attP1',NULL,1,'NO','ACTIVE'),(1258,27,'ncG3','ncG3',NULL,1,'NO','ACTIVE'),(1259,27,'exon','exon',NULL,1,'NO','ACTIVE'),(1260,27,'ENSMUSG00000025630','ENSMUSG00000025630',NULL,1,'NO','ACTIVE'),(1261,27,'D3','D3',NULL,1,'NO','ACTIVE'),(1262,27,'D5','D5',NULL,1,'NO','ACTIVE'),(1263,27,'phe S','phe S',NULL,1,'NO','ACTIVE'),(1264,27,'EM7','EM7',NULL,1,'NO','ACTIVE'),(1265,27,'U3','U3',NULL,1,'NO','ACTIVE'),(1266,27,'U5','U5',NULL,1,'NO','ACTIVE'),(1267,27,'ncG5','ncG5',NULL,1,'NO','ACTIVE'),(1268,27,'G5','G5',NULL,1,'NO','ACTIVE'),(1269,27,'ex3','ex3',NULL,1,'NO','ACTIVE'),(1270,27,'ex4','ex4',NULL,1,'NO','ACTIVE'),(1271,27,'ex7','ex7',NULL,1,'NO','ACTIVE'),(1272,27,'G3','G3',NULL,1,'NO','ACTIVE'),(1273,2,'Expression failed','Expression failed',NULL,1,'NO','ACTIVE'),(1274,27,'IU-PCR','IU-PCR',NULL,1,'NO','ACTIVE'),(1275,27,'UI-PCR','UI-PCR',NULL,1,'NO','ACTIVE'),(1276,27,'pGTseqR','pGTseqR',NULL,1,'NO','ACTIVE'),(1277,27,'beta-GAL','beta-GAL',NULL,1,'NO','ACTIVE'),(1278,27,'attP-R primer','attP-R primer',NULL,1,'NO','ACTIVE'),(1279,27,'FRT primer','FRT primer',NULL,1,'NO','ACTIVE'),(1280,27,'FRT reverse','FRT reverse',NULL,1,'NO','ACTIVE'),(1281,37,'SgrAI','SgrAI',NULL,1,'NO','ACTIVE'),(1282,37,'FseI','FseI',NULL,1,'NO','ACTIVE'),(1283,27,'L1-U3 link','L1-U3 link',NULL,1,'NO','ACTIVE'),(1284,27,'L2-D5 link','L2-D5 link',NULL,1,'NO','ACTIVE'),(1285,27,'bleo','bleo',NULL,1,'NO','ACTIVE'),(1286,27,'GAP5','GAP5',NULL,1,'NO','ACTIVE'),(1287,27,'ENSMUSG00000001506','ENSMUSG00000001506',NULL,1,'NO','ACTIVE'),(1288,27,'MUT5\'','MUT5\'',NULL,1,'NO','ACTIVE'),(1289,27,'MUT3\'','MUT3\'',NULL,1,'NO','ACTIVE'),(1290,27,'GAP3','GAP3',NULL,1,'NO','ACTIVE'),(1291,27,'HSV-TK1','HSV-TK1',NULL,1,'NO','ACTIVE'),(1292,27,'451baseF','451baseF',NULL,1,'NO','ACTIVE'),(1293,27,'451cloF','451cloF',NULL,1,'NO','ACTIVE'),(1294,27,'451cloR','451cloR',NULL,1,'NO','ACTIVE'),(1295,27,'T3 primer sequence','T3 primer sequence',NULL,1,'NO','ACTIVE'),(1296,27,'451baseR','451baseR',NULL,1,'NO','ACTIVE'),(1297,27,'452faF','452faF',NULL,1,'NO','ACTIVE'),(1298,27,'452cloF','452cloF',NULL,1,'NO','ACTIVE'),(1299,27,'452cloR','452cloR',NULL,1,'NO','ACTIVE'),(1300,27,'452faR','452faR',NULL,1,'NO','ACTIVE'),(1301,24,'Tetracycline Resistance','Tetracycline Resistance',NULL,1,'NO','ACTIVE'),(1302,27,'Rosa26 5\'arm','Rosa26 5\'arm',NULL,1,'NO','ACTIVE'),(1303,27,'Rosa26 3\'arm','Rosa26 3\'arm',NULL,1,'NO','ACTIVE'),(1304,27,'gam','gam',NULL,1,'NO','ACTIVE'),(1305,27,'bet','bet',NULL,1,'NO','ACTIVE'),(1306,27,'exo','exo',NULL,1,'NO','ACTIVE'),(1307,27,'RecA','RecA',NULL,1,'NO','ACTIVE'),(1308,27,'repA','repA',NULL,1,'NO','ACTIVE'),(1309,5,'pBAD','pBAD',NULL,1,'NO','ACTIVE'),(1310,37,'NheI','NheI',NULL,1,'NO','ACTIVE'),(1311,27,'hKLF4','hKLF4',NULL,1,'NO','ACTIVE'),(1312,27,'hcMYC','hcMYC',NULL,1,'NO','ACTIVE'),(1313,27,'hLIN28','hLIN28',NULL,1,'NO','ACTIVE'),(1314,27,'hNanog','hNanog',NULL,1,'NO','ACTIVE'),(1315,27,'hOCT-4','hOCT-4',NULL,1,'NO','ACTIVE'),(1316,27,'hSOX2','hSOX2',NULL,1,'NO','ACTIVE'),(1317,27,'rtTA-Advanced','rtTA-Advanced',NULL,1,'NO','ACTIVE'),(1318,37,'PaeI','PaeI',NULL,1,'NO','ACTIVE'),(1319,25,'PGKpurobGHpA','PGKpurobGHpA',NULL,1,'NO','ACTIVE'),(1320,27,'HPRT','HPRT',NULL,1,'NO','ACTIVE'),(1321,27,'piggyBac ','piggyBac ',NULL,1,'NO','ACTIVE'),(1322,27,'pCAG-pBase','pCAG-pBase',NULL,1,'NO','ACTIVE'),(1323,37,'BamHI','BamHI',NULL,1,'NO','ACTIVE'),(1324,37,'SacI','SacI',NULL,1,'NO','ACTIVE'),(1325,4,'','',NULL,1,'NO','ACTIVE'),(1326,27,'GF473','GF473',NULL,1,'NO','ACTIVE'),(1327,27,'mIR-17-92','mIR-17-92',NULL,1,'NO','ACTIVE'),(1328,27,'GB802','GB802',NULL,1,'NO','ACTIVE'),(1329,27,'Psv40_d90','Psv40_d90',NULL,1,'NO','ACTIVE'),(1330,27,'FX237','FX237',NULL,1,'NO','ACTIVE'),(1331,27,'FY611','FY611',NULL,1,'NO','ACTIVE'),(1332,27,'PVUI3\'','PVUI3\'',NULL,1,'NO','ACTIVE'),(1333,27,'PVUI5\'','PVUI5\'',NULL,1,'NO','ACTIVE'),(1334,27,'P_bla','P_bla',NULL,1,'NO','ACTIVE'),(1335,27,'AICDA','AICDA',NULL,1,'NO','ACTIVE'),(1336,27,'BgeoJrev','BgeoJrev',NULL,1,'NO','ACTIVE'),(1337,25,'TK','TK',NULL,1,'NO','ACTIVE'),(1338,27,'3\' ITR','3\' ITR',NULL,1,'NO','ACTIVE'),(1339,27,'V5 Epitope','V5 Epitope',NULL,1,'NO','ACTIVE'),(1340,27,'5\' ITR','5\' ITR',NULL,1,'NO','ACTIVE'),(1341,27,'FoxA2','FoxA2',NULL,1,'NO','ACTIVE'),(1342,27,'Glis3','Glis3',NULL,1,'NO','ACTIVE'),(1343,27,'Hex','Hex',NULL,1,'NO','ACTIVE'),(1344,27,'hMnx1','hMnx1',NULL,1,'NO','ACTIVE'),(1345,27,'Hnf1a','Hnf1a',NULL,1,'NO','ACTIVE'),(1346,27,'Hnf1b','Hnf1b',NULL,1,'NO','ACTIVE'),(1347,27,'Hnf4a8','Hnf4a8',NULL,1,'NO','ACTIVE'),(1348,27,'Hnf6','Hnf6',NULL,1,'NO','ACTIVE'),(1349,27,'hNkx6.1','hNkx6.1',NULL,1,'NO','ACTIVE'),(1350,27,'Isl1','Isl1',NULL,1,'NO','ACTIVE'),(1351,27,'MafA','MafA',NULL,1,'NO','ACTIVE'),(1352,27,'NeuroD1','NeuroD1',NULL,1,'NO','ACTIVE'),(1353,27,'Ngn3','Ngn3',NULL,1,'NO','ACTIVE'),(1354,27,'Nkx2.2','Nkx2.2',NULL,1,'NO','ACTIVE'),(1355,27,'Nr5a1','Nr5a1',NULL,1,'NO','ACTIVE'),(1356,27,'Pax4','Pax4',NULL,1,'NO','ACTIVE'),(1357,27,'Pax6','Pax6',NULL,1,'NO','ACTIVE'),(1358,27,'CAT/CamR','CAT/CamR',NULL,1,'NO','ACTIVE'),(1359,2,'AscI PacI digest verified','AscI PacI digest verified',NULL,1,'NO','ACTIVE'),(1360,2,'AscI PacI verification failed','AscI PacI verification failed',NULL,1,'NO','ACTIVE'),(1363,2,'AAA verified','AAA verified',NULL,1,'NO','DEP'),(1364,2,'Mass spec verified','Mass spec verified',NULL,1,'NO','DEP'),(1365,15,'Dog (Canis Familiaris)','Dog (Canis Familiaris)',NULL,1,'NO','DEP'),(1366,15,'Human (Homo sapiens)','Human (Homo sapiens)',NULL,1,'NO','DEP'),(1367,15,'Mouse (Mus musculus)','Mouse (Mus musculus)',NULL,1,'NO','DEP'),(1368,15,'Rat (Rattus norvegicus)','Rat (Rattus norvegicus)',NULL,1,'NO','DEP'),(1369,15,'Yeast (Saccharomyces cerevisiae)','Yeast (Saccharomyces cerevisiae)',NULL,1,'NO','DEP'),(1379,54,'TEV','TEV',NULL,1,'NO','DEP'),(1380,54,'Thrombin','Thrombin',NULL,1,'NO','DEP'),(1388,53,'misc','misc',NULL,1,'NO','DEP'),(1390,27,'kozak','kozak',NULL,1,'NO','ACTIVE'),(1400,2,'published','published',NULL,1,'NO','ACTIVE'),(1401,20,'MCC','MCC',NULL,1,'NO','ACTIVE'),(1402,64,'Neomycin resistance','Neomycin resistance',NULL,1,'NO','ACTIVE'),(1403,64,'Puromycin resistance','Puromycin resistance',NULL,1,'NO','ACTIVE'),(1404,2,'used ad nauseum','used ad nauseum',NULL,1,'NO','ACTIVE'),(1405,64,'Blasticidin resistance','Blasticidin resistance',NULL,1,'NO','ACTIVE'),(1406,37,'attP2','attP2',NULL,1,'NO','ACTIVE'),(1411,68,'n-terminal','n-terminal',NULL,1,'NO','ACTIVE'),(1412,69,'flag','flag',NULL,1,'NO','ACTIVE'),(1413,70,'pept','pept',NULL,1,'NO','ACTIVE'),(1416,73,'n-terminal','n-terminal',NULL,1,'NO','ACTIVE'),(1417,74,'tag1','tag1',NULL,1,'NO','ACTIVE');
/*!40000 ALTER TABLE `System_Set_tbl` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `UserCategories_tbl`
--

DROP TABLE IF EXISTS `UserCategories_tbl`;
SET @saved_cs_client     = @@character_set_client;
SET character_set_client = utf8;
CREATE TABLE `UserCategories_tbl` (
  `categoryID` int(11) NOT NULL auto_increment,
  `category` varchar(250) NOT NULL default '',
  `status` enum('ACTIVE','DEP') NOT NULL default 'ACTIVE',
  PRIMARY KEY  (`categoryID`)
) ENGINE=MyISAM AUTO_INCREMENT=5 DEFAULT CHARSET=latin1;
SET character_set_client = @saved_cs_client;

--
-- Dumping data for table `UserCategories_tbl`
--

LOCK TABLES `UserCategories_tbl` WRITE;
/*!40000 ALTER TABLE `UserCategories_tbl` DISABLE KEYS */;
INSERT INTO `UserCategories_tbl` VALUES (1,'Admin','ACTIVE'),(2,'Creator','ACTIVE'),(3,'Writer','ACTIVE'),(4,'Reader','ACTIVE');
/*!40000 ALTER TABLE `UserCategories_tbl` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `UserPermission_tbl`
--

DROP TABLE IF EXISTS `UserPermission_tbl`;
SET @saved_cs_client     = @@character_set_client;
SET character_set_client = utf8;
CREATE TABLE `UserPermission_tbl` (
  `userPermID` int(11) NOT NULL auto_increment,
  `pageID` int(11) NOT NULL default '0',
  `categoryID` int(11) NOT NULL default '0',
  `status` enum('ACTIVE','DEP') NOT NULL default 'ACTIVE',
  PRIMARY KEY  (`userPermID`),
  KEY `pageID` (`pageID`),
  KEY `categoryID` (`categoryID`)
) ENGINE=MyISAM AUTO_INCREMENT=47 DEFAULT CHARSET=latin1;
SET character_set_client = @saved_cs_client;

--
-- Dumping data for table `UserPermission_tbl`
--

LOCK TABLES `UserPermission_tbl` WRITE;
/*!40000 ALTER TABLE `UserPermission_tbl` DISABLE KEYS */;
INSERT INTO `UserPermission_tbl` VALUES (1,1,4,'ACTIVE'),(2,2,4,'ACTIVE'),(3,3,4,'ACTIVE'),(5,5,3,'ACTIVE'),(6,6,4,'ACTIVE'),(7,7,4,'ACTIVE'),(8,8,4,'ACTIVE'),(9,9,4,'ACTIVE'),(10,10,4,'ACTIVE'),(12,12,1,'ACTIVE'),(13,13,3,'ACTIVE'),(14,14,3,'ACTIVE'),(15,15,4,'ACTIVE'),(16,16,2,'ACTIVE'),(17,17,3,'ACTIVE'),(18,18,4,'ACTIVE'),(21,21,2,'ACTIVE'),(22,22,2,'ACTIVE'),(23,23,1,'ACTIVE'),(24,24,1,'ACTIVE'),(25,25,1,'ACTIVE'),(26,26,1,'ACTIVE'),(27,27,4,'ACTIVE'),(30,30,4,'ACTIVE'),(34,34,4,'ACTIVE'),(35,35,4,'ACTIVE'),(36,36,3,'ACTIVE'),(37,37,3,'ACTIVE'),(38,38,3,'ACTIVE'),(41,41,1,'ACTIVE'),(42,42,1,'ACTIVE'),(43,43,1,'ACTIVE'),(44,44,1,'ACTIVE'),(45,45,1,'ACTIVE'),(46,46,1,'ACTIVE');
/*!40000 ALTER TABLE `UserPermission_tbl` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `Users_tbl`
--

DROP TABLE IF EXISTS `Users_tbl`;
SET @saved_cs_client     = @@character_set_client;
SET character_set_client = utf8;
CREATE TABLE `Users_tbl` (
  `userID` int(11) NOT NULL auto_increment,
  `username` varchar(27) NOT NULL default '',
  `password` tinytext NOT NULL,
  `email` tinytext,
  `labID` int(11) NOT NULL default '1',
  `status` enum('ACTIVE','DEP') NOT NULL default 'ACTIVE',
  `firstname` varchar(250) NOT NULL default '',
  `lastname` varchar(250) NOT NULL default '',
  `category` int(11) NOT NULL default '4',
  `description` varchar(25) NOT NULL default '',
  `position` enum('Student','Postdoc','Technical','RA','MS','Administrative','IT','PI','Other') default NULL,
  PRIMARY KEY  (`userID`),
  KEY `labID` (`labID`)
) ENGINE=MyISAM AUTO_INCREMENT=2 DEFAULT CHARSET=latin1;
SET character_set_client = @saved_cs_client;

--
-- Dumping data for table `Users_tbl`
--

LOCK TABLES `Users_tbl` WRITE;
/*!40000 ALTER TABLE `Users_tbl` DISABLE KEYS */;
INSERT INTO `Users_tbl` VALUES (1,'admin','21232f297a57a5a743894a0e4a801fc3','openfreezer.admin@lunenfeld.ca',1,'ACTIVE','','',1,'Administrator',NULL);
/*!40000 ALTER TABLE `Users_tbl` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `WebTracker_tbl`
--

DROP TABLE IF EXISTS `WebTracker_tbl`;
SET @saved_cs_client     = @@character_set_client;
SET character_set_client = utf8;
CREATE TABLE `WebTracker_tbl` (
  `trackerID` int(11) NOT NULL auto_increment,
  `timestamp` datetime default NULL,
  `userIP` varchar(27) default NULL,
  `userhost` varchar(27) default NULL,
  `url` tinytext,
  PRIMARY KEY  (`trackerID`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1;
SET character_set_client = @saved_cs_client;

--
-- Dumping data for table `WebTracker_tbl`
--

LOCK TABLES `WebTracker_tbl` WRITE;
/*!40000 ALTER TABLE `WebTracker_tbl` DISABLE KEYS */;
/*!40000 ALTER TABLE `WebTracker_tbl` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `Wells_tbl`
--

DROP TABLE IF EXISTS `Wells_tbl`;
SET @saved_cs_client     = @@character_set_client;
SET character_set_client = utf8;
CREATE TABLE `Wells_tbl` (
  `wellID` int(11) NOT NULL auto_increment,
  `containerID` int(11) NOT NULL default '0',
  `wellCol` int(11) NOT NULL default '0',
  `wellRow` int(11) NOT NULL default '0',
  `do_not_use` enum('TRUE','FALSE') NOT NULL default 'FALSE',
  `reserved` enum('TRUE','FALSE') NOT NULL default 'TRUE',
  `creatorID` smallint(6) default '1',
  `status` enum('ACTIVE','DEP') NOT NULL default 'ACTIVE',
  PRIMARY KEY  (`wellID`),
  KEY `wellID` (`wellID`),
  KEY `containerID` (`containerID`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1;
SET character_set_client = @saved_cs_client;

--
-- Dumping data for table `Wells_tbl`
--

LOCK TABLES `Wells_tbl` WRITE;
/*!40000 ALTER TABLE `Wells_tbl` DISABLE KEYS */;
/*!40000 ALTER TABLE `Wells_tbl` ENABLE KEYS */;
UNLOCK TABLES;
/*!40103 SET TIME_ZONE=@OLD_TIME_ZONE */;

/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40014 SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;

-- Dump completed on 2012-01-13  0:04:21
